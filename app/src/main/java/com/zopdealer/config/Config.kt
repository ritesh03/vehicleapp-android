package com.zopdealer.config

class Config {

 companion object {

     const val BASE_URL = "https://demo.zopsoftware.com/api/admin/get/connection"
     const val BASE_URL_PAL = "https://pal.zopdms.com/"
     const val BASE_URL_KITE = "https://kite.zopdms.com"
     const val BASE_URL_PASTIMEMOTORS = "https://pastimemotors.zopdms.com"


      val APP_MODE = Modes.DEV
//      val APP_MODE = Modes.LIVE

 } // comp object

    enum class Modes{
        DEV, LIVE
    }


}//Config