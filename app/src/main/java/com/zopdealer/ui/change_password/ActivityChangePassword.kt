package com.zopdealer.ui.change_password

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.zopdealer.R
import com.zopdealer.base_classes.BaseActivity
import kotlinx.android.synthetic.main.activity_change_password.*

class ActivityChangePassword : BaseActivity() {

    companion object {
        fun start(context: Context) {
            val starter = Intent(context, ActivityChangePassword::class.java)
            context.startActivity(starter)
        }
    }


    override fun getID(): Int {
        return R.layout.activity_change_password
    }

    override fun iniView(savedInstanceState: Bundle?) {
    }

    fun onClickChangePassword(view: View) {
        when (view) {
            btn_back -> {
                finish()
            }
        }
    }
}