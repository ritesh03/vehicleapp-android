package com.zopdealer.ui.forgot_password

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.zopdealer.R
import com.zopdealer.base_classes.BaseActivity
import kotlinx.android.synthetic.main.activity_forgot_password.*

class ActivityForgotPassword : BaseActivity() {
    companion object {
        fun start(context: Context) {
            val starter = Intent(context, ActivityForgotPassword::class.java)
            context.startActivity(starter)
        }
    }
    override fun getID(): Int {
        return R.layout.activity_forgot_password
    }

    override fun iniView(savedInstanceState: Bundle?) {

    }

    fun onClickForgotPassword(view: View) {
        when(view){
            btn_back->{
                finish()
            }
            btn_submit->{
                finish()
            }
        }
    }
}