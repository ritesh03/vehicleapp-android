package com.zopdealer.ui.add_vehicle.interfaces

import android.view.View
import com.zopdealer.ui.add_vehicle.model.InteriorPhotosModel

interface RecyclerViewClickListener {
    fun onAdapterItemClick(view: View, pos: Int)

}