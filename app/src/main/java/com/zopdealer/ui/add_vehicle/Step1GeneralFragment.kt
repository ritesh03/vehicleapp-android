package com.zopdealer.ui.add_vehicle

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.zopdealer.R
import com.zopdealer.base_classes.BaseFragment
import com.zopdealer.network.responses.Inventory
import com.zopdealer.ui.add_vehicle.AddVehicleActivity.Companion.EXTRA_INVENTORY_DATA
import com.zopdealer.ui.add_vehicle.AddVehicleActivity.Companion.EXTRA_IS_EDIT
import com.zopdealer.ui.add_vehicle.adapter.AdapterMultipleSelectList
import com.zopdealer.ui.add_vehicle.adapter.AdapterVehicleTags
import com.zopdealer.ui.add_vehicle.interfaces.SelectTagsCallBack
import com.zopdealer.ui.add_vehicle.model.TagsModel
import com.zopdealer.ui.add_vehicle.viewmodel.AddEditVehicleViewModel
import com.zopdealer.ui.dealer.DealerDetailsActivity
import com.zopdealer.utils.Constants
import com.zopdealer.utils.preferences.Preferences
import kotlinx.android.synthetic.main.fragment_step1_general_addvehicle.*
import org.json.JSONArray
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class Step1GeneralFragment : BaseFragment(), SelectTagsCallBack, View.OnClickListener,
    OnItemSelectedListener {

    lateinit var adapterMultipleSelectList: AdapterMultipleSelectList
    var arrayTagList = ArrayList<TagsModel>()
    private lateinit var addVehicleViewModel: AddEditVehicleViewModel
    private lateinit var adapterVehicleTags: AdapterVehicleTags
    private val listVehicleTags = ArrayList<String>()
    private val arrayListLocations = ArrayList<String>()
    var decode:Boolean=false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_step1_general_addvehicle, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addVehicleViewModel = ViewModelProvider(requireActivity()).get(AddEditVehicleViewModel::class.java)

        val isEdit = arguments?.getBoolean(EXTRA_IS_EDIT)
        isEdit?.let {
            if (it) {
                addVehicleViewModel.setIsEditInventory(true)
                val inventoryData = arguments?.getSerializable(EXTRA_INVENTORY_DATA)
                addVehicleViewModel.setInventoryData(inventoryData as? Inventory?)
            }
        }

        adapterVehicleTags = AdapterVehicleTags(requireContext(), listVehicleTags)
        rv_vehicle_tags.adapter = adapterVehicleTags

        Preferences.prefs?.getString(
            Constants.BASE_URL,
            ""
        )?.let {
            addVehicleViewModel.getVehicleLocation(it)
            addVehicleViewModel.getActiveConfiguration(it)
           /* if (!addVehicleViewModel.isEditInventory) {

            }*/
        }

        addVehicleViewModel.vinResponse.observe(::getLifecycle) {
            tv_decode_vin.isEnabled = true
            showLoading(false)
            it?.let { vinResponse ->
                if (vinResponse.error != null) {
                    showSnackBar(vinResponse.error!!)
                    vinResponse.error = null
                } else{
                    try {
                        if (vinResponse.vin!=null && decode==true){
                            showSnackBar("VIN number successfully decoded!")
                            decode=false
                        }
                    }catch (e:Exception){

                    }

                }
            }
        }

        addVehicleViewModel.vehicleLocations.observe(::getLifecycle) {
            it?.let { listLocations ->
                arrayListLocations.clear()
                listLocations.forEach { location ->
                    arrayListLocations.add(location.name)
                }
            }
        }

        addVehicleViewModel.error.observe(::getLifecycle) {
            tv_decode_vin.isEnabled = true
            showLoading(false)
            it?.let {
                if (it.equals("Unauthorized Request")){
                    Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
                    Preferences.prefs?.edit()?.clear()?.apply()
                    DealerDetailsActivity.start(requireContext())
                    activity?.finishAffinity()
                }else{
                    Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
                }

            }
        }

        if (addVehicleViewModel.isEditInventory){
            et_stock_date.text = addVehicleViewModel.stockDate.value
        }else{
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            et_stock_date.text = sdf.format(cal.time).toString().toUpperCase()
            addVehicleViewModel.stockDate.value = sdf.format(cal.time).toString()
        }

        /*Click events*/
        setOnclickListener()
        //attachSpinnerAdater()
        spinnerListener()

        switch_vin_display.isChecked = true
        switch_visibility.isChecked = true
      //  addVehicleViewModel.vinDisplay.value = "1"
        // create an OnDateSetListener
        val dateSetListener =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDateInView()
            }
        et_stock_date.setOnClickListener {
            DatePickerDialog(
                requireContext(),
                dateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        switch_visibility.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                et_visibility.text = "On"
            } else {
                et_visibility.text = "Off"
            }
        }

        switch_vin_display.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                tv_vin_display_value.text = "On"
                switch_vin_display.isChecked = true
               // addVehicleViewModel.vinDisplay.value = "1"
            } else {
                tv_vin_display_value.text = "Off"
                switch_vin_display.isChecked = false
               // addVehicleViewModel.vinDisplay.value = "0"
            }
        }

        switch_feature_listing.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                tv_feature_listing_value.text = "On"
                addVehicleViewModel.featureListing.value = "1"
            } else {
                tv_feature_listing_value.text = "Off"
                addVehicleViewModel.featureListing.value = "0"
            }
        }

        adapterVehicleTags.onItemDelete = {
            listVehicleTags.remove(it)
            adapterVehicleTags.notifyDataSetChanged()
            if (adapterVehicleTags.itemCount == 0) {
                rv_vehicle_tags.visibility = View.GONE
            }
        }

        loadData()
    }

    private fun loadData() {
        et_vin_number.setText(addVehicleViewModel.vinNumber.value)
        et_stock_number.setText(addVehicleViewModel.stockNumber.value)
        et_select_status.text = addVehicleViewModel.status.value ?: "Instock"
        et_sub_select_status.text = addVehicleViewModel.subStatus.value?:"Available For Sale"
        et_vehicle_type.text = addVehicleViewModel.vehicleType.value
        et_vehicle_loaction.text = addVehicleViewModel.location.value

        if (addVehicleViewModel.vinDisplay.value == "0") {
            tv_vin_display_value.text = "Off"
            switch_vin_display.isChecked = false
        } else {
            tv_vin_display_value.text = "On"
            switch_vin_display.isChecked = true
        }

        if (addVehicleViewModel.featureListing.value == "1") {
            tv_feature_listing_value.text = "On"
            switch_feature_listing.isChecked = true
        } else {
            tv_feature_listing_value.text = "Off"
            switch_feature_listing.isChecked = false
        }

        if (addVehicleViewModel.visibility.value == "0") {
            et_visibility.text = "Off"
            switch_visibility.isChecked = false
        } else {
            et_visibility.text = "On"
            switch_visibility.isChecked = true
        }

        if (addVehicleViewModel.vehicleTags.value != null) {
            val jsonArray = JSONArray(addVehicleViewModel.vehicleTags.value)
            for (i in 0 until jsonArray.length()) {
                listVehicleTags.add(jsonArray.getString(i))
            }
            val hashSet = HashSet<String>()
            hashSet.addAll(listVehicleTags)
            listVehicleTags.clear()
            listVehicleTags.addAll(hashSet)
            adapterVehicleTags.notifyDataSetChanged()
            if (adapterVehicleTags.itemCount > 0) {
                rv_vehicle_tags.visibility = View.VISIBLE
            }
        }


    }

    fun attachSpinnerAdater() {
        //  spinnerArrayAdapter(spin_select_status, R.array.spinner_Select_status,requireContext())
        spinnerArrayAdapter(
            spin_vehicle_type,
            R.array.spinner_Select_vehicle_type,
            requireContext()
        )
        spinnerArrayAdapter(
            spin_vehicle_location,
            R.array.spinner_Select_location,
            requireContext()
        )
    }

    fun setOnclickListener() {
        btn_submit.setOnClickListener(this)
        et_select_status.setOnClickListener(this)
        et_vehicle_type.setOnClickListener(this)
        tv_toolTipTextTags.setOnClickListener(this)
        et_vehicle_loaction.setOnClickListener(this)
        tv_decode_vin.setOnClickListener(this)
        et_sub_select_status.setOnClickListener(this)
        tv_add_tag.setOnClickListener(this)
    }

    fun spinnerListener() {
        spin_select_status.setOnItemSelectedListener(this)
        spin_select_sub_status.setOnItemSelectedListener(this)
        spin_vehicle_type.setOnItemSelectedListener(this)
        spin_vehicle_location.setOnItemSelectedListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            btn_submit -> {
                if (et_vin_number.text.toString().trim().isEmpty()) {
                    showSnackBar(getString(R.string.error_empty_vin_number))
                    return
                }
                addVehicleViewModel.vinNumber.value = et_vin_number.text.toString().trim()
                addVehicleViewModel.stockNumber.value = et_stock_number.text.toString().trim()
                addVehicleViewModel.vehicleTags.value = listVehicleTags
                addVehicleViewModel.status.value = et_select_status.text.toString().trim()
                addVehicleViewModel.subStatus.value = et_sub_select_status.text.toString().trim()
                addVehicleViewModel.vehicleType.value = et_vehicle_type.text.toString().trim()
                addVehicleViewModel.location.value = et_vehicle_loaction.text.toString().trim()
                addVehicleViewModel.stockDate.value = et_stock_date.text.toString().trim()

                if (switch_visibility.isChecked)
                    addVehicleViewModel.visibility.value = "1"
                else
                    addVehicleViewModel.visibility.value = "0"

                if (switch_vin_display.isChecked)
                    addVehicleViewModel.vinDisplay.value = "1"
                else
                    addVehicleViewModel.vinDisplay.value = "0"

                if (switch_feature_listing.isChecked)
                    addVehicleViewModel.featureListing.value = "1"
                else
                    addVehicleViewModel.featureListing.value = "0"

                hideKeyboard()
                AddVehicleActivity.tv_steps_count_tv.text = "Step 2 of 8"
                AddVehicleActivity.selectedPosition = 1
                val step2VehicleInfoFragment = Step2VehicleInfoFragment();
                AddVehicleActivity.adapterStepsCount.notifyDataSetChanged()
                AddVehicleActivity.rv_steps_add_vehicle_rv.scrollToPosition(1)
                replaceFragment(step2VehicleInfoFragment, false, R.id.add_vehicle_steps_container)

                if(addVehicleViewModel.isEditInventory){
                    Preferences.prefs?.getString(
                        Constants.BASE_URL,
                        ""
                    )?.let {
                        addVehicleViewModel.editInventory(it, addVehicleViewModel.inventoryId.value ?: "")
                    }
                }
            }
            tv_toolTipTextTags -> {
                showDialog("Select Tags")
            }
            et_select_status -> {
                spinnerArrayAdapter(
                    spin_select_status,
                    R.array.spinner_Select_status,
                    requireContext()
                )
                spin_select_status.performClick()
            }
            et_sub_select_status -> {
                spinnerArrayAdapter(
                    spin_select_sub_status,
                    R.array.spinner_select_sub_status,
                    requireContext()
                )
                spin_select_sub_status.performClick()
            }
            et_vehicle_type -> {
                spinnerArrayAdapter(
                    spin_vehicle_type,
                    R.array.spinner_Select_vehicle_type,
                    requireContext()
                )
                spin_vehicle_type.performClick()
            }
            et_vehicle_loaction -> {
                if (arrayListLocations.isNotEmpty()) {
                    spinnerArrayAdapter(
                        spin_vehicle_location,
                        arrayListLocations,
                        requireContext()
                    )
                    spin_vehicle_location.performClick()
                }
            }

            tv_decode_vin -> {
                if (et_vin_number.text.toString().trim().isEmpty()) {
                    showSnackBar(getString(R.string.error_empty_vin_number))
                } else {
                    showLoading(true)
                    tv_decode_vin.isEnabled = false
                    decode=true
                    addVehicleViewModel.decodeVin(
                        Preferences.prefs?.getString(
                            Constants.BASE_URL,
                            ""
                        ) ?: "", et_vin_number.text.toString().trim()
                    )
                }
            }
            tv_add_tag -> {
                if (et_vehicle_tags.text.toString().trim().isEmpty()) {
                    showSnackBar(getString(R.string.error_empty_vehicle_tag))
                } else {
                    hideKeyboard()
                    listVehicleTags.add(et_vehicle_tags.text.toString().trim())
                    adapterVehicleTags.notifyDataSetChanged()
                    et_vehicle_tags.setText("")
                    if (adapterVehicleTags.itemCount > 0) {
                        rv_vehicle_tags.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val selectedItemText = parent?.getItemAtPosition(position) as String

        when (parent.id) {
            R.id.spin_select_status -> {
                et_select_status.text = selectedItemText
            }
            R.id.spin_select_sub_status -> {
                et_sub_select_status.text = selectedItemText
            }
            R.id.spin_vehicle_type -> {
                et_vehicle_type.text = selectedItemText
            }
            R.id.spin_vehicle_location -> {
                et_vehicle_loaction.text = selectedItemText
            }
        }
    }

    private fun updateDateInView() {
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        et_stock_date.text = sdf.format(cal.time).toString().toUpperCase()
        addVehicleViewModel.stockDate.value = sdf.format(cal.time).toString()
    }

    private fun showDialog(title: String) {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.custom_dialog_select_tags)
        dialog.getWindow()?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        );

        val body = dialog.findViewById(R.id.tv_title) as TextView
        body.text = title
        val btn_cancel_dialog = dialog.findViewById(R.id.btn_cancel_dialog) as CardView
        val tv_text_cancel = dialog.findViewById(R.id.tv_text_cancel) as TextView
        val btn_ok = dialog.findViewById(R.id.btn_ok) as TextView
        val rv = dialog.findViewById(R.id.rv_tags) as RecyclerView


        //Setting adapter
        initTagsView(rv, arrayTagList)

        btn_ok.setOnClickListener {
            dialog.dismiss()
        }
        tv_text_cancel.setOnClickListener { dialog.dismiss() }
        dialog.show()

    }


    fun initTagsView(rv: RecyclerView, arrayTitle: ArrayList<TagsModel>) {
        adapterMultipleSelectList = AdapterMultipleSelectList(requireContext(), arrayTitle, 1, this)
        rv.layoutManager = StaggeredGridLayoutManager(1, 1)
        rv.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        rv.adapter = adapterMultipleSelectList
    }

    override fun onCheckBoxClick(position: Int, text: String, isChecked: Boolean) {
        arrayTagList.set(position, TagsModel(text, isChecked))

        val sb = StringBuilder()
        val listCheckedItem = ArrayList<TagsModel>()
        for (i in arrayTagList.indices) {
            if (arrayTagList.get(i).isChecked) {
                listCheckedItem.add(arrayTagList.get(i))
            }
        }

        for (i in listCheckedItem.indices) {
            if (!i.equals(listCheckedItem.lastIndex)) {
                sb.append(listCheckedItem.get(i).text)
                sb.append(",")
            } else {
                sb.append(listCheckedItem.get(i).text)
            }
        }
        tv_toolTipTextTags.text = sb.toString()

        if (sb.isEmpty()) {
            tv_toolTipTextTags.hint = "Select Tags"
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    companion object {
        const val serverFormat = "yyyy-MM-dd HH:mm"
        const val myFormat = "MM/dd/yyyy"
    }
}