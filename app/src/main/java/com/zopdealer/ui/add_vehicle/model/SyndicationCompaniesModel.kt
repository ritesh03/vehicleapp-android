package com.zopdealer.ui.add_vehicle.model

import com.google.gson.annotations.SerializedName

data class SyndicationCompaniesModel(
    @SerializedName("syndication_companies_list")
    val listSyndicationCompanies: List<SyndicationCompanies>?
)

data class SyndicationCompanies(
    @SerializedName("id")
    val id: String?,
    @SerializedName("image_url")
    val imageUrl: String?,
    @SerializedName("label")
    val label: String?,
    @SerializedName("slug")
    val slug: String?,
    @SerializedName("status")
    var status: String?,
    var isSelected: Boolean = false
)
