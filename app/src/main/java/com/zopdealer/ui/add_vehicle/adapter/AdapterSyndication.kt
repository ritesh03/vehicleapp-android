package com.zopdealer.ui.add_vehicle.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zopdealer.R
import com.zopdealer.ui.add_vehicle.model.SyndicationCompanies
import com.zopdealer.utils.Constants
import com.zopdealer.utils.preferences.Preferences
import kotlinx.android.synthetic.main.adapter_syndication.view.*

class AdapterSyndication(
    private val context: Context,
    private val listSyndicationCompanies: List<SyndicationCompanies>
) :
    RecyclerView.Adapter<AdapterSyndication.MyViewHolder>() {

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.adapter_syndication,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return listSyndicationCompanies.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = listSyndicationCompanies[position]
      /*  if(model.status.equals("1")){
            holder.itemView.radioButton.isChecked = true
        }else{0
            holder.itemView.radioButton.isChecked = true
        }*/
        holder.itemView.radioButton.isChecked = model.isSelected

        val baseUrl = Preferences.prefs?.getString(Constants.BASE_URL, "")
        baseUrl?.let { url ->
            val trimmedUrl = url.substring(0, url.length - 1)
            Glide.with(context).load(Constants.IMAGE_BASE_URL + "" + model.imageUrl)
                .placeholder(R.drawable.place_holder)
                .into(holder.itemView.imageView4)
        }

        holder.itemView.radioButton.setOnClickListener {
            model.isSelected = !model.isSelected
        }
    }
}