package com.zopdealer.ui.add_vehicle.model

data class VehicleLocationModel(val locations: List<Locations>)

data class Locations(val name: String)