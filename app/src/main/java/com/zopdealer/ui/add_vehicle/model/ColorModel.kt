package com.zopdealer.ui.add_vehicle.model

data class ColorModel(val colorName: String, var isSelected: Boolean = false)