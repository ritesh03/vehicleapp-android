package com.zopdealer.ui.add_vehicle

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.zopdealer.R
import com.zopdealer.base_classes.BaseFragment
import com.zopdealer.ui.add_vehicle.adapter.AdapterExteriorColor
import com.zopdealer.ui.add_vehicle.adapter.AdapterInteriorColor
import com.zopdealer.ui.add_vehicle.model.ColorModel
import com.zopdealer.ui.add_vehicle.viewmodel.AddEditVehicleViewModel
import com.zopdealer.utils.Constants
import com.zopdealer.utils.preferences.Preferences
import kotlinx.android.synthetic.main.fragment_step3_mileage_addvehicle.*

class Step3MileageFragment : BaseFragment(), AdapterView.OnItemSelectedListener {
    lateinit var adapterInteriorColor: AdapterInteriorColor
    lateinit var adapterExteriorColor: AdapterExteriorColor
    var arrayInteriorColors = ArrayList<ColorModel>()
    var arrayExteriorColors = ArrayList<ColorModel>()
    private lateinit var addVehicleViewModel: AddEditVehicleViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_step3_mileage_addvehicle, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addVehicleViewModel =
            ViewModelProvider(requireActivity()).get(AddEditVehicleViewModel::class.java)

        spinnerArrayAdapter(spinner_odometer_type, R.array.spinner_odometer_type)

        arrayInteriorColors.add(ColorModel("Black"))
        arrayInteriorColors.add(ColorModel("Grey"))
        arrayInteriorColors.add(ColorModel("White"))
        arrayInteriorColors.add(ColorModel("Cream"))
        arrayInteriorColors.add(ColorModel("Tan"))
        arrayInteriorColors.add(ColorModel("Beige"))
        arrayInteriorColors.add(ColorModel("Brown"))
        arrayInteriorColors.add(ColorModel("Red"))
        arrayInteriorColors.add(ColorModel("Orange"))
        arrayInteriorColors.add(ColorModel("Yellow"))
        arrayInteriorColors.add(ColorModel("Blue"))

        arrayExteriorColors.add(ColorModel("Black"))
        arrayExteriorColors.add(ColorModel("Grey"))
        arrayExteriorColors.add(ColorModel("Silver"))
        arrayExteriorColors.add(ColorModel("White"))
        arrayExteriorColors.add(ColorModel("Cream"))
        arrayExteriorColors.add(ColorModel("Tan"))
        arrayExteriorColors.add(ColorModel("Brown"))
        arrayExteriorColors.add(ColorModel("Maroon"))
        arrayExteriorColors.add(ColorModel("Red"))
        arrayExteriorColors.add(ColorModel("Orange"))
        arrayExteriorColors.add(ColorModel("Gold"))
        arrayExteriorColors.add(ColorModel("Yellow"))
        arrayExteriorColors.add(ColorModel("Green"))
        arrayExteriorColors.add(ColorModel("Teal"))
        arrayExteriorColors.add(ColorModel("Blue"))
        arrayExteriorColors.add(ColorModel("Purple"))



        initViewsInterior()
        initViewsExterior()
        spinnerListener()

        btn_submit.setOnClickListener {
            hideKeyboard()
            addVehicleViewModel.odometer.value = et_odometer.text.toString().trim()

            var isExteriorColorSelected: Boolean = false
            arrayExteriorColors.map {
                if (it.isSelected) {
                    addVehicleViewModel.exteriorColor.value = it.colorName
                    isExteriorColorSelected = true
                }
            }

            var isInteriorColorSelected: Boolean = false
            arrayInteriorColors.map {
                if (it.isSelected) {
                    addVehicleViewModel.interiorColor.value = it.colorName
                    isInteriorColorSelected = true
                }
            }

            if (!et_other_exterior_color.text.toString().trim().isNullOrEmpty()) {
                addVehicleViewModel.exteriorColor.value =
                    et_other_exterior_color.text.toString().trim()
            }

            if (!et_other_interior_color.text.toString().trim().isNullOrEmpty()) {
                addVehicleViewModel.interiorColor.value =
                    et_other_interior_color.text.toString().trim()
            }

            AddVehicleActivity.tv_steps_count_tv.text = "Step 4 of 8"
            AddVehicleActivity.selectedPosition = 3
            val fragment = Step4PricingFragment()
            AddVehicleActivity.adapterStepsCount.notifyDataSetChanged()
            AddVehicleActivity.rv_steps_add_vehicle_rv.scrollToPosition(3)
            replaceFragment(fragment, false, R.id.add_vehicle_steps_container)

            if (addVehicleViewModel.isEditInventory) {
                Preferences.prefs?.getString(
                    Constants.BASE_URL,
                    ""
                )?.let {
                    addVehicleViewModel.editInventory(
                        it,
                        addVehicleViewModel.inventoryId.value ?: ""
                    )
                }
            }
        }

        loadData()
    }

    private fun spinnerListener() {
        spinner_odometer_type.onItemSelectedListener = this
    }

    private fun loadData() {
        et_odometer.setText(addVehicleViewModel.odometer.value)

        if(addVehicleViewModel.kmMiles.value.equals("Miles")){
            spinner_odometer_type.setSelection(1)
        }else if(addVehicleViewModel.kmMiles.value.equals("None")){
            spinner_odometer_type.setSelection(2)
        }else{
            spinner_odometer_type.setSelection(0)
        }

        var isInteriorColorSelected = false
        arrayInteriorColors.map {
            if (addVehicleViewModel.interiorColor.value == it.colorName) {
                it.isSelected = true
                isInteriorColorSelected= true
            }
        }
        adapterInteriorColor.notifyDataSetChanged()

        var isExteriorColorSelected = false
        arrayExteriorColors.map {
            if (addVehicleViewModel.exteriorColor.value == it.colorName) {
                it.isSelected = true
                isExteriorColorSelected = true
            }
        }
        adapterExteriorColor.notifyDataSetChanged()

        if (addVehicleViewModel.isEditInventory){
            if (!isInteriorColorSelected){
                et_other_interior_color.setText(addVehicleViewModel.interiorColor.value.toString())
            }
        }

        if (addVehicleViewModel.isEditInventory){
            if (!isExteriorColorSelected){
                et_other_exterior_color.setText(addVehicleViewModel.exteriorColor.value.toString())
            }
        }

    }

    private fun initViewsInterior() {
        adapterInteriorColor = AdapterInteriorColor(requireContext(), arrayInteriorColors)
        rv_interior_color.layoutManager = StaggeredGridLayoutManager(1, 1)
        rv_interior_color.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        rv_interior_color.adapter = adapterInteriorColor

        adapterInteriorColor.onColorClick = {
            arrayInteriorColors.map { colorModel ->
                colorModel.isSelected = it.colorName == colorModel.colorName
            }
            adapterInteriorColor.notifyDataSetChanged()
        }
    }

    private fun initViewsExterior() {
        adapterExteriorColor = AdapterExteriorColor(requireContext(), arrayExteriorColors)

        rv_exterior_color.layoutManager = StaggeredGridLayoutManager(1, 1)
        rv_exterior_color.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        rv_exterior_color.adapter = adapterExteriorColor

        adapterExteriorColor.onColorClick = {
            arrayExteriorColors.map { colorModel ->
                colorModel.isSelected = it.colorName == colorModel.colorName
            }
            adapterExteriorColor.notifyDataSetChanged()
        }
    }

    private fun spinnerArrayAdapter(spinner: Spinner, array: Int) {
        ArrayAdapter.createFromResource(
            requireContext(),
            array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val selectedItemText = parent?.getItemAtPosition(position) as String
        when (parent.id) {
            R.id.spinner_odometer_type -> {
                et_km_miles.text = selectedItemText
                addVehicleViewModel.kmMiles.value = selectedItemText
            }
        }
    }
}