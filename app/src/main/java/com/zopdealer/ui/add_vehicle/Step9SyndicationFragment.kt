package com.zopdealer.ui.add_vehicle

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.zopdealer.R
import com.zopdealer.base_classes.BaseFragment
import com.zopdealer.ui.add_vehicle.adapter.AdapterSyndication
import com.zopdealer.ui.add_vehicle.model.SyndicationCompanies
import com.zopdealer.ui.add_vehicle.viewmodel.AddEditVehicleViewModel
import com.zopdealer.utils.Constants
import com.zopdealer.utils.preferences.Preferences
import kotlinx.android.synthetic.main.fragment_step9_syndication_addvehicle.*
import org.json.JSONArray
import kotlin.collections.ArrayList


class Step9SyndicationFragment : BaseFragment() {

    lateinit var adapterSyndication: AdapterSyndication
    private lateinit var addVehicleViewModel: AddEditVehicleViewModel
    private var listSyndicate = ArrayList<SyndicationCompanies>()

    companion object {

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_step9_syndication_addvehicle, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addVehicleViewModel =
            ViewModelProvider(requireActivity()).get(AddEditVehicleViewModel::class.java)
        showLoading(true)
        btn_save.isEnabled = false

        addVehicleViewModel.getSyndicationCompanies("https://zopsoftware.com/")

        addVehicleViewModel.syndicationCompanies.observe(::getLifecycle) {
            it?.let {
                Preferences.prefs?.getString(Constants.BASE_URL, "")?.let {
                    addVehicleViewModel.getActiveSyndication(it)
                }
            }
        }

        //addVehicleViewModel.syndication

        addVehicleViewModel.createInventorySuccess.observe(::getLifecycle) {
            showLoading(false)
            it?.let { createInventorySuccess ->
                if (createInventorySuccess.error != null) {
                    showSnackBar(createInventorySuccess.error!!)
                    createInventorySuccess.error = null
                } else  if (createInventorySuccess.vin != null) {
                    showSnackBar(createInventorySuccess.vin!!)

                }else  if (createInventorySuccess.stock_no != null) {
                    showSnackBar(createInventorySuccess.stock_no!!)
                }else{
                    Toast.makeText(requireContext(), it.success, Toast.LENGTH_LONG).show()
                    requireActivity().finish()
                }
            }

        }

        addVehicleViewModel.error.observe(::getLifecycle) {
            showLoading(false)
            it?.let {
                Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
            }
        }

        addVehicleViewModel.erroradd.observe(::getLifecycle) {
            showLoading(false)
            it?.let {
              //  Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
            }
        }

        addVehicleViewModel.activeSyndication.observe(::getLifecycle) {
            showLoading(false)
            it?.let { activeSyndication ->
                val selectedSyndicationArray = JSONArray(activeSyndication[0].value)
                val listFilteredSyndication = ArrayList<SyndicationCompanies>()

                for (i in 0 until selectedSyndicationArray.length()) {
                    val jsonObject = selectedSyndicationArray.getJSONObject(i)
                    Log.e("object====",jsonObject.toString())
                    val slug = jsonObject.optString("slug")
                    val status:String=jsonObject.optString("status")
                    addVehicleViewModel.syndicationCompanies.value?.map { syndicateCompany ->


                        if (syndicateCompany.slug == slug) {

                            if (addVehicleViewModel.isEditInventory) {
                                val jsonSyndicationArray = JSONArray(addVehicleViewModel.syndication.value)
                                Log.e("list====",jsonSyndicationArray.toString())

                                Log.e("list====",jsonSyndicationArray.toString())

                                if (addVehicleViewModel.syndication.value!!.contains(slug)){
                                    syndicateCompany.status=status
                                    syndicateCompany.isSelected = true
                                }else{
                                    syndicateCompany.status=status

                                    syndicateCompany.isSelected = false
                                }
                            }else{
                                if (status.equals("1",true)){
                                    syndicateCompany.status=status
                                    syndicateCompany.isSelected = true
                                }else{
                                    syndicateCompany.status=status

                                    syndicateCompany.isSelected = false
                                }
                            }

                            listFilteredSyndication.add(syndicateCompany)
                        }
                    }
                }
                //Log.e("list====",listFilteredSyndication.toString())
                setAdapter(listFilteredSyndication)
                btn_save.isEnabled = true
            }
        }

        btn_save.setOnClickListener {
            hideKeyboard()
            val listSelectedSyndication = ArrayList<String>()
            listSyndicate.map {
                if (it.isSelected) {
                    it.slug?.let { it1 -> listSelectedSyndication.add(it1) }
                }
            }
            addVehicleViewModel.syndication.value = listSelectedSyndication

            if (addVehicleViewModel.isEditInventory) {
                Preferences.prefs?.getString(
                    Constants.BASE_URL,
                    ""
                )?.let {
                    showLoading(true)
                    addVehicleViewModel.editInventory(
                        it,
                        addVehicleViewModel.inventoryId.value ?: "", true
                    )
                }
            } else {
                if (addVehicleViewModel.vinNumber.value.isNullOrEmpty()){
                    showSnackBar(getString(R.string.error_empty_vin_number))
                }else if(addVehicleViewModel.year.value.isNullOrEmpty()){
                    showSnackBar(getString(R.string.error_empty_year))
                }else if (addVehicleViewModel.make.value.isNullOrEmpty()){
                    showSnackBar(getString(R.string.error_empty_make))
                }else if (addVehicleViewModel.model.value.isNullOrEmpty()){
                    showSnackBar(getString(R.string.error_empty_model))
                }else{
                    Preferences.prefs?.getString(
                            Constants.BASE_URL,
                            ""
                    )?.let {
                        showLoading(true)
                        addVehicleViewModel.addInventory(it,requireActivity())
                    }
                }

            }
        }
    }

    private fun setAdapter(listSyndicate: List<SyndicationCompanies>) {
        this.listSyndicate = listSyndicate as ArrayList<SyndicationCompanies>
        adapterSyndication =
            AdapterSyndication(requireContext(), listSyndicate)
        rv_syndication.layoutManager = StaggeredGridLayoutManager(1, 1)
        rv_syndication.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        rv_syndication.adapter = adapterSyndication
    }

}