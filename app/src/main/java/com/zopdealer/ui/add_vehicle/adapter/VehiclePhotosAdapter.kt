package com.zopdealer.ui.add_vehicle.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.zopdealer.R
import com.zopdealer.network.responses.Gallery
import com.zopdealer.ui.add_vehicle.call_back.ItemInteriorPhotoMoveCallback
import com.zopdealer.ui.add_vehicle.interfaces.RecyclerViewClickListener
import com.zopdealer.utils.Constants
import com.zopdealer.utils.preferences.Preferences
import kotlinx.android.synthetic.main.adapter_photos.view.*


class VehiclePhotosAdapter(
    var context: Context,
    var vehicleImagesList: MutableList<Gallery>,
    private val clickListener: RecyclerViewClickListener,
    private val status: Int
) :
    RecyclerView.Adapter<VehiclePhotosAdapter.MyViewHolder>(),
    ItemInteriorPhotoMoveCallback.ItemTouchHelperContract {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        return MyViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.adapter_photos,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return vehicleImagesList.size

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.onBind(vehicleImagesList[position])
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        /**
         * Used to bind the view
         */
        fun onBind(gallery: Gallery) {
            val baseUrl = Preferences.prefs?.getString(Constants.BASE_URL, "")
            baseUrl?.let { url ->
                val trimmedUrl = url.substring(0, url.length - 1)
                Glide.with(context).load(Constants.IMAGE_BASE_URL+ "" + gallery.image_url)
                    .placeholder(R.drawable.place_holder)
                    .transform(CenterCrop())
                    .into(itemView.img_selected_img)
            }
            // Used to handle the active and inactive image status
            gallery.active?.let { isActive ->
                if (isActive == Constants.INVENTORY_IMG_ACTIVE) {
                    itemView.img_status.setImageResource(R.drawable.ic_active)
                } else {
                    itemView.img_status.setImageResource(R.drawable.ic_inactive)
                }
            }
            itemView.btn_cross.visibility = View.VISIBLE
            // Used to handle the click for the image status view
            itemView.img_status.setOnClickListener {
                clickListener.onAdapterItemClick(
                    itemView.img_status,
                    adapterPosition
                )
            }
            // Used to handle the cross button
            itemView.btn_cross.setOnClickListener {
                clickListener.onAdapterItemClick(itemView.btn_cross, adapterPosition)
            }
        }

    }

    override fun onRowMoved(fromPosition1: Int, toPosition1: Int): Boolean {
        val fromPosition = fromPosition1
        val toPosition = toPosition1
        val item = vehicleImagesList.removeAt(fromPosition)
        vehicleImagesList.add(toPosition, item)
        notifyItemMoved(fromPosition, toPosition)
        return true

    }

    override fun onRowSelected(myViewHolder: MyViewHolder?) {
        myViewHolder?.itemView?.img_selected_img?.setBackgroundResource(R.color.grey);
    }

    override fun onRowClear(myViewHolder: MyViewHolder?) {
        myViewHolder?.itemView?.img_selected_img?.setBackgroundResource(R.drawable.bg_round_corner);

    }


}