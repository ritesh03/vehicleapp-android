package com.zopdealer.ui.add_vehicle.model

import com.google.gson.annotations.SerializedName

data class ActiveSyndicationModel(
    @SerializedName("id")
    val id: String?,
    @SerializedName("value")
    val value: String?
)
