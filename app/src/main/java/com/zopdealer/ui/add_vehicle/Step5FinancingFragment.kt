package com.zopdealer.ui.add_vehicle

import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.Spanned
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.lifecycle.ViewModelProvider
import com.zopdealer.R
import com.zopdealer.base_classes.BaseFragment
import com.zopdealer.ui.add_vehicle.model.ActiveConfigurationModel
import com.zopdealer.ui.add_vehicle.viewmodel.AddEditVehicleViewModel
import com.zopdealer.utils.Constants
import com.zopdealer.utils.preferences.Preferences
import kotlinx.android.synthetic.main.fragment_step5_financing_addvehicle.*
import java.util.regex.Pattern


class Step5FinancingFragment : BaseFragment(), View.OnClickListener,
    AdapterView.OnItemSelectedListener {

    private lateinit var addVehicleViewModel: AddEditVehicleViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_step5_financing_addvehicle, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addVehicleViewModel =
            ViewModelProvider(requireActivity()).get(AddEditVehicleViewModel::class.java)

        //attachSpinnerAdater()
        setOnclickListener()
        spinnerListener()
        loadData()

        if (!addVehicleViewModel.isEditInventory) {
            addVehicleViewModel.activeConfiguration.observe(::getLifecycle) {
                it?.let { listActiveConfiguration ->
                   setActiveConfiguration(listActiveConfiguration)
                }
            }
        }

        switch_financing.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                tv_financing_status.text = "On"
            } else {
                tv_financing_status.text = "Off"
            }
        }

        switch_special_financing.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                cl_view.visibility = View.VISIBLE
                tv_special_financing_status.text = "On"
            } else {
                cl_view.visibility = View.GONE
                tv_special_financing_status.text = "Off"
            }
        }

        et_down_payment.filters = arrayOf(InputFilterMinMax(0f, 100f, 3, 2))
        et_interest_rate.filters = arrayOf(InputFilterMinMax(0f, 100f, 3, 2))

        /* et_down_payment.addTextChangedListener(object : TextWatcher {

             override fun afterTextChanged(s: Editable?) {

             }

             override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

             }
             override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                 if (before==0){
                     var  strEnteredVal=et_down_payment.text.toString()
                     if(!strEnteredVal.equals("")) {
                         var num = strEnteredVal.toFloat()
                         if ( num<=100.1) {
                             et_down_payment.setText(""+num)
                             //showSnackBar("You can not enter max value 100.1")
                         } else {
                            // var value=num.toInt()-1
                             et_down_payment.setText("")
                         }
                     }
                 }else{

                 }
             }
         })*/

    }

    private fun setActiveConfiguration(listActiveConfiguration: List<ActiveConfigurationModel>) {
        listActiveConfiguration.map {
            if (it.key == "vehicle_default_financing") {
                if (it.value == "0") {
                    switch_financing.isChecked = false
                    tv_financing_status.text = "Off"
                } else {
                    switch_financing.isChecked = true
                    tv_financing_status.text = "On"
                }
            } else if (it.key == "vehicle_default_downpayment") {
                et_down_payment.setText(it.value)
                //et_down_payment.setFilters(arrayOf<InputFilter>(MinMaxFilter("1", "99")))
            } else if (it.key == "vehicle_default_interest_rate") {
                et_interest_rate.setText(it.value)
            } else if (it.key == "vehicle_default_term") {
                tv_term_value.text = it.value
            } else if (it.key == "vehicle_default_payment_frequency") {
                et_payment_freq.text = it.value
            }
        }
    }

    private fun loadData() {

        if (addVehicleViewModel.financing.value .equals("1", true)) {
            tv_financing_status.text = "On"
            switch_financing.isChecked = true
        } else {
            tv_financing_status.text = "Off"
            switch_financing.isChecked = false
        }

        if (addVehicleViewModel.specialFinancing.value.equals("1", true)) {
            tv_special_financing_status.text = "On"
            switch_special_financing.isChecked = true
            cl_view.visibility = View.VISIBLE
        } else {
            tv_special_financing_status.text = "Off"
            switch_special_financing.isChecked = false
            cl_view.visibility = View.GONE
        }

        if (addVehicleViewModel.isEditInventory){
            et_down_payment.setText(addVehicleViewModel.downPayment.value)
            et_interest_rate.setText(addVehicleViewModel.interestRate.value)
            tv_term_value.text = addVehicleViewModel.term.value
            et_payment_freq.text = addVehicleViewModel.paymentFrequency.value
        }

    }

    private fun setOnclickListener() {
        btn_submit.setOnClickListener(this)
        et_payment_freq.setOnClickListener(this)
        tv_term_value.setOnClickListener(this)
    }

    private fun spinnerListener() {
        spinner_payment_freq.onItemSelectedListener = this
        spinner_term.onItemSelectedListener = this
    }

    fun attachSpinnerAdater() {
        spinnerArrayAdapter(spinner_payment_freq, R.array.spinner_payment_freq, requireContext())
    }

    override fun onClick(v: View?) {
        when (v) {
            btn_submit -> {
                hideKeyboard()
                addVehicleViewModel.downPayment.value = et_down_payment.text.toString().trim()
                addVehicleViewModel.interestRate.value = et_interest_rate.text.toString().trim()
                if (switch_special_financing.isChecked)
                    addVehicleViewModel.specialFinancing.value = "1"
                else
                    addVehicleViewModel.specialFinancing.value = "0"

                if (switch_financing.isChecked)
                    addVehicleViewModel.financing.value = "1"
                else
                    addVehicleViewModel.financing.value = "0"

                addVehicleViewModel.paymentFrequency.value = et_payment_freq.text.toString().trim()
                addVehicleViewModel.term.value = tv_term_value.text.toString().trim()

                AddVehicleActivity.tv_steps_count_tv.text = "Step 6 of 8"
                AddVehicleActivity.selectedPosition = 5
                val fragment = Step6DescriptionFragment()
                AddVehicleActivity.adapterStepsCount.notifyDataSetChanged()
                AddVehicleActivity.rv_steps_add_vehicle_rv.scrollToPosition(5)
                replaceFragment(fragment, false, R.id.add_vehicle_steps_container)

                if (addVehicleViewModel.isEditInventory) {
                    Preferences.prefs?.getString(
                            Constants.BASE_URL,
                            ""
                    )?.let {
                        addVehicleViewModel.editInventory(
                                it, addVehicleViewModel.inventoryId.value
                                ?: ""
                        )
                    }
                }
            }
            et_payment_freq -> {
                spinnerArrayAdapter(
                        spinner_payment_freq,
                        R.array.spinner_payment_freq,
                        requireContext()
                )
                spinner_payment_freq.performClick()
            }

            tv_term_value -> {
                val listTerm = ArrayList<String>()
                for (term in 0..96) {
                    listTerm.add(term.toString())
                }
                spinnerArrayAdapter(
                        spinner_term,
                        listTerm,
                        requireContext()
                )
                spinner_term.performClick()
            }
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val selectedItemText = parent?.getItemAtPosition(position) as String
        when (parent.id) {
            R.id.spinner_payment_freq -> {
                et_payment_freq.text = selectedItemText
            }
            R.id.spinner_term -> {
                tv_term_value.text = selectedItemText
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    internal class InputFilterMinMax(min: Float, max: Float, digitsBeforeZero: Int, digitsAfterZero: Int) : InputFilter {
        private var min: Float = 0f
        private var max: Float = 0f
        private var mPattern: Pattern

        init {
            this.min = min
            this.max = max
            mPattern = Pattern.compile("[0-9]{0," + (digitsBeforeZero-1) + "}+((\\.[0-9]{0," + (digitsAfterZero-1) + "})?)||(\\.)?")
        }

        override fun filter(
            source: CharSequence,
            start: Int,
            end: Int,
            dest: Spanned,
            dstart: Int,
            dend: Int
        ): CharSequence? {
            try {
                val input = (dest.toString() + source.toString()).toFloat()
                if (isInRange(min, max, input)) {
                    val matcher = mPattern.matcher(dest)
                    return if (!matcher.matches()) "" else null
                }
            } catch (nfe: NumberFormatException) {
                nfe.printStackTrace()
            }
            return ""
        }

        private fun isInRange(a: Float, b: Float, c: Float): Boolean {
            return if (b > a) c in a..b else c in b..a
        }
    }
}
