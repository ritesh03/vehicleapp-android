package com.zopdealer.ui.add_vehicle.model

import com.google.gson.annotations.SerializedName

class CreateInventorySuccessModel(
    val success: String?,
    @SerializedName("inventory_id")
    val inventoryId: Int?,
    @SerializedName("vin")
    val vin: String?=null,
     @SerializedName("stock_no")
     val stock_no: String?=null,
    @SerializedName("error")
     var error: String?

)
