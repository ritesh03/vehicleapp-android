package com.zopdealer.ui.add_vehicle.call_back

import androidx.core.view.size
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.zopdealer.ui.add_vehicle.adapter.VehiclePhotosAdapter


class ItemInteriorPhotoMoveCallback(private var mAdapter: ItemTouchHelperContract) :
    ItemTouchHelper.Callback() {

    var mDraggable = true

    override fun isLongPressDragEnabled(): Boolean {
        return true
    }

    override fun isItemViewSwipeEnabled(): Boolean {
        return false
    }


    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

    }

    override fun getMovementFlags(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ): Int {
        val dragFlags =
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT or ItemTouchHelper.UP or ItemTouchHelper.DOWN
        return makeMovementFlags(dragFlags, 0)
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return mAdapter.onRowMoved(viewHolder.adapterPosition, target.adapterPosition);
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {


        if (actionState !== ItemTouchHelper.ACTION_STATE_IDLE) {
            if (viewHolder is VehiclePhotosAdapter.MyViewHolder) {
                val myViewHolder: VehiclePhotosAdapter.MyViewHolder =
                    viewHolder as VehiclePhotosAdapter.MyViewHolder

                mAdapter!!.onRowSelected(myViewHolder)
            }
        }

        super.onSelectedChanged(viewHolder, actionState)

    }

    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        super.clearView(recyclerView, viewHolder)

        if (viewHolder is VehiclePhotosAdapter.MyViewHolder) {
            val myViewHolder: VehiclePhotosAdapter.MyViewHolder =
                viewHolder as VehiclePhotosAdapter.MyViewHolder
            mAdapter!!.onRowClear(myViewHolder)
        }

    }

    interface ItemTouchHelperContract {
        fun onRowMoved(fromPosition: Int, toPosition: Int): Boolean
        fun onRowSelected(myViewHolder: VehiclePhotosAdapter.MyViewHolder?)
        fun onRowClear(myViewHolder: VehiclePhotosAdapter.MyViewHolder?)
    }
}