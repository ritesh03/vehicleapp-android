package com.zopdealer.ui.add_vehicle.model

import android.graphics.Bitmap

data class InteriorPhotosModel(
    val status: Int,
    val image: Bitmap,
    val list_item_string: String
) {
}