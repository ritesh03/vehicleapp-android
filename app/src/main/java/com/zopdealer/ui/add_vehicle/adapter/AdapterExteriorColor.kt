package com.zopdealer.ui.add_vehicle.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zopdealer.R
import com.zopdealer.ui.add_vehicle.model.ColorModel
import kotlinx.android.synthetic.main.adapter_interior_colour.view.*


class AdapterExteriorColor(
    private val context: Context,
    private val arrayColors: ArrayList<ColorModel>
) :
    RecyclerView.Adapter<AdapterExteriorColor.MyViewHolder>() {

    var onColorClick: ((ColorModel) -> Unit)? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.adapter_interior_colour,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return arrayColors.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = arrayColors[position]
        (holder.itemView.btn_color.background as GradientDrawable).setColor(
            Color.parseColor(
                getColorCode(model.colorName)
            )
        )

        if (getColorCode(model.colorName) == "#FFFFFF") {
            holder.shape_oval.visibility = View.VISIBLE
            holder.iv_check.setColorFilter(Color.parseColor("#000000"))
        } else {
            holder.shape_oval.visibility = View.GONE
            holder.iv_check.setColorFilter(Color.parseColor("#FFFFFF"))
        }

        if (model.isSelected) {
            holder.iv_check.visibility = View.VISIBLE
        } else {
            holder.iv_check.visibility = View.GONE
        }

        holder.btn_color.setOnClickListener {
            onColorClick?.invoke(model)
        }
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val iv_check = view.iv_check;
        val btn_color = view.btn_color;
        val shape_oval = view.shape_oval;
    }

    private fun getColorCode(colorStr: String?): String {
        return when (colorStr) {
            "Black" -> "#000000"
            "Grey" -> "#939294"
            "Silver" -> "#C1C0C2"
            "White" -> "#FFFFFF"
            "Cream" -> "#EEE7D0"
            "Tan" -> "#D4B691"
            "Brown" -> "#974700"
            "Maroon" -> "#7F0E24"
            "Red" -> "#F92800"
            "Orange" -> "#F4A201"
            "Gold" -> "#F4B132"
            "Yellow" -> "#FDFB03"
            "Green" -> "#166500"
            "Teal" -> "#1F7D7E"
            "Blue" -> "#1E33FF"
            "Purple" -> "#7E91CB"
            "Beige" -> "#F5F5DC"
            "Instock" -> "#39DA8A"
            "Sold" -> "#FF5B5C"
            "Archive" -> "#FDAC41"
            "Unpublished" -> "#475F7B"
            "other" -> "#475F7B"
            else -> "#000000"
        }
    }
}