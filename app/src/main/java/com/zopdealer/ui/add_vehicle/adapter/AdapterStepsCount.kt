package com.zopdealer.ui.add_vehicle.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.zopdealer.R
import com.zopdealer.ui.add_vehicle.AddVehicleActivity
import com.zopdealer.ui.add_vehicle.AddVehicleActivity.Companion.selectedPosition
import kotlinx.android.synthetic.main.adapter_addvehicle_steps_count.view.*


class AdapterStepsCount(
    private val context: AppCompatActivity,
    private val stepsName: ArrayList<String>,
    supportFragmentManager: FragmentManager,
    val tvStepsCount: TextView

) : RecyclerView.Adapter<AdapterStepsCount.MyViewHolder>() {
    val conted = context;
    val supportFragmentManagerr = supportFragmentManager;
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        return MyViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.adapter_addvehicle_steps_count,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return stepsName.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.tv_step_name.text = stepsName.get(position)
        if (selectedPosition==position) {
            holder.tv_step_name.setBackgroundColor(Color.parseColor("#F05E45"))
            holder.tv_step_name.setTextColor(Color.parseColor("#FFFFFF"))
        }else{

            holder.tv_step_name.setBackgroundColor(Color.parseColor("#E5E2E2"))
            holder.tv_step_name.setTextColor(Color.parseColor("#9D9D9D"))
        }
        holder.tv_step_name.setOnClickListener {
            val addVehicleActivity = AddVehicleActivity();
            selectedPosition=position

            addVehicleActivity.onPressButton(
                stepsName.get(position),
                context,
                supportFragmentManagerr,
                tvStepsCount
            )

            notifyDataSetChanged()
        }
    }


    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_step_name = view.tv_step_name
    }

}