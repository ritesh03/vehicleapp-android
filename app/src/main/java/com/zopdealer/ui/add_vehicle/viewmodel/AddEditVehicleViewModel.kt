package com.zopdealer.ui.add_vehicle.viewmodel

import android.R
import android.app.Dialog
import android.view.Window
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.zopdealer.api.ApiService
import com.zopdealer.api.RetrofitClientInstance
import com.zopdealer.network.entities.ResponseError
import com.zopdealer.network.responses.Inventory
import com.zopdealer.ui.add_vehicle.model.*
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.security.auth.callback.Callback
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class AddEditVehicleViewModel : ViewModel() {

    private val _error: MutableLiveData<String> = MutableLiveData()
    private val _erroradd: MutableLiveData<String> = MutableLiveData()
    private val _vinResponse: MutableLiveData<VinModel> = MutableLiveData()
    private val _vehicleLocations: MutableLiveData<List<Locations>> = MutableLiveData()
    private val _activeConfiguration: MutableLiveData<List<ActiveConfigurationModel>> =
        MutableLiveData()
    private val _year: MutableLiveData<List<String>> = MutableLiveData()
    private val _make: MutableLiveData<List<String>> = MutableLiveData()
    private val _bodyType: MutableLiveData<List<String>> = MutableLiveData()
    private val _door: MutableLiveData<List<String>> = MutableLiveData()
    private val _cylinder: MutableLiveData<List<String>> = MutableLiveData()
    private val _transmission: MutableLiveData<List<String>> = MutableLiveData()
    private val _driveTrain: MutableLiveData<List<String>> = MutableLiveData()
    private val _passenger: MutableLiveData<List<String>> = MutableLiveData()
    private val _fuelType: MutableLiveData<List<String>> = MutableLiveData()
    private val _syndicationCompanies: MutableLiveData<List<SyndicationCompanies>> =
        MutableLiveData()
    private val _activeSyndication: MutableLiveData<List<ActiveSyndicationModel>> =
        MutableLiveData()
    private val _createInventorySuccess: MutableLiveData<CreateInventorySuccessModel> =
        MutableLiveData()

    val error: LiveData<String>
        get() = _error
    val erroradd: LiveData<String>
        get() = _erroradd

    val vinResponse: LiveData<VinModel>
        get() = _vinResponse

    val vehicleLocations: LiveData<List<Locations>>
        get() = _vehicleLocations

    val activeConfiguration: LiveData<List<ActiveConfigurationModel>>
        get() = _activeConfiguration

    val yearList: LiveData<List<String>>
        get() = _year

    val makeList: LiveData<List<String>>
        get() = _make

    val doorList: LiveData<List<String>>
        get() = _door

    val bodyTypeList: LiveData<List<String>>
        get() = _bodyType

    val cylinderList: LiveData<List<String>>
        get() = _cylinder

    val transmissionList: LiveData<List<String>>
        get() = _transmission

    val driveTrainList: LiveData<List<String>>
        get() = _driveTrain

    val passengerList: LiveData<List<String>>
        get() = _passenger

    val fuelTypeList: LiveData<List<String>>
        get() = _fuelType

    val syndicationCompanies: LiveData<List<SyndicationCompanies>>
        get() = _syndicationCompanies

    val activeSyndication: LiveData<List<ActiveSyndicationModel>>
        get() = _activeSyndication

    val createInventorySuccess: LiveData<CreateInventorySuccessModel>
        get() = _createInventorySuccess

    val inventoryId = MutableLiveData<String>()
    val vinNumber = MutableLiveData<String>()
    val vinDisplay = MutableLiveData<String>()
    val stockNumber = MutableLiveData<String>()
    val stockDate = MutableLiveData<String>()
    val status = MutableLiveData<String>()
    val subStatus = MutableLiveData<String>()
    val featureListing = MutableLiveData<String>()
    val vehicleType = MutableLiveData<String>()
    val visibility = MutableLiveData<String>()
    val vehicleTags = MutableLiveData<List<String>>()
    val location = MutableLiveData<String>()
    val title = MutableLiveData<String>()
    val year = MutableLiveData<String>()
    val make = MutableLiveData<String>()
    val model = MutableLiveData<String>()
    val trim = MutableLiveData<String>()
    val body = MutableLiveData<String>()
    val doors = MutableLiveData<String>()
    val cylinders = MutableLiveData<String>()
    val displacement = MutableLiveData<String>()
    val transmission = MutableLiveData<String>()
    val driveTrain = MutableLiveData<String>()
    val passengers = MutableLiveData<String>()
    val fuelType = MutableLiveData<String>()
    val odometer = MutableLiveData<String>()
    val kmMiles = MutableLiveData<String>()
    val exteriorColor = MutableLiveData<String>()
    val interiorColor = MutableLiveData<String>()
    val price = MutableLiveData<String>()
    val hidePrice = MutableLiveData<String>()
    val specialPrice = MutableLiveData<String>()
    val showStripe = MutableLiveData<String>()
    val specialPriceOn = MutableLiveData<String>()
    val specialPriceType = MutableLiveData<String>()
    val specialPriceDate = MutableLiveData<String>()
    val financing = MutableLiveData<String>()
    val specialFinancing = MutableLiveData<String>()
    val downPayment = MutableLiveData<String>()
    val interestRate = MutableLiveData<String>()
    val term = MutableLiveData<String>()
    val paymentFrequency = MutableLiveData<String>()
    val description = MutableLiveData<String>()
    val vehicleFeatures = MutableLiveData<List<String>>()
    val syndication = MutableLiveData<List<String>>()
    val fuelCity = MutableLiveData<String>()
    val fuelHwy = MutableLiveData<String>()
    val technicalSpecification = MutableLiveData<String>()
    val genericEquipment = MutableLiveData<String>()
    val optional = MutableLiveData<String>()
    val standard = MutableLiveData<String>()
    val vinCompany = MutableLiveData<String>()

    fun setInventoryData(inventory: Inventory?) {
        inventoryId.value = inventory?.inventory_id
        vinNumber.value = inventory?.vin
        vinDisplay.value = inventory?.vindisplay
        stockNumber.value = inventory?.stock_no
        if (isEditInventory && !inventory?.stock_date.isNullOrEmpty())
            stockDate.value =
                getAbbreviatedFromDateTime(inventory?.stock_date!!, "yyyy-MM-dd", "MM/dd/yyyy")
        else
            stockDate.value = inventory?.stock_date
        status.value = inventory?.status
        subStatus.value = inventory?.sub_status
        featureListing.value = inventory?.feature_listing
        vehicleType.value = inventory?.vehicle_type
        visibility.value = inventory?.visibility

        if (!inventory?.vehicle_tags.isNullOrEmpty()) {
            val listVehicleTags = ArrayList<String>()
            val jArray = JSONArray(inventory?.vehicle_tags)
            for (i in 0 until jArray.length()) {
                listVehicleTags.add(jArray.getString(i))
            }
            vehicleTags.value = listVehicleTags
        }
        location.value = inventory?.location
        title.value = inventory?.title
        year.value = inventory?.year
        make.value = inventory?.make
        model.value = inventory?.model
        trim.value = inventory?.trim
        body.value = inventory?.body_type
        doors.value = inventory?.door
        cylinders.value = inventory?.engine
        displacement.value = inventory?.litres
        transmission.value = inventory?.transmission
        driveTrain.value = inventory?.drivetrain
        passengers.value = inventory?.passengers
        fuelType.value = inventory?.fuel_type
        odometer.value = inventory?.odometer
        kmMiles.value = inventory?.km_miles
        exteriorColor.value = inventory?.exterior_color
        interiorColor.value = inventory?.interior_color
        price.value = inventory?.price
        hidePrice.value = inventory?.hide_price
        specialPrice.value = inventory?.special_price
        showStripe.value = inventory?.show_stripe
        specialPriceOn.value = inventory?.special_price_on
        specialPriceType.value = inventory?.special_price_type
        if (isEditInventory && !inventory?.special_price_date.isNullOrEmpty())
            specialPriceDate.value =
                getAbbreviatedFromDateTime(inventory?.special_price_date!!, "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm")
        else
            specialPriceDate.value = inventory?.special_price_date
        financing.value = inventory?.financing_on
        specialFinancing.value = inventory?.financing_special
        downPayment.value = inventory?.downpayment
        interestRate.value = inventory?.interest_rate
        term.value = inventory?.term
        paymentFrequency.value = inventory?.payment_frequency
        description.value = inventory?.vehicle_description
        if (!inventory?.custom_features.isNullOrEmpty()) {
            try {
                val listVehicleFeatures = ArrayList<String>()
                val jArrayFeatues = JSONArray(inventory?.custom_features)
                for (i in 0 until jArrayFeatues.length()) {
                    listVehicleFeatures.add(jArrayFeatues.getString(i))
                }
                vehicleFeatures.value = listVehicleFeatures
            }catch (e: Exception){

            }
        }
        if (!inventory?.syndication.isNullOrEmpty()) {
            val listSyndication = ArrayList<String>()
            val jArraySyndication = JSONArray(inventory?.syndication)
            for (i in 0 until jArraySyndication.length()) {
                listSyndication.add(jArraySyndication.getString(i))
            }
            syndication.value = listSyndication
        }
        fuelCity.value = inventory?.fuel_city
        fuelHwy.value = inventory?.fuel_hwy
        if (inventory?.technical_specification!=null){
            technicalSpecification.value = inventory?.technical_specification
        }
        if (inventory?.technical_specification!=null){
            technicalSpecification.value = inventory?.technical_specification
        }
        if (inventory?.generic_equipment!=null){
            genericEquipment.value = inventory?.generic_equipment
        }
        if (inventory?.optional!=null){
            optional.value = inventory?.optional
        }
        if (inventory?.standard!=null){
            standard.value = inventory?.standard
        }
        vinCompany.value = inventory?.vin_company
    }

    var isEditInventory: Boolean = false

    fun setIsEditInventory(editInventory: Boolean) {
        this.isEditInventory = editInventory
    }

    fun decodeVin(
            baseUrl: String,
            vinNumber: String?
    ) {
        val apiService =
            RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call = apiService?.decodeVin(vinNumber)
        call?.enqueue(object : Callback, retrofit2.Callback<VinModel> {
            override fun onFailure(call: Call<VinModel>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                    call: Call<VinModel>,
                    response: Response<VinModel>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        _vinResponse.postValue(it)
                    }
                } else {
                    try {
                        val responseError = Gson().fromJson(
                                response.errorBody()!!.string(),
                                ResponseError::class.java
                        )
                        _error.value = responseError.error
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })
    }

    fun getVehicleLocation(baseUrl: String) {
        val apiService =
            RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call = apiService?.getVehicleLocation()
        call?.enqueue(object : Callback, retrofit2.Callback<VehicleLocationModel> {
            override fun onFailure(call: Call<VehicleLocationModel>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                    call: Call<VehicleLocationModel>,
                    response: Response<VehicleLocationModel>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        _vehicleLocations.postValue(it.locations)
                    }
                } else {
                    try {
                        val responseError = Gson().fromJson(
                                response.errorBody()!!.string(),
                                ResponseError::class.java
                        )
                        _error.value = responseError.error
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })
    }

    fun getActiveConfiguration(baseUrl: String) {
        val apiService =
            RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call = apiService?.getActiveConfiguration()
        call?.enqueue(object : Callback, retrofit2.Callback<List<ActiveConfigurationModel>> {
            override fun onFailure(call: Call<List<ActiveConfigurationModel>>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                    call: Call<List<ActiveConfigurationModel>>,
                    response: Response<List<ActiveConfigurationModel>>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        _activeConfiguration.postValue(it)
                    }
                } else {
                    try {
                        val responseError = Gson().fromJson(
                                response.errorBody()!!.string(),
                                ResponseError::class.java
                        )
                        _error.value = responseError.error
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })
    }

    fun getYear(baseUrl: String) {
        val apiService =
            RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call = apiService?.getYear()
        call?.enqueue(object : Callback, retrofit2.Callback<List<String>> {
            override fun onFailure(call: Call<List<String>>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                    call: Call<List<String>>,
                    response: Response<List<String>>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        _year.postValue(it)
                    }
                } else {
                    try {
                        val responseError = Gson().fromJson(
                                response.errorBody()!!.string(),
                                ResponseError::class.java
                        )
                        _error.value = responseError.error
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })
    }

    fun getMake(baseUrl: String) {
        val apiService =
            RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call = apiService?.getMake()
        call?.enqueue(object : Callback, retrofit2.Callback<List<String>> {
            override fun onFailure(call: Call<List<String>>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                    call: Call<List<String>>,
                    response: Response<List<String>>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        _make.postValue(it)
                    }
                } else {
                    try {
                        val responseError = Gson().fromJson(
                                response.errorBody()!!.string(),
                                ResponseError::class.java
                        )
                        _error.value = responseError.error
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })
    }

    fun getBodyType(baseUrl: String) {
        val apiService =
            RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call = apiService?.getBodyType()
        call?.enqueue(object : Callback, retrofit2.Callback<List<String>> {
            override fun onFailure(call: Call<List<String>>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                    call: Call<List<String>>,
                    response: Response<List<String>>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        _bodyType.postValue(it)
                    }
                } else {
                    try {
                        val responseError = Gson().fromJson(
                                response.errorBody()!!.string(),
                                ResponseError::class.java
                        )
                        _error.value = responseError.error
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })
    }

    fun getDoor(baseUrl: String) {
        val apiService =
            RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call = apiService?.getDoor()
        call?.enqueue(object : Callback, retrofit2.Callback<List<String>> {
            override fun onFailure(call: Call<List<String>>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                    call: Call<List<String>>,
                    response: Response<List<String>>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        _door.postValue(it)
                    }
                } else {
                    try {
                        val responseError = Gson().fromJson(
                                response.errorBody()!!.string(),
                                ResponseError::class.java
                        )
                        _error.value = responseError.error
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })
    }

    fun getCylinder(baseUrl: String) {
        val apiService =
            RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call = apiService?.getCylinder()
        call?.enqueue(object : Callback, retrofit2.Callback<List<String>> {
            override fun onFailure(call: Call<List<String>>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                    call: Call<List<String>>,
                    response: Response<List<String>>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        _cylinder.postValue(it)
                    }
                } else {
                    try {
                        val responseError = Gson().fromJson(
                                response.errorBody()!!.string(),
                                ResponseError::class.java
                        )
                        _error.value = responseError.error
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })
    }

    fun getTransmission(baseUrl: String) {
        val apiService =
            RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call = apiService?.getTransmission()
        call?.enqueue(object : Callback, retrofit2.Callback<List<String>> {
            override fun onFailure(call: Call<List<String>>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                    call: Call<List<String>>,
                    response: Response<List<String>>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        _transmission.postValue(it)
                    }
                } else {
                    try {
                        val responseError = Gson().fromJson(
                                response.errorBody()!!.string(),
                                ResponseError::class.java
                        )
                        _error.value = responseError.error
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })
    }

    fun getDriveTrain(baseUrl: String) {
        val apiService =
            RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call = apiService?.getDriveTrain()
        call?.enqueue(object : Callback, retrofit2.Callback<List<String>> {
            override fun onFailure(call: Call<List<String>>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                    call: Call<List<String>>,
                    response: Response<List<String>>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        _driveTrain.postValue(it)
                    }
                } else {
                    try {
                        val responseError = Gson().fromJson(
                                response.errorBody()!!.string(),
                                ResponseError::class.java
                        )
                        _error.value = responseError.error
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })
    }

    fun getPassenger(baseUrl: String) {
        val apiService =
            RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call = apiService?.getPassenger()
        call?.enqueue(object : Callback, retrofit2.Callback<List<String>> {
            override fun onFailure(call: Call<List<String>>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                    call: Call<List<String>>,
                    response: Response<List<String>>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        _passenger.postValue(it)
                    }
                } else {
                    try {
                        val responseError = Gson().fromJson(
                                response.errorBody()!!.string(),
                                ResponseError::class.java
                        )
                        _error.value = responseError.error
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })
    }

    fun getFuelType(baseUrl: String) {
        val apiService =
            RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call = apiService?.getFuelType()
        call?.enqueue(object : Callback, retrofit2.Callback<List<String>> {
            override fun onFailure(call: Call<List<String>>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                    call: Call<List<String>>,
                    response: Response<List<String>>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        _fuelType.postValue(it)
                    }
                } else {
                    try {
                        val responseError = Gson().fromJson(
                                response.errorBody()!!.string(),
                                ResponseError::class.java
                        )
                        _error.value = responseError.error
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })
    }

    fun getSyndicationCompanies(baseUrl: String) {
        val apiService =
            RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call = apiService?.getSyndicationCompanies()
        call?.enqueue(object : Callback, retrofit2.Callback<SyndicationCompaniesModel> {
            override fun onFailure(call: Call<SyndicationCompaniesModel>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                    call: Call<SyndicationCompaniesModel>,
                    response: Response<SyndicationCompaniesModel>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        _syndicationCompanies.postValue(it.listSyndicationCompanies)
                    }
                } else {
                    try {
                        val responseError = Gson().fromJson(
                                response.errorBody()!!.string(),
                                ResponseError::class.java
                        )
                        _error.value = responseError.error
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })
    }

    fun getActiveSyndication(baseUrl: String) {
        val apiService =
            RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call = apiService?.getActiveSyndication()
        call?.enqueue(object : Callback, retrofit2.Callback<List<ActiveSyndicationModel>> {
            override fun onFailure(call: Call<List<ActiveSyndicationModel>>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                    call: Call<List<ActiveSyndicationModel>>,
                    response: Response<List<ActiveSyndicationModel>>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        _activeSyndication.postValue(it)
                    }
                } else {
                    try {
                        val responseError = Gson().fromJson(
                                response.errorBody()!!.string(),
                                ResponseError::class.java
                        )
                        _error.value = responseError.error
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })
    }

    private fun getPostData(): HashMap<String, String> {
        val objGson = Gson()
        val vehicleTagsArray = objGson.toJson(vehicleTags.value)
        val vehicleFeaturesArray = objGson.toJson(vehicleFeatures.value)
        val syndicationArray = objGson.toJson(syndication.value)

        val fields: HashMap<String, String> = HashMap()
        fields["vin"] = vinNumber.value ?: ""
        fields["vindisplay"] = vinDisplay.value ?: ""
        fields["stock_no"] = stockNumber.value ?: ""
        fields["stock_date"] = stockDate.value ?: ""
        fields["status"] = status.value ?: ""
        fields["sub_status"] = subStatus.value ?: ""
        fields["feature_listing"] = featureListing.value ?: ""
        fields["vehicle_type"] = vehicleType.value ?: ""
        fields["visibility"] = visibility.value ?: "1"
        fields["vehicle_tags"] = vehicleTagsArray
        fields["title"] = title.value ?: ""
        fields["year"] = year.value ?: ""
        fields["make"] = make.value ?: ""
        fields["model"] = model.value ?: ""
        fields["trim"] = trim.value ?: ""
        fields["body_type"] = body.value ?: ""
        fields["door"] = doors.value ?: ""
        fields["engine"] = cylinders.value ?: ""
        fields["litres"] = displacement.value ?: ""
        fields["transmission"] = transmission.value ?: ""
        fields["drivetrain"] = driveTrain.value ?: ""
        fields["passengers"] = passengers.value ?: ""
        fields["odometer"] = odometer.value ?: ""
        fields["km_miles"] = kmMiles.value ?: ""
        fields["fuel_type"] = fuelType.value ?: ""
        fields["fuel_city"] = fuelCity.value ?: ""
        fields["fuel_hwy"] = fuelHwy.value ?: ""
        fields["exterior_color"] = exteriorColor.value ?: ""
        fields["interior_color"] = interiorColor.value ?: ""
        fields["price"] = price.value ?: ""
        fields["hide_price"] = hidePrice.value ?: ""
        fields["special_price_on"] = specialPriceOn.value ?: ""
        fields["special_price"] = specialPrice.value ?: ""
        fields["special_price_type"] = specialPriceType.value ?: ""
        fields["special_price_date"] = specialPriceDate.value ?: ""
        fields["show_stripe"] = showStripe.value ?: ""
        fields["financing_on"] = financing.value ?: ""
        fields["financing_special"] = specialFinancing.value ?: ""
        fields["downpayment"] = downPayment.value ?: ""
        fields["interest_rate"] = interestRate.value ?: ""
        fields["term"] = term.value ?: ""
        fields["payment_frequency"] = paymentFrequency.value ?: ""
        fields["location"] = location.value ?: ""
        fields["vehicle_description"] = description.value ?: ""
        fields["custom_features"] = vehicleFeaturesArray
        fields["syndication"] = syndicationArray
        fields["vin_company"] = vinCompany.value ?: ""
        if (!standard.value.toString().equals("null")){
            fields["standard"] = standard.value ?: ""
        }
        if (!optional.value.toString().equals("null")){
            fields["optional"] = optional.value ?: ""
        }
        if (!technicalSpecification.value.toString().equals("null")){
            fields["technical_specification"] = technicalSpecification.value ?: ""
        }
        if (!genericEquipment.value.toString().equals("null")){
            fields["generic_equipment"] = genericEquipment.value ?: ""
        }

        return fields
    }

    fun addInventory(baseUrl: String, requireActivity: FragmentActivity) {
        val apiService =
            RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call = apiService?.createInventory(getPostData())
        call?.enqueue(object : Callback, retrofit2.Callback<CreateInventorySuccessModel> {

            override fun onFailure(call: Call<CreateInventorySuccessModel>, t: Throwable) {
                _erroradd.postValue(t.localizedMessage)
            }

            override fun onResponse(
                    call: Call<CreateInventorySuccessModel>,
                    response: Response<CreateInventorySuccessModel>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        _createInventorySuccess.postValue(it)
                    }

                } else {
                    try {
                        if (response.errorBody()?.toString()!=null){
                            _erroradd.value =""
                            Toast.makeText(requireActivity,response.errorBody()?.string(),Toast.LENGTH_LONG).show()
                        }else{
                            _erroradd.value = "Server error"
                        }
                       /* val responseError = Gson().fromJson(
                                response.errorBody()?.string(),
                                ResponseError::class.java
                        )
                        if (responseError != null)
                         _error.value = responseError.error
                        else
                        _error.value = "Server error"*/
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _erroradd.value = e.toString()
                    }
                }
            }
        })
    }

    fun editInventory(baseUrl: String, inventoryId: String, isUpdate: Boolean = false) {
        val apiService =
            RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call = apiService?.updateInventory(inventoryId, getPostData())
        call?.enqueue(object : Callback, retrofit2.Callback<CreateInventorySuccessModel> {
            override fun onFailure(call: Call<CreateInventorySuccessModel>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                    call: Call<CreateInventorySuccessModel>,
                    response: Response<CreateInventorySuccessModel>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        if (isUpdate)
                            _createInventorySuccess.postValue(it)
                    }
                } else {
                    try {
                        val responseError = Gson().fromJson(
                                response.errorBody()?.string(),
                                ResponseError::class.java
                        )
                        if (responseError != null)
                            _error.value = responseError.error
                        else
                            _error.value = "Server error"
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })
    }

    private fun getAbbreviatedFromDateTime(
            dateTime: String,
            dateFormat: String,
            field: String
    ): String? {
        val input = SimpleDateFormat(dateFormat, Locale.getDefault())
        val output = SimpleDateFormat(field, Locale.getDefault())
        try {
            val getAbbreviate = input.parse(dateTime)    // parse input
            return output.format(getAbbreviate)    // format output
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return null
    }
}

class AddEditVehicleViewModelFactory : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AddEditVehicleViewModel() as T
    }
}

