package com.zopdealer.ui.add_vehicle

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.zopdealer.R
import com.zopdealer.base_classes.BaseActivity
import com.zopdealer.network.responses.Inventory
import com.zopdealer.ui.add_vehicle.adapter.AdapterStepsCount
import com.zopdealer.ui.add_vehicle.interfaces.StepButtonListener
import kotlinx.android.synthetic.main.activity_add_vehicle.*


class AddVehicleActivity : BaseActivity(), StepButtonListener {
    var stepsName = ArrayList<String>()

    companion object {
        fun start(context: Context) {
            val starter = Intent(context, AddVehicleActivity::class.java)
            context.startActivity(starter)
        }

        fun start(context: Context, inventoryObj: Inventory) {
            val starter = Intent(context, AddVehicleActivity::class.java)
            starter.putExtra(EXTRA_INVENTORY_DATA, inventoryObj)
            starter.putExtra(EXTRA_IS_EDIT, true)
            context.startActivity(starter)
        }

        var selectedPosition = 0

        lateinit var adapterStepsCount: AdapterStepsCount

        lateinit var rv_steps_add_vehicle_rv: RecyclerView
        lateinit var tv_steps_count_tv: TextView

        const val EXTRA_INVENTORY_DATA = "EXTRA_INVENTORY_DATA"
        const val EXTRA_IS_EDIT = "EXTRA_IS_EDIT"

    }

    override fun getID(): Int {
        return R.layout.activity_add_vehicle
    }

    override fun iniView(savedInstanceState: Bundle?) {
        val bundle = Bundle()
        if (intent.getBooleanExtra(EXTRA_IS_EDIT, false)) {
            home_title_text.text = "Edit Vehicle"
            bundle.putSerializable(
                EXTRA_INVENTORY_DATA,
                intent.getSerializableExtra(EXTRA_INVENTORY_DATA)
            )
            bundle.putBoolean(EXTRA_IS_EDIT, true)
        }

        rv_steps_add_vehicle_rv = findViewById(R.id.rv_steps_add_vehicle)
        tv_steps_count_tv = findViewById(R.id.tv_steps_count)

        stepsName.add("General")
        stepsName.add("Vehicle Info")
        stepsName.add("Mileage")
        stepsName.add("Pricing")
        stepsName.add("Financing")
        stepsName.add("Description")
        /*stepsName.add("Photos")*/
        stepsName.add("Features")
        stepsName.add("Syndication")

        initViews()
        val step1 = Step1GeneralFragment();
        replaceFragment(step1, supportFragmentManager)
        step1.arguments = bundle


        btn_close.setOnClickListener {
            val alert = AskOption()
            alert.show()
        }
    }


    fun replaceFragment(
        fragment: Fragment,
        supportFragmentManager: FragmentManager
    ) {
        val tag: String = fragment::class.java.simpleName
        val fragmentObj: Fragment? = supportFragmentManager.findFragmentByTag(tag)
        val transaction = supportFragmentManager.beginTransaction()
        if (fragmentObj == null) {
            transaction.replace(R.id.add_vehicle_steps_container, fragment, tag)
                .addToBackStack(tag)
                .commitAllowingStateLoss()
        } else {
            transaction.replace(R.id.add_vehicle_steps_container, fragmentObj, tag)
                .addToBackStack(tag)
                .commitAllowingStateLoss()
        }
    }

    fun initViews() {
        adapterStepsCount = AdapterStepsCount(
            this@AddVehicleActivity,
            stepsName,
            supportFragmentManager,
            tv_steps_count
        )
        rv_steps_add_vehicle_rv.layoutManager = StaggeredGridLayoutManager(1, 1)
        rv_steps_add_vehicle_rv.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rv_steps_add_vehicle_rv.adapter = adapterStepsCount
    }

    override fun onPressButton(
        value: String,
        context: Context,
        supportFragmentManagerr: FragmentManager,
        tvStepsCount: TextView?
    ) {
        when (value) {
            "General" -> {
                val step1 = Step1GeneralFragment()
                replaceFragment(step1, supportFragmentManagerr)

                tvStepsCount?.text = "Step 1 of 8"
            }
            "Vehicle Info" -> {
                val step2 = Step2VehicleInfoFragment();
                replaceFragment(step2, supportFragmentManagerr)
                tvStepsCount?.text = "Step 2 of 8"
            }


            "Mileage" -> {
                val step3 = Step3MileageFragment();
                replaceFragment(step3, supportFragmentManagerr)
                tvStepsCount?.text = "Step 3 of 8"
            }
            "Pricing" -> {
                val step4 = Step4PricingFragment();
                replaceFragment(step4, supportFragmentManagerr)
                tvStepsCount?.text = "Step 4 of 8"
            }
            "Financing" -> {
                val step5 = Step5FinancingFragment();
                replaceFragment(step5, supportFragmentManagerr)
                tvStepsCount?.text = "Step 5 of 8"
            }
            "Description" -> {
                val step6 = Step6DescriptionFragment();
                replaceFragment(step6, supportFragmentManagerr)
                tvStepsCount?.text = "Step 6 of 8"
            }
            /*"Photos" -> {
                val step7 = Step7PhotosFragment();
                replaceFragment(step7, supportFragmentManagerr)
                tvStepsCount?.text = "Step 7 of 9"
            }*/
            "Features" -> {
                val step2 = Step8FeaturesFragment();
                replaceFragment(step2, supportFragmentManagerr)
                tvStepsCount?.text = "Step 7 of 8"
            }
            "Syndication" -> {
                val step2 = Step9SyndicationFragment();
                replaceFragment(step2, supportFragmentManagerr)
                tvStepsCount?.text = "Step 8 of 8"
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        selectedPosition = 0
    }

    fun replaceFragment(
        fragment: Fragment, addToBackStack: Boolean, id: Int
    ) {
        val tag: String = fragment::class.java.simpleName
        val fragmentManager = this.supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        transaction.replace(id, fragment, tag)
            .addToBackStack(null)
            .commitAllowingStateLoss()
    }

    override fun onBackPressed() {
        val fragments = supportFragmentManager.backStackEntryCount
        when {
            fragments == 1 -> {
                finish()
            }
            supportFragmentManager.backStackEntryCount > 1 -> {
                supportFragmentManager.popBackStack()
                changeTabs()
            }
            else -> {
                super.onBackPressed()
            }
        }
    }

    private fun changeTabs() {
        when (supportFragmentManager.getBackStackEntryAt(supportFragmentManager.backStackEntryCount - 2).name) {
            "Step1GeneralFragment" -> {
                tv_steps_count_tv.text = "Step 1 of 8"
                selectedPosition = 0
                adapterStepsCount.notifyDataSetChanged()
                rv_steps_add_vehicle_rv.scrollToPosition(0)
            }
            "Step2VehicleInfoFragment" -> {
                tv_steps_count_tv.text = "Step 2 of 8"
                selectedPosition = 1
                adapterStepsCount.notifyDataSetChanged()
                rv_steps_add_vehicle_rv.scrollToPosition(1)
            }
            "Step3MileageFragment" -> {
                tv_steps_count_tv.text = "Step 3 of 8"
                selectedPosition = 2
                adapterStepsCount.notifyDataSetChanged()
                rv_steps_add_vehicle_rv.scrollToPosition(2)
            }
            "Step4PricingFragment" -> {
                tv_steps_count_tv.text = "Step 4 of 8"
                selectedPosition = 3
                adapterStepsCount.notifyDataSetChanged()
                rv_steps_add_vehicle_rv.scrollToPosition(3)
            }
            "Step5FinancingFragment" -> {
                tv_steps_count_tv.text = "Step 5 of 8"
                selectedPosition = 4
                adapterStepsCount.notifyDataSetChanged()
                rv_steps_add_vehicle_rv.scrollToPosition(4)
            }
            "Step6DescriptionFragment" -> {
                tv_steps_count_tv.text = "Step 6 of 8"
                selectedPosition = 5
                adapterStepsCount.notifyDataSetChanged()
                rv_steps_add_vehicle_rv.scrollToPosition(5)
            }
            "Step8FeaturesFragment" -> {
                tv_steps_count_tv.text = "Step 7 of 8"
                selectedPosition = 6
                adapterStepsCount.notifyDataSetChanged()
                rv_steps_add_vehicle_rv.scrollToPosition(6)
            }
            "Step9SyndicationFragment" -> {
                tv_steps_count_tv.text = "Step 8 of 8"
                selectedPosition = 7
                adapterStepsCount.notifyDataSetChanged()
                rv_steps_add_vehicle_rv.scrollToPosition(7)
            }
        }
    }

    private fun AskOption(): AlertDialog {
        return AlertDialog.Builder(this)
            .setTitle("Zop Dealer")
            .setMessage("Are you sure you want to close?")
            .setPositiveButton("Yes") { dialog, whichButton ->

                finish()
                //your deleting code
                dialog.dismiss()
            }


            .setNegativeButton(
                "No"
            ) { dialog, which -> dialog.dismiss() }
            .create()

    }
}


