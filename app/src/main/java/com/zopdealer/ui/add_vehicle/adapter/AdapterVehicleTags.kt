package com.zopdealer.ui.add_vehicle.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zopdealer.R

class AdapterVehicleTags(context: Context, listTags: ArrayList<String>) :
    RecyclerView.Adapter<AdapterVehicleTags.MyViewHolder>() {
    var context: Context? = null
    private var listTags: ArrayList<String>

    init {
        this.context = context
        this.listTags = listTags
    }

    var onItemDelete: ((String) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_vehicle_tags, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return listTags.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = listTags[position]
        holder.textViewTag.text = model

        holder.imageViewDelete.setOnClickListener {
            onItemDelete?.invoke(model)
        }
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textViewTag: TextView = itemView.findViewById(R.id.tv_tag)
        var imageViewDelete: ImageView = itemView.findViewById(R.id.iv_delete)
    }
}