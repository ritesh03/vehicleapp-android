package com.zopdealer.ui.add_vehicle

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import androidx.lifecycle.ViewModelProvider
import com.zopdealer.R
import com.zopdealer.base_classes.BaseFragment
import com.zopdealer.ui.add_vehicle.viewmodel.AddEditVehicleViewModel
import com.zopdealer.utils.Constants
import com.zopdealer.utils.preferences.Preferences
import kotlinx.android.synthetic.main.fragment_step4_pricing_addvehicle.*
import java.text.SimpleDateFormat
import java.util.*

class Step4PricingFragment : BaseFragment() {

    var dateTimeString: String = ""
    private lateinit var addVehicleViewModel: AddEditVehicleViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_step4_pricing_addvehicle, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addVehicleViewModel =
            ViewModelProvider(requireActivity()).get(AddEditVehicleViewModel::class.java)

        spinnerArrayAdapter(spinner_offer_type, R.array.spinner_offer_type, requireContext())

        spinner_offer_type.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                selectedItemView: View?,
                i: Int,
                id: Long
            ) {
                val selectedItemText = adapterView?.getItemAtPosition(i) as? String
                addVehicleViewModel.specialPriceType.value = selectedItemText
                if (selectedItemText == "Forever") {
                    tv_stock_date.visibility = View.GONE
                    et_date_time.visibility = View.GONE
                } else {
                    tv_stock_date.visibility = View.VISIBLE
                    et_date_time.visibility = View.VISIBLE
                }
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) { // your code here

            }
        }

        switch_special_price.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                cl1.visibility = View.VISIBLE
            } else {
                cl1.visibility = View.INVISIBLE
            }
        }

        val dateSetListener =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                dateTimeString = updateDateInView(cal, "dd MMM yyyy")
                et_date_time.text = updateDateInView(cal, "MM/dd/yyyy")

                timePicker()
            }

        et_date_time.setOnClickListener {
            DatePickerDialog(
                requireContext(),
                dateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        btn_submit.setOnClickListener {
            hideKeyboard()
            addVehicleViewModel.price.value = et_price.text.toString().trim()
            addVehicleViewModel.specialPrice.value = et_special_price.text.toString().trim()
            if (switch_price.isChecked) {
                addVehicleViewModel.hidePrice.value = "1"
            } else {
                addVehicleViewModel.hidePrice.value = "0"
            }

            if (switch_special_price.isChecked) {
                addVehicleViewModel.specialPriceOn.value = "1"
            } else {
                addVehicleViewModel.specialPriceOn.value = "0"
            }

            if (cb_special_strip.isChecked) {
                addVehicleViewModel.showStripe.value = "1"
            } else {
                addVehicleViewModel.showStripe.value = "0"
            }

            AddVehicleActivity.tv_steps_count_tv.text = "Step 5 of 8"
            AddVehicleActivity.selectedPosition = 4
            val fragment = Step5FinancingFragment();
            AddVehicleActivity.adapterStepsCount.notifyDataSetChanged()
            AddVehicleActivity.rv_steps_add_vehicle_rv.scrollToPosition(4)
            replaceFragment(fragment, false, R.id.add_vehicle_steps_container)

            if (addVehicleViewModel.isEditInventory) {
                Preferences.prefs?.getString(
                    Constants.BASE_URL,
                    ""
                )?.let {
                    addVehicleViewModel.editInventory(
                        it,
                        addVehicleViewModel.inventoryId.value ?: ""
                    )
                }
            }
        }

        loadData()
    }

    private fun loadData() {
        et_price.setText(addVehicleViewModel.price.value)
        et_special_price.setText(addVehicleViewModel.specialPrice.value)
        switch_price.isChecked = addVehicleViewModel.hidePrice.value == "1"
        switch_special_price.isChecked = addVehicleViewModel.specialPriceOn.value == "1"
        cb_special_strip.isChecked = addVehicleViewModel.showStripe.value == "1"
        et_date_time.text = addVehicleViewModel.specialPriceDate.value

        if (addVehicleViewModel.specialPriceType.value.equals("Limited-Time", true)) {
            spinner_offer_type.setSelection(1)
        } else {
            spinner_offer_type.setSelection(0)
        }
    }

    private fun timePicker() {
        val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
            cal.set(Calendar.HOUR_OF_DAY, hour)
            cal.set(Calendar.MINUTE, minute)

            et_date_time.text =
                et_date_time.text.toString() + " " + SimpleDateFormat("HH:mm").format(cal.time)
                    .toString().toUpperCase()
            addVehicleViewModel.specialPriceDate.value = et_date_time.text.toString()
        }
        TimePickerDialog(
            requireContext(),
            timeSetListener,
            cal.get(Calendar.HOUR_OF_DAY),
            cal.get(Calendar.MINUTE),
            false
        ).show()
    }
}