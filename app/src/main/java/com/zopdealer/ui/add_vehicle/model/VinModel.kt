package com.zopdealer.ui.add_vehicle.model

import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName

data class VinModel(
    @SerializedName("vin_company")
    val vinCompany: String?,
    @SerializedName("vin")
    val vin: String?,
    @SerializedName("stock_no")
    val stock_no: String?,
    @SerializedName("year")
    val year: String?,
    @SerializedName("make")
    val make: String?,
    @SerializedName("model")
    val model: String?,
    @SerializedName("trim")
    val trim: List<String>?,
    @SerializedName("door")
    val door: List<String>?,
    @SerializedName("drivetrain")
    val driveTrain: List<String>?,
    @SerializedName("body_type")
    val bodyType: String?,
    @SerializedName("stock_image")
    val stockImage: List<String>?,
    @SerializedName("engine")
    val engine: List<String>?,
    @SerializedName("litres")
    val litres: List<String>?,
    @SerializedName("transmission")
    val transmission: List<String>?,
    @SerializedName("fuel_type")
    val fuelType: List<String>?,
    @SerializedName("fuel_city")
    val fuelCity: List<String>?,
    @SerializedName("fuel_hwy")
    val fuelHwy: List<String>?,
    @SerializedName("error")
    var error: String?,
    @SerializedName("passengers")
    var passengers: String?,
    @SerializedName("technical_specification")
    var technicalSpecification: JsonObject?,
    @SerializedName("optional")
    var optional: JsonObject?,
    @SerializedName("standard")
    var standard: JsonObject?,
    @SerializedName("generic_equipment")
    var genericEquipment: JsonObject?
)
