package com.zopdealer.ui.add_vehicle.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zopdealer.R
import com.zopdealer.ui.add_vehicle.interfaces.SelectTagsCallBack
import com.zopdealer.ui.add_vehicle.model.TagsModel
import kotlinx.android.synthetic.main.adapter_custom_dialog_select_tags.view.*

class AdapterMultipleSelectList(
    private val context: Context,
    val arrayTitle: ArrayList<TagsModel>,
    val status: Int, val listener: SelectTagsCallBack
) :
    RecyclerView.Adapter<AdapterMultipleSelectList.MyViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {

        return MyViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.adapter_custom_dialog_select_tags,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return arrayTitle.size
    }

    override fun onBindViewHolder(holder: AdapterMultipleSelectList.MyViewHolder, position: Int) {
        holder.itemView.checkbox.setText(arrayTitle.get(position).text)
        holder.itemView.checkbox.setChecked(arrayTitle.get(position).isChecked)

        holder.itemView.checkbox.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                listener.onCheckBoxClick(
                    position,
                    arrayTitle.get(position).text,
                    true
                )
            }else{
                listener.onCheckBoxClick(
                    position,
                    arrayTitle.get(position).text,
                    false
                )
            }
        }

        holder.itemView.checkbox.setOnClickListener {
        }
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    }
}