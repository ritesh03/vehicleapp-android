package com.zopdealer.ui.add_vehicle

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.core.text.HtmlCompat
import androidx.lifecycle.ViewModelProvider
import com.zopdealer.R
import com.zopdealer.base_classes.BaseFragment
import com.zopdealer.ui.add_vehicle.model.ActiveConfigurationModel
import com.zopdealer.ui.add_vehicle.model.VinModel
import com.zopdealer.ui.add_vehicle.viewmodel.AddEditVehicleViewModel
import com.zopdealer.utils.Constants
import com.zopdealer.utils.preferences.Preferences
import kotlinx.android.synthetic.main.fragment_step1_general_addvehicle.*
import kotlinx.android.synthetic.main.fragment_step2_vechileinfo_addvehicle.*
import kotlinx.android.synthetic.main.fragment_step2_vechileinfo_addvehicle.btn_submit

class Step2VehicleInfoFragment : BaseFragment(), View.OnClickListener,
    AdapterView.OnItemSelectedListener {

    private lateinit var addVehicleViewModel: AddEditVehicleViewModel
    private var listYear = ArrayList<String>()
    private var listMake = ArrayList<String>()
    private var listDoor = ArrayList<String>()
    private var listBodyType = ArrayList<String>()
    private var listCylinder = ArrayList<String>()
    private var listTransmission = ArrayList<String>()
    private var listDriveTrain = ArrayList<String>()
    private var listPassenger = ArrayList<String>()
    private var listFuelType = ArrayList<String>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_step2_vechileinfo_addvehicle, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addVehicleViewModel =
            ViewModelProvider(requireActivity()).get(AddEditVehicleViewModel::class.java)
        Preferences.prefs?.getString(
            Constants.BASE_URL,
            ""
        )?.let {
            addVehicleViewModel.getYear(it)
            addVehicleViewModel.getMake(it)
            addVehicleViewModel.getDoor(it)
            addVehicleViewModel.getBodyType(it)
            addVehicleViewModel.getCylinder(it)
            addVehicleViewModel.getTransmission(it)
            addVehicleViewModel.getDriveTrain(it)
            addVehicleViewModel.getPassenger(it)
            addVehicleViewModel.getFuelType(it)
        }

        addVehicleViewModel.vinResponse.observe(::getLifecycle) {
            it?.let { vinResponse ->
                setVinData(vinResponse)
            }
        }
        if (!addVehicleViewModel.isEditInventory) {
            addVehicleViewModel.activeConfiguration.observe(::getLifecycle) {
                it?.let { listActiveConfiguration ->
                    setActiveConfiguration(listActiveConfiguration)
                }
            }
        }
        addVehicleViewModel.yearList.observe(::getLifecycle) {
            it?.let { listYear ->
                this.listYear = listYear as ArrayList<String>
            }
        }

        addVehicleViewModel.makeList.observe(::getLifecycle) {
            it?.let { listMake ->
                this.listMake = listMake as ArrayList<String>
            }
        }

        addVehicleViewModel.doorList.observe(::getLifecycle) {
            it?.let { listDoor ->
                this.listDoor = listDoor as ArrayList<String>
            }
        }

        addVehicleViewModel.bodyTypeList.observe(::getLifecycle) {
            it?.let { listBodyType ->
                this.listBodyType = listBodyType as ArrayList<String>
            }
        }

        addVehicleViewModel.cylinderList.observe(::getLifecycle) {
            it?.let { listCylinder ->
                this.listCylinder = listCylinder as ArrayList<String>
            }
        }

        addVehicleViewModel.transmissionList.observe(::getLifecycle) {
            it?.let { listTransmission ->
                this.listTransmission = listTransmission as ArrayList<String>
            }
        }

        addVehicleViewModel.driveTrainList.observe(::getLifecycle) {
            it?.let { listDriveTrain ->
                this.listDriveTrain = listDriveTrain as ArrayList<String>
            }
        }

        addVehicleViewModel.passengerList.observe(::getLifecycle) {
            it?.let { listPassenger ->
                this.listPassenger = listPassenger as ArrayList<String>
            }
        }

        addVehicleViewModel.fuelTypeList.observe(::getLifecycle) {
            it?.let { listFuelType ->
                this.listFuelType = listFuelType as ArrayList<String>
            }
        }

        addVehicleViewModel.error.observe(::getLifecycle) {
            it?.let {
                Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
            }
        }

        //attachSpinnerAdater()
        spinnerListener()
        setOnclickListener()

        loadData()
    }

    private fun loadData() {
        et_vehicle_title.setText(addVehicleViewModel.title.value)
        et_model.setText(addVehicleViewModel.model.value)
        et_year.text = addVehicleViewModel.year.value
        et_make_year.text = addVehicleViewModel.make.value
        et_trim.setText(addVehicleViewModel.trim.value)
        et_body.text = addVehicleViewModel.body.value
        et_door.text = addVehicleViewModel.doors.value
        et_cylinder.text = addVehicleViewModel.cylinders.value
        et_stock_numbedr.setText(addVehicleViewModel.displacement.value)
        et_transmission.text = addVehicleViewModel.transmission.value
        et_drive_train.text = addVehicleViewModel.driveTrain.value
        et_no_passengers.text = addVehicleViewModel.passengers.value
        et_fule_type.text = addVehicleViewModel.fuelType.value
    }

    private fun setVinData(vinResponse: VinModel) {
        et_year.text = vinResponse.year
        et_make_year.text = vinResponse.make
        et_model.setText(vinResponse.model)
        et_trim.setText(vinResponse.trim?.get(0))
        et_body.text = vinResponse.bodyType
        et_door.text = vinResponse.door?.get(0)
        et_cylinder.text = vinResponse.engine?.get(0)
        et_stock_numbedr.setText(vinResponse.litres?.get(0))
        et_transmission.text = vinResponse.transmission?.get(0)
        et_drive_train.text = vinResponse.driveTrain?.get(0)
        et_no_passengers.text = vinResponse.passengers
        et_fule_type.text = vinResponse.fuelType?.get(0)

        addVehicleViewModel.fuelCity.value = vinResponse.fuelCity?.get(0)
        addVehicleViewModel.fuelHwy.value = vinResponse.fuelHwy?.get(0)
        addVehicleViewModel.technicalSpecification.value = vinResponse.technicalSpecification.toString()
        addVehicleViewModel.genericEquipment.value = vinResponse.genericEquipment.toString()
        addVehicleViewModel.optional.value = vinResponse.optional.toString()
        addVehicleViewModel.standard.value = vinResponse.standard.toString()
        addVehicleViewModel.vinCompany.value = vinResponse.vinCompany
    }

    private fun setYearSpinner(listYear: List<String>) {
    }

    private fun setActiveConfiguration(listActiveConfiguration: List<ActiveConfigurationModel>) {
        listActiveConfiguration.map {
            if (it.key == "editor_default_title") {
                et_vehicle_title.setText(it.value?.let { letTitle ->
                    HtmlCompat.fromHtml(
                        letTitle,
                        HtmlCompat.FROM_HTML_MODE_LEGACY
                    )
                })
            }
        }
    }

    fun setOnclickListener() {
        et_year.setOnClickListener(this)
        btn_submit.setOnClickListener(this)
        et_make_year.setOnClickListener(this)
        et_body.setOnClickListener(this)
        et_door.setOnClickListener(this)
        et_cylinder.setOnClickListener(this)
        et_transmission.setOnClickListener(this)
        et_drive_train.setOnClickListener(this)
        et_no_passengers.setOnClickListener(this)
        et_fule_type.setOnClickListener(this)
    }

    fun attachSpinnerAdater() {
        spinnerArrayAdapter(spin_make_year, R.array.spinner_make_year, requireContext())
        spinnerArrayAdapter(spin_body, R.array.spinner_body, requireContext())
        spinnerArrayAdapter(spin_door, R.array.spinner_door, requireContext())
        spinnerArrayAdapter(spin_cylinder, R.array.spinner_vehicle_cylinder, requireContext())
        spinnerArrayAdapter(
            spin_transmission,
            R.array.spinner_vehicle_transmission,
            requireContext()
        )
        spinnerArrayAdapter(spin_drive_train, R.array.spinner_vehicle_drive_train, requireContext())
        spinnerArrayAdapter(
            spin_no_passengers,
            R.array.spinner_vehicle_no_passenger,
            requireContext()
        )
        spinnerArrayAdapter(spin_year, getYearList(), requireContext())
    }

    fun spinnerListener() {
        spin_make_year.setOnItemSelectedListener(this)
        spin_body.setOnItemSelectedListener(this)
        spin_door.setOnItemSelectedListener(this)
        spin_cylinder.setOnItemSelectedListener(this)
        spin_transmission.setOnItemSelectedListener(this)
        spin_drive_train.setOnItemSelectedListener(this)
        spin_no_passengers.setOnItemSelectedListener(this)
        spin_year.setOnItemSelectedListener(this)
        spin_fule_type.setOnItemSelectedListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            btn_submit -> {
                hideKeyboard()
                if (et_year.text.toString().trim().isEmpty()) {
                    showSnackBar(getString(R.string.error_empty_year))
                    return
                }else if (et_make_year.text.toString().trim().isEmpty()){
                    showSnackBar(getString(R.string.error_empty_make))
                    return
                }else if(et_model.text.toString().trim().isEmpty()){
                    showSnackBar(getString(R.string.error_empty_model))
                    return
                }
                addVehicleViewModel.title.value = et_vehicle_title.text.toString().trim()
                addVehicleViewModel.model.value = et_model.text.toString().trim()
                addVehicleViewModel.trim.value = et_trim.text.toString().trim()
                addVehicleViewModel.displacement.value = et_stock_numbedr.text.toString().trim()
                addVehicleViewModel.make.value = et_make_year.text.toString().trim()
                addVehicleViewModel.year.value = et_year.text.toString().trim()
                addVehicleViewModel.body.value = et_body.text.toString().trim()
                addVehicleViewModel.doors.value = et_door.text.toString().trim()
                addVehicleViewModel.cylinders.value = et_cylinder.text.toString().trim()
                addVehicleViewModel.transmission.value = et_transmission.text.toString().trim()
                addVehicleViewModel.driveTrain.value = et_drive_train.text.toString().trim()
                addVehicleViewModel.passengers.value = et_no_passengers.text.toString().trim()
                addVehicleViewModel.fuelType.value = et_fule_type.text.toString().trim()

                AddVehicleActivity.tv_steps_count_tv.text = "Step 3 of 8"
                AddVehicleActivity.selectedPosition = 2
                val fragment = Step3MileageFragment()
                AddVehicleActivity.adapterStepsCount.notifyDataSetChanged()
                AddVehicleActivity.rv_steps_add_vehicle_rv.scrollToPosition(2)
                replaceFragment(fragment, false, R.id.add_vehicle_steps_container)

                if(addVehicleViewModel.isEditInventory){
                    Preferences.prefs?.getString(
                        Constants.BASE_URL,
                        ""
                    )?.let {
                        addVehicleViewModel.editInventory(it, addVehicleViewModel.inventoryId.value ?: "")
                    }
                }
            }
            et_year -> {
                spinnerArrayAdapter(spin_year, listYear, requireContext())
                spin_year.performClick()
            }
            et_make_year -> {
                spinnerArrayAdapter(spin_make_year, listMake, requireContext())
                spin_make_year.performClick()
            }
            et_body -> {
                spinnerArrayAdapter(spin_body, listBodyType, requireContext())
                spin_body.performClick()
            }
            et_door -> {
                spinnerArrayAdapter(spin_door, listDoor, requireContext())
                spin_door.performClick()
            }
            et_cylinder -> {
                spinnerArrayAdapter(
                    spin_cylinder,
                    listCylinder,
                    requireContext()
                )
                spin_cylinder.performClick()
            }
            et_transmission -> {
                spinnerArrayAdapter(
                    spin_transmission,
                    listTransmission,
                    requireContext()
                )
                spin_transmission.performClick()
            }
            et_drive_train -> {
                spinnerArrayAdapter(
                    spin_drive_train,
                    listDriveTrain,
                    requireContext()
                )
                spin_drive_train.performClick()
            }
            et_no_passengers -> {
                spinnerArrayAdapter(
                    spin_no_passengers,
                    listPassenger,
                    requireContext()
                )
                spin_no_passengers.performClick()
            }
            et_fule_type -> {
                spinnerArrayAdapter(spin_fule_type, listFuelType, requireContext())
                spin_fule_type.performClick()
            }
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val selectedItemText = parent?.getItemAtPosition(position) as String
        when (parent.id) {
            R.id.spin_make_year -> {
                et_make_year.text = selectedItemText
            }
            R.id.spin_body -> {
                et_body.text = selectedItemText
            }
            R.id.spin_door -> {
                et_door.text = selectedItemText
            }
            R.id.spin_cylinder -> {
                et_cylinder.text = selectedItemText
            }
            R.id.spin_transmission -> {
                et_transmission.text = selectedItemText
            }
            R.id.spin_drive_train -> {
                et_drive_train.text = selectedItemText
            }
            R.id.spin_no_passengers -> {
                et_no_passengers.text = selectedItemText
            }
            R.id.spin_year -> {
                et_year.text = selectedItemText
            }
            R.id.spin_fule_type -> {
                et_fule_type.text = selectedItemText
            }
        }
    }

    fun getYearList(): ArrayList<String> {
        return listYear
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

}