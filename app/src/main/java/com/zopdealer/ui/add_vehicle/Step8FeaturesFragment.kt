package com.zopdealer.ui.add_vehicle

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.zopdealer.R
import com.zopdealer.base_classes.BaseFragment
import com.zopdealer.ui.add_vehicle.adapter.AdapterVehicleTags
import com.zopdealer.ui.add_vehicle.viewmodel.AddEditVehicleViewModel
import com.zopdealer.utils.Constants
import com.zopdealer.utils.preferences.Preferences
import kotlinx.android.synthetic.main.fragment_step8_features_addvehicle.*
import org.json.JSONArray

class Step8FeaturesFragment : BaseFragment() {

    private lateinit var adapterVehicleTags: AdapterVehicleTags
    private val listVehicleFeatures = ArrayList<String>()
    private lateinit var addVehicleViewModel: AddEditVehicleViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_step8_features_addvehicle, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addVehicleViewModel =
            ViewModelProvider(requireActivity()).get(AddEditVehicleViewModel::class.java)

        adapterVehicleTags = AdapterVehicleTags(requireContext(), listVehicleFeatures)
        rv_vehicle_features.adapter = adapterVehicleTags
        adapterVehicleTags.onItemDelete = {
            listVehicleFeatures.remove(it)
            adapterVehicleTags.notifyDataSetChanged()
            if (adapterVehicleTags.itemCount == 0) {
                rv_vehicle_features.visibility = View.GONE
            }
        }

        tv_add.setOnClickListener {
            if (et_vehicle_feature.text.toString().trim().isEmpty()) {
                showSnackBar(getString(R.string.error_empty_vehicle_feature))
            } else {
                hideKeyboard()
                listVehicleFeatures.add(et_vehicle_feature.text.toString().trim())
                adapterVehicleTags.notifyDataSetChanged()
                et_vehicle_feature.setText("")
                if (adapterVehicleTags.itemCount > 0) {
                    rv_vehicle_features.visibility = View.VISIBLE
                }
            }
        }

        btn_submit.setOnClickListener {
            hideKeyboard()
            addVehicleViewModel.vehicleFeatures.value = listVehicleFeatures

            AddVehicleActivity.tv_steps_count_tv.text = "Step 8 of 8"
            AddVehicleActivity.selectedPosition = 7
            val fragment = Step9SyndicationFragment()
            AddVehicleActivity.adapterStepsCount.notifyDataSetChanged()
            AddVehicleActivity.rv_steps_add_vehicle_rv.scrollToPosition(7)
            replaceFragment(fragment, false, R.id.add_vehicle_steps_container)

            if(addVehicleViewModel.isEditInventory){
                Preferences.prefs?.getString(
                    Constants.BASE_URL,
                    ""
                )?.let {
                    addVehicleViewModel.editInventory(it, addVehicleViewModel.inventoryId.value ?: "")
                }
            }
        }

        loadData()
    }

    private fun loadData() {
        if (addVehicleViewModel.vehicleFeatures.value != null) {
            val jsonArray = JSONArray(addVehicleViewModel.vehicleFeatures.value)
            for (i in 0 until jsonArray.length()) {
                listVehicleFeatures.add(jsonArray.getString(i))
            }
            val hashSet = HashSet<String>()
            hashSet.addAll(listVehicleFeatures)
            listVehicleFeatures.clear()
            listVehicleFeatures.addAll(hashSet)

            adapterVehicleTags.notifyDataSetChanged()
            if (adapterVehicleTags.itemCount > 0) {
                rv_vehicle_features.visibility = View.VISIBLE
            }
        }
    }
}