package com.zopdealer.ui.add_vehicle.interfaces

import android.content.Context
import android.widget.TextView
import androidx.fragment.app.FragmentManager

interface StepButtonListener {

    fun onPressButton(
        value: String,
        context: Context,
        supportFragmentManagerr: FragmentManager,
        tvStepsCount: TextView?
    );
}