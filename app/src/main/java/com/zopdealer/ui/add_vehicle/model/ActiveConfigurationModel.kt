package com.zopdealer.ui.add_vehicle.model

import com.google.gson.annotations.SerializedName

data class ActiveConfigurationModel(
    @SerializedName("id")
    val id: String?,
    @SerializedName("code_scope")
    val codeScope: String?,
    @SerializedName("label")
    val label: String?,
    @SerializedName("key")
    val key: String?,
    @SerializedName("value")
    val value: String?,
    @SerializedName("type")
    val type: String?,
    @SerializedName("active")
    val active: String?
)
