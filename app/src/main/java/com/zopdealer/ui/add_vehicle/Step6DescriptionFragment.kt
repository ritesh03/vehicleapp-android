package com.zopdealer.ui.add_vehicle

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.lifecycle.ViewModelProvider
import com.zopdealer.R
import com.zopdealer.base_classes.BaseFragment
import com.zopdealer.ui.add_vehicle.model.ActiveConfigurationModel
import com.zopdealer.ui.add_vehicle.viewmodel.AddEditVehicleViewModel
import com.zopdealer.utils.Constants
import com.zopdealer.utils.preferences.Preferences
import kotlinx.android.synthetic.main.fragment_step2_vechileinfo_addvehicle.*
import kotlinx.android.synthetic.main.fragment_step6_description_addvehicle.*
import kotlinx.android.synthetic.main.fragment_step6_description_addvehicle.btn_submit

class Step6DescriptionFragment : BaseFragment() {

    private lateinit var addVehicleViewModel: AddEditVehicleViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_step6_description_addvehicle, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addVehicleViewModel =
            ViewModelProvider(requireActivity()).get(AddEditVehicleViewModel::class.java)

        editText3.setText(addVehicleViewModel.description.value)

        if (!addVehicleViewModel.isEditInventory) {
            addVehicleViewModel.activeConfiguration.observe(::getLifecycle) {
                it?.let { listActiveConfiguration ->
                    setActiveConfiguration(listActiveConfiguration)
                }
            }
        }


        btn_submit.setOnClickListener {
            hideKeyboard()
            addVehicleViewModel.description.value = editText3.text.toString().trim()

            AddVehicleActivity.tv_steps_count_tv.text = "Step 7 of 8"
            AddVehicleActivity.selectedPosition = 6
            val fragment = Step8FeaturesFragment()
            AddVehicleActivity.adapterStepsCount.notifyDataSetChanged()
            AddVehicleActivity.rv_steps_add_vehicle_rv.scrollToPosition(6)
            replaceFragment(fragment, false, R.id.add_vehicle_steps_container)

            if(addVehicleViewModel.isEditInventory){
                Preferences.prefs?.getString(
                    Constants.BASE_URL,
                    ""
                )?.let {
                    addVehicleViewModel.editInventory(it, addVehicleViewModel.inventoryId.value ?: "")
                }
            }
        }
    }

    private fun setActiveConfiguration(listActiveConfiguration: List<ActiveConfigurationModel>) {
        listActiveConfiguration.map {
            if (it.key == "editor_default_vehicle_description") {
                editText3.setText(addVehicleViewModel.description.value ?: it.value?.let { letTitle ->
                    HtmlCompat.fromHtml(
                        letTitle.replace("%26amp;", "&")
                        .replace("26", "&")
                        .replace("And", "&")
                        .replace("%&amp;", "&"),
                        HtmlCompat.FROM_HTML_MODE_LEGACY
                    )
                })
            }
        }
    }
}