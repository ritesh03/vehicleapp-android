package com.zopdealer.ui.add_vehicle.interfaces

interface SelectTagsCallBack {
    fun onCheckBoxClick(position:Int,text:String,isChecked:Boolean)
}