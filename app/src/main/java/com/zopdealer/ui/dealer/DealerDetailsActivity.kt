package com.zopdealer.ui.dealer

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.auth0.android.Auth0
import com.auth0.android.Auth0Exception
import com.auth0.android.authentication.AuthenticationAPIClient
import com.auth0.android.authentication.AuthenticationException
import com.auth0.android.authentication.storage.CredentialsManager
import com.auth0.android.authentication.storage.SharedPreferencesStorage
import com.auth0.android.provider.AuthCallback
import com.auth0.android.provider.VoidCallback
import com.auth0.android.provider.WebAuthProvider
import com.auth0.android.result.Credentials
import com.zopdealer.R
import com.zopdealer.base_classes.BaseActivity
import com.zopdealer.ui.home.HomeActivity
import com.zopdealer.ui.login.LoginActivity
import com.zopdealer.utils.Constants
import com.zopdealer.utils.preferences.Preferences
import com.zopdealer.utils.preferences.saveValue
import kotlinx.android.synthetic.main.activity_dealer_details.*

class DealerDetailsActivity : BaseActivity() {

    companion object {
        fun start(context: Context) {
            val starter = Intent(context, DealerDetailsActivity::class.java)
            starter.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(starter)
        }
        val EXTRA_CLEAR_CREDENTIALS = "com.auth0.CLEAR_CREDENTIALS"
        var appstate:String=""
    }

    private var account: Auth0? = null

    private lateinit var viewModel: DealerViewModel

    private var manager: CredentialsManager? = null

    override fun getID(): Int {
        return R.layout.activity_dealer_details
    }

    override fun iniView(savedInstanceState: Bundle?) {
        account = Auth0(
                this.getString(R.string.com_auth0_client_id),
                this.getString(R.string.com_auth0_domain)
        )
        account?.isOIDCConformant = true
        account?.isLoggingEnabled = true
        
        val authentication = AuthenticationAPIClient(account!!)

        manager = CredentialsManager(authentication, SharedPreferencesStorage(this))

        //Check if the activity was launched to log the user out
       /* if (intent.getBooleanExtra(EXTRA_CLEAR_CREDENTIALS, false)) {
            logout()
        }*/

        val loggedIn = manager?.hasValidCredentials() ?: false

        Log.e(TAG, " ------ logged In = $loggedIn")

      /*  if (!Preferences.prefs?.getString(Constants.DEALER_ID, "0").isNullOrBlank()) {
            et_dealer_id.setText(Preferences.prefs?.getString(Constants.DEALER_ID, ""))
        }
        if (!Preferences.prefs?.getString(Constants.DEALER_DOMAIN, "0").isNullOrBlank()) {
            et_dealer_domain.setText(Preferences.prefs?.getString(Constants.DEALER_DOMAIN, ""))
        }*/

        viewModel = ViewModelProviders.of(
                this,
                DealerViewModelFactory()
        )[DealerViewModel::class.java]

        viewModel.connectionId.observe(::getLifecycle) {
            showLoading(false)
            it?.let {
                //live base url
                Preferences.prefs?.saveValue(
                        Constants.BASE_URL,
                        "https://" + et_dealer_domain.text.toString().trim() + ".zopsoftware.com/"
                )

                //Test base url
             /*   Preferences.prefs?.saveValue(
                        Constants.BASE_URL,
                        "https://" + et_dealer_domain.text.toString().trim() + ".zopdms.com/"
                )*/
             /*   if (Preferences.prefs?.getString(Constants.BASE_URL,"")?.contains("zopsoftware.com")!!){
                    appstate="live"
                }else{
                    appstate="test"
                }*/
                appstate= it.connection_id.toString()


                LoginActivity.start(this)
               /* if (!loggedIn)
                    initAuth0andGetToken(it)*/
            }
        }

        viewModel.error.observe(::getLifecycle) {
            showLoading(false)
            it?.let {
                Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun onClickDealerDetail(view: View) {
        when (view) {
            btn_submit -> {

                if (et_dealer_domain.text.toString().isBlank()) {
                    showSnackBar(
                            getString(R.string.domain_empty),
                            findViewById(android.R.id.content)
                    )
                } else {
                    showLoading(true)
                    Preferences.prefs?.saveValue(
                            Constants.DEALER_DOMAIN,
                            et_dealer_domain.text.toString()
                    )
                    //live connection url
                    viewModel.getConnectionId(
                            "https://" + et_dealer_domain.text.toString()
                                    .trim() + ".zopsoftware.com/"

                            //test connection url
                                    /*viewModel.getConnectionId(
                                    "https://" + et_dealer_domain.text.toString()
                                            .trim() + ".zopdms.com//"*/
                    )
                }
            }
        }
    }

    private fun initAuth0andGetToken(connectionId: String) {
        val authCallback: AuthCallback = object : AuthCallback {
            override fun onFailure(dialog: Dialog) {
                Log.e(TAG, " ------ onFailure, dialog = $dialog")
                // Show error Dialog to user
                if (!dialog.isShowing)
                    dialog.show()
            }

            override fun onFailure(exception: AuthenticationException) {
                Log.e(TAG, " ------ onFailure, exception = $exception")
                // Show error to user
//                manager?.clearCredentials()
//                DealerDetailsActivity.start(this@DealerDetailsActivity)
            }

            override fun onSuccess(credentials: Credentials) {
                Log.e(TAG, " ------ onSuccess, credentials = $credentials")
                manager?.saveCredentials(credentials)
                Preferences.prefs?.saveValue(Constants.USER_TOKEN, credentials.idToken)
                // Store credentials
                // Navigate to your main activity
                HomeActivity.start(this@DealerDetailsActivity)
                finish()
            }
        }

        account?.let {
            WebAuthProvider.login(it)
//                .withRedirectUri("https://zopdealer.auth0.com/android/com.zopdealer/callback")
                .withScheme("demo")
                .withScope("openid profile email offline_access read:current_user update:current_user_metadata")
                .withAudience("https://zopdealer.auth0.com/userinfo")
                .withConnection(connectionId)
                .start(this, authCallback)
        }

       /*     manager?.getCredentials(object :
                BaseCallback<Credentials?, CredentialsManagerException?> {
                override fun onSuccess(credentials: Credentials?) {
                    Log.e(TAG, " ------ onSuccess, credentials = $credentials")
                    //Use credentials
                    manager?.saveCredentials(credentials!!);

                }

                override fun onFailure(error: CredentialsManagerException) {
                    Log.e(TAG, " ------ onFailure, error = $error")
                    //No credentials were previously saved or they couldn't be refreshed
                }
            })*/
    }//initAuth0andGetToken

    private fun logout() {
        account?.let {
            WebAuthProvider.logout(it)
                .withScheme("demo")
                .start(this, object : VoidCallback {
                    override fun onSuccess(payload: Void?) {
                        Toast.makeText(this@DealerDetailsActivity, "Success", Toast.LENGTH_SHORT)
                                .show()
                    }

                    override fun onFailure(error: Auth0Exception) {
                        // Show error to user
                        Toast.makeText(
                                this@DealerDetailsActivity,
                                "Error " + error.message,
                                Toast.LENGTH_SHORT
                        ).show()
                       /* HomeActivity.start(this@DealerDetailsActivity)
                        finish()*/
                    }
                })
        }

    }
}