package com.zopdealer.ui.dealer

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.zopdealer.api.ApiService
import com.zopdealer.api.RetrofitClientInstance
import com.zopdealer.network.entities.ResponseError
import com.zopdealer.network.responses.ConnectionResponse
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback

class DealerViewModel : ViewModel() {
    private val _error: MutableLiveData<String> = MutableLiveData()
    private val _connectionId: MutableLiveData<ConnectionResponse> = MutableLiveData()

    val error: LiveData<String>
        get() = _error

    val connectionId: LiveData<ConnectionResponse>
        get() = _connectionId

    fun getConnectionId(baseUrl: String) {
        val apiService = RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call = apiService?.getConnectionId()
        call?.enqueue(object : Callback, retrofit2.Callback<ConnectionResponse> {
            override fun onFailure(call: Call<ConnectionResponse>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                call: Call<ConnectionResponse>,
                response: Response<ConnectionResponse>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        _connectionId.postValue(it)
                    }
                } else {
                    try {
                        val responseError = Gson().fromJson(
                            response.errorBody()!!.string(),
                            ResponseError::class.java
                        )
                        _error.value = "responseError.message"
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })
    }
}

class DealerViewModelFactory :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DealerViewModel() as T
    }
}