package com.zopdealer.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel : ViewModel() {
    private val _error: MutableLiveData<String> = MutableLiveData()

    val error: LiveData<String>
        get() = _error
}
