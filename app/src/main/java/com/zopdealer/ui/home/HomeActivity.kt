package com.zopdealer.ui.home

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.zopdealer.R
import com.zopdealer.application.Application.AppContext.getContext
import com.zopdealer.base_classes.BaseActivity
import com.zopdealer.ui.add_vehicle.AddVehicleActivity
import com.zopdealer.ui.filter.FilterActivity
import com.zopdealer.ui.filter.FilterStatus
import com.zopdealer.ui.filter.InventorySort
import com.zopdealer.ui.inventory.InventoryFragment
import com.zopdealer.ui.inventory.search_inventory.ActivitySearchInventory
import com.zopdealer.ui.profile.ProfileFragment
import kotlinx.android.synthetic.main.activity_home.*
import java.io.Serializable

class HomeActivity : BaseActivity() {
    //
//    override val kodein by kodein()
//    private val factory: HomeViewModelFactory by instance()
//
    private lateinit var viewModel: HomeViewModel

    private lateinit var inventoryFragment: InventoryFragment

    private var filterStatusList: List<FilterStatus>? = null

    private var inventorySort: InventorySort? = null

    private var sortItemPosition: Int? = 0

    companion object {
        fun start(context: Context) {
            val starter = Intent(context, HomeActivity::class.java)
            starter.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(starter)
        }

        var btn_edit_activity: ImageView? = null
        var text_edit_activity: TextView? = null

        const val REQUEST_CODE_FILTER = 101
    }

    override fun getID(): Int {
        return R.layout.activity_home
    }

    override fun iniView(savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(
            this,
            HomeViewModelFactory()
        )[HomeViewModel::class.java]

        viewModel.error.observe(::getLifecycle) {
            showLoading(false)
            it?.let {
                Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
            }
        }

        btn_edit_activity = this.btn_edit
        text_edit_activity = this.text_edit
        checkAndRequestPermissions()
        inventoryFragment = InventoryFragment()
        replaceFragment(inventoryFragment)
        btn_search.visibility = View.VISIBLE
        btn_filter.visibility = View.VISIBLE
        home_title_text.text = "All Inventory"
    }

    fun onHomeActivityClick(v: View) {
        when (v) {

            btn_inventory -> {
                val inventoryFragment = InventoryFragment();
                replaceFragment(inventoryFragment)
                btn_search.visibility = View.VISIBLE
                btn_filter.visibility = View.VISIBLE
                btn_edit.visibility = View.GONE
                text_edit_activity?.visibility = View.GONE
                home_title_text.text = "All Inventory"

                tv_inventory.setTextColor(resources.getColor(R.color.redTextColor))
                btn_inventory.setColorFilter(
                    ContextCompat.getColor(
                        getContext(),
                        R.color.redTextColor
                    )
                )

                btn_profile.setColorFilter(ContextCompat.getColor(getContext(), R.color.darkgrey2));
                tv_profile.setTextColor(resources.getColor(R.color.darkgrey2))
            }
            btn_profile -> {
                val profileFragment = ProfileFragment()
                replaceFragment(profileFragment)
                btn_search.visibility = View.GONE
                btn_filter.visibility = View.GONE
                btn_edit.visibility = View.GONE
                home_title_text.text = "Profile"
                tv_profile.setTextColor(resources.getColor(R.color.redTextColor))

                btn_profile.setColorFilter(
                    ContextCompat.getColor(
                        this,
                        R.color.redTextColor
                    )
                );
                btn_inventory.setColorFilter(
                    ContextCompat.getColor(
                        this,
                        R.color.darkgrey2
                    )
                );

                tv_inventory.setTextColor(resources.getColor(R.color.darkgrey2))
            }
            btn_add -> {
                AddVehicleActivity.start(this)
            }
            btn_filter -> {
                val intent = Intent(this, FilterActivity::class.java)
                intent.putExtra(
                    FilterActivity.EXTRA_STATUS_FILTERS,
                    filterStatusList as? Serializable
                )
                intent.putExtra(FilterActivity.EXTRA_SORT_FILTERS, inventorySort)
                intent.putExtra(FilterActivity.EXTRA_SORT_FILTER_POSITION, sortItemPosition)
                startActivityForResult(intent, REQUEST_CODE_FILTER)
            }
            btn_search -> {
                ActivitySearchInventory.start(this)
            }
        }
    }

    fun replaceFragment(fragment: Fragment) {
        val tag: String = fragment::class.java.simpleName
        val transaction = supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(
            R.anim.fade_in,
            R.anim.fade_out
        )
        transaction.replace(R.id.home_container, fragment, tag).commitAllowingStateLoss()
    }

    private val REQUEST_ID_MULTIPLE_PERMISSIONS = 3

    private fun checkAndRequestPermissions(): Boolean {
        val camerapermission =
            ContextCompat.checkSelfPermission(this!!, Manifest.permission.CAMERA)
        val writepermission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        val readpermission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )
        val listPermissionsNeeded = java.util.ArrayList<String>()

        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        }

        if (listPermissionsNeeded.isNotEmpty()) {
            ActivityCompat.requestPermissions(
                this,
                listPermissionsNeeded.toTypedArray(),
                REQUEST_ID_MULTIPLE_PERMISSIONS
            )
            return false
        } else {

        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_FILTER) {
            if (resultCode == Activity.RESULT_OK) {
                val filterStatus =
                    data?.getSerializableExtra(FilterActivity.EXTRA_STATUS_FILTERS) as? List<*>
                filterStatusList = filterStatus?.filterIsInstance<FilterStatus>()
                    .takeIf { it?.size == filterStatus?.size }
                inventorySort =
                    data?.getSerializableExtra(FilterActivity.EXTRA_SORT_FILTERS) as? InventorySort
                sortItemPosition = data?.getIntExtra(FilterActivity.EXTRA_SORT_FILTER_POSITION, 0)
                inventoryFragment.setFilters(filterStatusList, inventorySort)
            }
        }
    }


}//HomeActivity