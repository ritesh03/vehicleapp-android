package com.zopdealer.ui.filter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.zopdealer.R
import kotlinx.android.synthetic.main.adapter_addvehicle_steps_count.view.*

class AdapterDefaultFilter(
    private val context: FilterActivity,
    private val listItem: ArrayList<FilterStatus>
) : RecyclerView.Adapter<AdapterDefaultFilter.MyViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.adapter_addvehicle_steps_count,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = listItem[position]
        holder.tvStepName.text = model.status

        if (model.isSelected!!) {
            holder.tvStepName.setBackgroundColor(Color.parseColor("#F05E45"))
            holder.tvStepName.setTextColor(Color.parseColor("#FFFFFF"))
        } else {
            holder.tvStepName.setBackgroundColor(Color.parseColor("#E5E2E2"))
            holder.tvStepName.setTextColor(Color.parseColor("#9D9D9D"))
        }

        holder.tvStepName.setOnClickListener {
            model.isSelected = !model.isSelected!!
            notifyItemChanged(position)
            context.selectPotion(model.status, model.isSelected!!)

        }
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvStepName: TextView = view.tv_step_name
    }
}