package com.zopdealer.ui.filter

import java.io.Serializable

data class InventorySort(var column: String?, var order: String?) : Serializable