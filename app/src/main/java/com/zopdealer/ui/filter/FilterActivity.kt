package com.zopdealer.ui.filter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.zopdealer.R
import com.zopdealer.base_classes.BaseActivity
import kotlinx.android.synthetic.main.activity_filter.*

class FilterActivity : BaseActivity() {

    lateinit var adapterDefaultFilter: AdapterDefaultFilter

    private var defaultListItem = ArrayList<FilterStatus>()

    private var inventorySort: InventorySort? = null

    private var sortItemPosition: Int? = 0

    companion object {
        fun start(context: Context) {
            val starter = Intent(context, FilterActivity::class.java)
            context.startActivity(starter)
        }

        var selectedPosition = 0
        const val EXTRA_SORT_FILTERS = "EXTRA_SORT_FILTERS"
        const val EXTRA_STATUS_FILTERS = "EXTRA_STATUS_FILTERS"
        const val EXTRA_SORT_FILTER_POSITION = "EXTRA_SORT_FILTER_POSITION"
        var filterList:ArrayList<String>?= ArrayList<String>()
    }

    override fun getID(): Int {
        return R.layout.activity_filter
    }

    override fun iniView(savedInstanceState: Bundle?) {
        spinnerArrayAdapter(spinner_sort, R.array.spinner_sort)

        val filterStatus = intent?.getSerializableExtra(EXTRA_STATUS_FILTERS) as? List<*>
        val filterStatusList = filterStatus?.filterIsInstance<FilterStatus>().takeIf { it?.size == filterStatus?.size }

        inventorySort = intent?.getSerializableExtra(EXTRA_SORT_FILTERS) as? InventorySort
        sortItemPosition = intent?.getIntExtra(EXTRA_SORT_FILTER_POSITION, 0)

        if (inventorySort != null) {
            val sortItems = resources.getStringArray(R.array.spinner_sort)
            tv_spinner.text = sortItems[sortItemPosition!!]
            tv_spinner.visibility = View.VISIBLE
            spinner_sort.setSelection(sortItemPosition!!)
        }

        if (filterStatusList != null) {
            defaultListItem = (filterStatusList as? ArrayList<FilterStatus>)!!
        } else {
            defaultListItem.add(FilterStatus("Instock"))
            defaultListItem.add(FilterStatus("Sold"))
            defaultListItem.add(FilterStatus("Archive"))
            defaultListItem.add(FilterStatus("Unpublished"))
            defaultListItem.add(FilterStatus("Deal Pending"))
        }

        initViews()

        money_seekbar.setIndicatorTextFormat("$\${PROGRESS}")

        spinner_sort.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                selectedItemView: View,
                i: Int,
                id: Long
            ) {
                val selectedItemText = adapterView?.getItemAtPosition(i) as String
                sortItemPosition = i
                tv_spinner.text = selectedItemText
                tv_spinner.visibility = View.VISIBLE
                inventorySort = setSortFilter(selectedItemText)
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) { // your code here
                Log.d("NothingSelect", parentView?.toString() as String)
            }
        }

        tv_spinner.setOnClickListener {
            tv_spinner.visibility = View.GONE
            tv_spinner.text = ""
            spinnerArrayAdapter(spinner_sort, R.array.spinner_sort)
            spinner_sort.performClick()
        }
    }

    fun onClickFilterActivity(v: View) {
        when (v) {
            btn_back -> {
                finish()
            }
            btn_apply -> {
                val returnIntent = Intent()
                returnIntent.putExtra(EXTRA_SORT_FILTERS, inventorySort)
                returnIntent.putExtra(EXTRA_STATUS_FILTERS, defaultListItem)
                returnIntent.putExtra(EXTRA_SORT_FILTER_POSITION, sortItemPosition)
                setResult(Activity.RESULT_OK, returnIntent)
                finish()
            }
            btn_reset -> {
                 tv_spinner.text = "Select"
                tv_spinner.visibility = View.VISIBLE
                spinner_sort.setSelection(0)
                filterList?.clear()
                inventorySort?.column=null
                inventorySort?.order=null
                defaultListItem.forEach { it.isSelected = false }.also {
                    adapterDefaultFilter.notifyDataSetChanged()
                }
                //finish()
            }
        }
    }

    private fun initViews() {
        adapterDefaultFilter = AdapterDefaultFilter(
            this,
            defaultListItem
        )
        rv_dfault.layoutManager = StaggeredGridLayoutManager(1, 1)
        rv_dfault.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rv_dfault.adapter = adapterDefaultFilter
    }

    private fun spinnerArrayAdapter(spinner: Spinner, array: Int) {
        ArrayAdapter.createFromResource(
            this,
            array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }
    }

    private fun setSortFilter(selectedSort: String): InventorySort? {
        return when (selectedSort) {
            "Price: High to Low" -> InventorySort("price", "DESC")
            "Price: Low to High" -> InventorySort("price", "ASC")
            "Odometer: High to Low" -> InventorySort("odometer", "DESC")
            "Odometer: Low to High" -> InventorySort("odometer", "ASC")
            "Year: New to Old" -> InventorySort("year", "DESC")
            "Year: Old to New" -> InventorySort("year", "ASC")
            "Make: A to Z" -> InventorySort("make", "ASC")
            "Make: Z to A" -> InventorySort("make", "DESC")
            "Model: A to Z" -> InventorySort("model", "ASC")
            "Model: Z to A" -> InventorySort("model", "DESC")
            "Creation Date: New to Old" -> InventorySort("created_at", "DESC")
            "Creation Date: Old to New" -> InventorySort("created_at", "ASC")
            else -> null
        }
    }

    fun selectPotion(status: String?, selected: Boolean) {
        if(selected== false){
            filterList?.remove(status.toString())
        }else{
            if (filterList?.contains(status)!!){

            }else{
                filterList?.add(status.toString())
            }
        }


    }
}
