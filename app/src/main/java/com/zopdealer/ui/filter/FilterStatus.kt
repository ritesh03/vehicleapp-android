package com.zopdealer.ui.filter

import java.io.Serializable

data class FilterStatus(val status: String?, var isSelected: Boolean? = false) : Serializable