package com.zopdealer.ui.vehicle_detail.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.zopdealer.R
import com.zopdealer.ui.vehicle_detail.adapter.AdapterOptions.MyViewHolder
import kotlinx.android.synthetic.main.adapter_features.view.*

class AdapterOptions(
    private val context: Context,
    private val listItem: ArrayList<String>,
    supportFragmentManager: FragmentManager
):RecyclerView.Adapter<MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.adapter_features,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.tv_step_name.text=listItem.get(position)
    }
    class MyViewHolder(view: View):RecyclerView.ViewHolder(view) {
        val tv_step_name = view.tv_step_name

    }
}