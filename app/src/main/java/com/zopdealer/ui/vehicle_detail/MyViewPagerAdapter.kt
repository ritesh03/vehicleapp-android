package com.zopdealer.ui.vehicle_detail
import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.zopdealer.R
import kotlinx.android.synthetic.main.view_pager_custom_layout.view.*


class MyViewPagerAdapter(
    activity: Activity,
    private val images: ArrayList<Int>
) : PagerAdapter() {

    var inflater: LayoutInflater? = null;
    var context=activity

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater?
        val view = inflater!!.inflate(R.layout.view_pager_custom_layout, container, false)
        val imageView=view.imageView
        imageView.setImageResource(images.get(position));


        container.addView(view)
        return view
    }

    override fun getCount(): Int {
        return images.size
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        val v = obj as View
        container.removeView(v)
    }

    override fun isViewFromObject(v: View, obj: Any): Boolean {
        return v === obj
    }

}