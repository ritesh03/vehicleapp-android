package com.zopdealer.ui.vehicle_detail.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zopdealer.R
import kotlinx.android.synthetic.main.adapter_flags.view.*

class AdapterFlags(
    private val context: Context,
    private val listItem: ArrayList<String>
) : RecyclerView.Adapter<AdapterFlags.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.adapter_flags,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.tv_name.text = listItem.get(position)
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)
}