package com.zopdealer.ui.vehicle_detail.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.zopdealer.R
import com.zopdealer.ui.image_view.ImageViewerActivity
import kotlinx.android.synthetic.main.adapter_vehicle_detail_photos.view.*

class AdapterVehicleDetailPhoto(var context: Context, private val status: Int) :
RecyclerView.Adapter<AdapterVehicleDetailPhoto.MyViewHolder>() {
    class MyViewHolder (view: View):RecyclerView.ViewHolder(view){

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.adapter_vehicle_detail_photos,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        if (status==2){
            return 2
        }else{

            return 5
        }
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.view_add_button.setOnClickListener{
            ImageViewerActivity.start(context)
        }
    }

}