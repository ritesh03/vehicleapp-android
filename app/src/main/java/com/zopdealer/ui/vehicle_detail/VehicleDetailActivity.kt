package com.zopdealer.ui.vehicle_detail

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.text.Html
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.zopdealer.R
import com.zopdealer.base_classes.BaseActivity
import com.zopdealer.network.responses.Inventory
import com.zopdealer.ui.add_vehicle.AddVehicleActivity
import com.zopdealer.ui.dealer.DealerDetailsActivity
import com.zopdealer.ui.inventory.InventoryViewModel
import com.zopdealer.ui.inventory.InventoryViewModelFactory
import com.zopdealer.ui.inventory.vehicle_photos.VehiclePhotosActivity
import com.zopdealer.ui.vehicle_detail.adapter.AdapterFlags
import com.zopdealer.utils.Constants
import com.zopdealer.utils.preferences.Preferences
import kotlinx.android.synthetic.main.activity_vehicle_detail.*
import org.json.JSONArray

class VehicleDetailActivity : BaseActivity() {
    lateinit var adapterFlags: AdapterFlags
    private lateinit var viewModel: InventoryViewModel
    private var inventoryData: Inventory? = null

    companion object {
        fun start(
            context: Context,
            name: String,
            drawable: Int,
            isOnline: Boolean
        ) {
            val starter = Intent(context, VehicleDetailActivity::class.java)
            starter.putExtra("car_name", name)
            starter.putExtra("car_image", drawable)
            starter.putExtra("car_isOnline", isOnline)
            context.startActivity(starter)
        }

        fun start(
            context: Context,
            inventory_id: String,
            inventory_images: String
        ) {
            val starter = Intent(context, VehicleDetailActivity::class.java)
            starter.putExtra("inventory_id", inventory_id)
            starter.putExtra("inventory_image", inventory_images)
            context.startActivity(starter)
        }
    }

    override fun getID(): Int {
        return R.layout.activity_vehicle_detail
    }

    override fun iniView(savedInstanceState: Bundle?) {
        viewModel = ViewModelProvider(
            this,
            InventoryViewModelFactory()
        )[InventoryViewModel::class.java]
        viewModel.inventoryDetail.observe(::getLifecycle) {
            showLoading(false)
            it?.let { inventory ->
                iv_edit.visibility = View.VISIBLE
                this.inventoryData = inventory
                setInventoryData(inventory)
            }
        }

        viewModel.error.observe(::getLifecycle) {
            showLoading(false)
            it?.let {
               if (it.equals("Unauthorized Request")){
                   Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
                   Preferences.prefs?.edit()?.clear()?.apply()
                   DealerDetailsActivity.start(this)
                   finishAffinity()
               }else{
                   Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
               }


            }
        }

        iv_edit.setOnClickListener {
            inventoryData?.let { inventory ->
                AddVehicleActivity.start(
                    this@VehicleDetailActivity,
                    inventory
                )
            }
        }

        tv_pictures_all.setOnClickListener {
            startActivity(
                Intent(this@VehicleDetailActivity, VehiclePhotosActivity::class.java).putExtra(
                    Constants.INVENTORY_ID,
                    inventoryData?.inventory_id
                )
            )
        }
    }

    override fun onResume() {
        super.onResume()
        showLoading(true)
        iv_edit.visibility = View.GONE
        viewModel.getInventoryDetails(
            Preferences.prefs?.getString(Constants.BASE_URL, "") ?: "",
            intent.getStringExtra("inventory_id")
        )
    }

    @SuppressLint("ResourceType")
    private fun setInventoryData(model: Inventory) {
        if (intent.getStringExtra("inventory_image").isNullOrEmpty()) {
            imageView2.setImageResource(R.drawable.place_holder)
        } else {
            intent.getStringExtra("inventory_image")?.let {
                val imageArr = it.split(";")
                tv_pictures_all.text = "${imageArr.size}"
                val baseUrl = Preferences.prefs?.getString(Constants.BASE_URL, "")
                baseUrl?.let { url ->
                    //val trimmedUrl = url.substring(0, url.length - 1)
                    Glide.with(this).load(Constants.IMAGE_BASE_URL + "" + imageArr[0]).centerInside()
                        .placeholder(R.drawable.place_holder)
                        .into(imageView2)
                }
            }
        }

        textView5.text = model.year + " " + model.make + " " + model.model
        tv_stock_no.text = "Stock No. : " + model.stock_no
        tv_vin_no.text = "VIN No. : " + model.vin
        textView7.text = model.status
        textView7.setTextColor(Color.parseColor(getColorCode(model.status)))

        if (model.visibility.equals("1")) {
            tv_visibility_status.text = "Online"
            tv_visibility_status.setTextColor(ContextCompat.getColor(this, R.color.color_green))
            (circle_online.background as GradientDrawable).setColor(
                Color.parseColor(
                    "#4CAF50"
                )
            )
        } else {
            tv_visibility_status.text = "Offline"
            tv_visibility_status.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.redTextColorDark
                )
            )
            (circle_online.background as GradientDrawable).setColor(R.color.redTextColorDark)
        }

        textView9.text = model.price
        if (model.special_price_on.equals("1")) {
            tv_special_price_new.visibility = View.VISIBLE
            textView98.visibility = View.VISIBLE
            view7.visibility = View.VISIBLE
            textView98.text = model.special_price
        } else if (model.special_price_on.equals("0")) {
            tv_special_price_new.visibility = View.GONE
            textView98.visibility = View.GONE
            view7.visibility = View.GONE
        }

        if (model.special_price_type.equals("Forever")) {
            tv_exp_date.text = "Special Price Expiry Date: No End Date"
        } else {
            model.special_price_date?.let {
                tv_exp_date.text = "Special Price Expiry Date: " + getAbbreviatedFromDateTime(
                    it,
                    "yyyy-MM-dd HH:mm:ss",
                    "MM/dd/yyyy hh:mm"
                )
            }
        }

        tv_vehicle_type_value.text = model.vehicle_type

        if (model.visibility.equals("1")) {
            tv_visibility_value.text = "ON"
        } else {
            tv_visibility_value.text = "OFF"
        }

        if (model.feature_listing.equals("1")) {
            tv_featured_value.text = "ON"
        } else {
            tv_featured_value.text = "OFF"
        }

        if (model.financing_on.equals("1")) {
            tv_financing_value.text = "ON"
        } else {
            tv_financing_value.text = "OFF"
        }

        tv_odometer.text = model.odometer + " " + model.km_miles
        tv_ex_color1.text = model.exterior_color
        tv_in_color1.text = model.interior_color
        tv_type_body1.text = model.body_type
        textView11.text = model.door
        tv_engine.text = model.engine
        tv_displacement.text = model.litres
        textView10.text = model.transmission
        tv_drive_train_value.text = model.drivetrain
        tv_passenger_value.text = model.passengers
        tv_fuel_type.text = model.fuel_type
        tv_fuel_city_value.text = model.fuel_city
        tv_fuel_hwy_value.text = model.fuel_hwy

        tv_vehicle_title.text = Html.fromHtml(model.title)
        if (!model.vehicle_description.isNullOrEmpty())
            tv_vehicle_description.text = Html.fromHtml(
                model.vehicle_description?.replace("%26amp;", "&")?.replace("%26", "&")
                    ?.replace("And", "&")?.replace("&amp;", "&")
            )

        if (model.vehicle_tags != null) {
            try{
                val jsonArray = JSONArray(model.vehicle_tags)
                val listTags = ArrayList<String>()
                for (i in 0 until jsonArray.length()) {
                    listTags.add(jsonArray.getString(i))
                }
                initFlagsView(listTags)
            }catch (e:Exception){

            }
        }

        if (model.custom_features != null) {
            try {
                val jsonArray = JSONArray(model.custom_features)
                val listFeatures = ArrayList<String>()
                for (i in 0 until jsonArray.length()) {
                    listFeatures.add(jsonArray.getString(i))
                }
                initFeaturesView(listFeatures)
            }catch (e:Exception){

            }
        }
    }

    private fun initFlagsView(tags: ArrayList<String>) {
        adapterFlags = AdapterFlags(this, tags)
        rv_vehicle_tags.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rv_vehicle_tags.adapter = adapterFlags
    }

    private fun initFeaturesView(features: ArrayList<String>) {
        val adapterFeatures = AdapterFlags(this, features)
        rv_options.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rv_options.adapter = adapterFeatures
    }

    fun onClickVehicleDetailsActivity(view: View) {
        when (view) {
            btn_back -> {
                finish()
            }
            btn_down_description -> {
                hideShowViewOnClick(tv_vehicle_description, btn_down_description)
                tv_focusForDescriptionText.requestFocus()
            }
            btn_down_option -> {
                hideShowViewOnClick(rv_options, btn_down_option)
            }
            btn_down_interior_photo -> {
                hideShowViewOnClick(tv_vehicle_title, btn_down_interior_photo)
                //       rv_interior_photos.requestFocus()
            }
            btn_down_exterior_photo -> {
                hideShowViewOnClick(rv_exterior_photos, btn_down_exterior_photo)
            }
            btn_down_vehicle_tags -> {
                hideShowViewOnClick(rv_vehicle_tags, btn_down_vehicle_tags)
            }
        }
    }


    private fun hideShowViewOnClick(isViewVisible: View, iconView: ImageView) {

        if (isViewVisible.isVisible) {
            iconView.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.ic_add_view_vehicle
                )
            )
            val fadOut = AnimationUtils.loadAnimation(
                this,
                R.anim.fade_out_slow
            )
            isViewVisible.startAnimation(fadOut)

            isViewVisible.visibility = View.GONE
        } else {
            iconView.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.ic_minus_view_vehicle
                )
            )

            val fadIn = AnimationUtils.loadAnimation(
                this,
                R.anim.fade_in_slow
            )
            isViewVisible.startAnimation(fadIn)
            isViewVisible.visibility = View.VISIBLE
        }

        isViewVisible.requestFocus()
    }

    private fun getColorCode(colorStr: String?): String {
        return when (colorStr) {
            "Instock" -> "#39DA8A"
            "Sold" -> "#FF5B5C"
            "Archive" -> "#FDAC41"
            "Unpublished" -> "#475F7B"
            "other" -> "#475F7B"
            else -> "#000000"
        }
    }
}