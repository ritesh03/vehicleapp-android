package com.zopdealer.ui.image_view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.zopdealer.R
import com.zopdealer.base_classes.BaseActivity
import kotlinx.android.synthetic.main.activity_image_viewer.*

class ImageViewerActivity :BaseActivity() {
    companion object {
        fun start(context: Context) {
            val starter = Intent(context, ImageViewerActivity::class.java)
            context.startActivity(starter)
        }
    }

    override fun getID(): Int {
        return R.layout.activity_image_viewer
    }

    override fun iniView(savedInstanceState: Bundle?) {

    }

    fun onClickImageViewerActivity(v: View){
        when(v){
            btn_close->{
                finish()
            }
        }
    }
}