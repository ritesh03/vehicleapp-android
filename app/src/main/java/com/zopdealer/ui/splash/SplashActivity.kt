package com.zopdealer.ui.splash

import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.auth0.android.Auth0
import com.auth0.android.authentication.AuthenticationAPIClient
import com.auth0.android.authentication.storage.CredentialsManager
import com.auth0.android.authentication.storage.CredentialsManagerException
import com.auth0.android.authentication.storage.SharedPreferencesStorage
import com.auth0.android.callback.BaseCallback
import com.auth0.android.result.Credentials
import com.zopdealer.R
import com.zopdealer.base_classes.BaseActivity
import com.zopdealer.ui.dealer.DealerDetailsActivity
import com.zopdealer.ui.home.HomeActivity
import com.zopdealer.ui.login.LoginActivity
import com.zopdealer.utils.Constants
import com.zopdealer.utils.preferences.Preferences
import com.zopdealer.utils.preferences.saveValue

class SplashActivity : BaseActivity() {


    private lateinit var mHandler: Handler

    override fun getID(): Int {
        return R.layout.activity_splash
    }

    override fun iniView(savedInstanceState: Bundle?) {
        mHandler = Handler()
        mHandler.postDelayed({
           // startNextActivity()
            if (Preferences.prefs?.getString(Constants.USER_TOKEN, "").equals("")){
                DealerDetailsActivity.start(this)
            }else{
                HomeActivity.start(this)
            }

        }, 1500)
    }

    private fun startNextActivity() {
        val account = Auth0(
            this.getString(R.string.com_auth0_client_id),
            this.getString(R.string.com_auth0_domain)
        )
        account.isOIDCConformant = true
        val authentication = AuthenticationAPIClient(account)
        val manager = CredentialsManager(authentication, SharedPreferencesStorage(this))
        val loggedIn = manager.hasValidCredentials()
        if (loggedIn) {
                 manager?.getCredentials(object :
                         BaseCallback<Credentials?, CredentialsManagerException?> {
                override fun onSuccess(credentials: Credentials?) {
                    Log.e(TAG, " ------ onSuccess, credentials = $credentials")
                    //Use credentials
                    manager?.saveCredentials(credentials!!);
                    Preferences.prefs?.saveValue(Constants.USER_TOKEN, credentials?.idToken)
                    // Store credentials
                    // Navigate to your main activity
                    HomeActivity.start(this@SplashActivity)

                }

                override fun onFailure(error: CredentialsManagerException) {
                    Log.e(TAG, " ------ onFailure, error = $error")
                    //No credentials were previously saved or they couldn't be refreshed
                    manager.clearCredentials()
                    DealerDetailsActivity.start(this@SplashActivity)
                }
            })


           // HomeActivity.start(this)
        } else {
            manager.clearCredentials()
            DealerDetailsActivity.start(this)
        }
        finish()
    }


}//SplashActivity