package com.zopdealer.ui.profile

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.auth0.android.Auth0
import com.auth0.android.Auth0Exception
import com.auth0.android.authentication.AuthenticationAPIClient
import com.auth0.android.authentication.storage.CredentialsManager
import com.auth0.android.authentication.storage.SharedPreferencesStorage
import com.auth0.android.provider.VoidCallback
import com.auth0.android.provider.WebAuthProvider
import com.zopdealer.R
import com.zopdealer.base_classes.BaseFragment
import com.zopdealer.ui.change_password.ActivityChangePassword
import com.zopdealer.ui.dealer.DealerDetailsActivity
import com.zopdealer.ui.home.HomeActivity
import com.zopdealer.ui.login.LoginActivity
import com.zopdealer.utils.CommonFunction
import com.zopdealer.utils.preferences.Preferences
import kotlinx.android.synthetic.main.fragment_profile.*


class ProfileFragment : BaseFragment(), View.OnClickListener {

    private val TAG: String = "ProfileFragment"

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tv_version_name.text = "Version " + CommonFunction.getAppVersion(requireContext())


        HomeActivity.btn_edit_activity?.setOnClickListener {

            HomeActivity.btn_edit_activity?.visibility = View.GONE
            HomeActivity.text_edit_activity?.visibility = View.VISIBLE
            btn_save.visibility = View.VISIBLE
            et_name.isEnabled = true
            et_email_user.isEnabled = true

        }

        HomeActivity.text_edit_activity?.setOnClickListener {
            HomeActivity.btn_edit_activity?.visibility = View.VISIBLE
            HomeActivity.text_edit_activity?.visibility = View.GONE
            btn_save.visibility = View.GONE
            et_name.isEnabled = false
            et_dealer_domain.isEnabled = false
            et_dealerid.isEnabled = false
            et_email_user.isEnabled = false
        }

        btn_save.setOnClickListener(this)
        tv_log_out.setOnClickListener(this)
        tv_change_password.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            btn_save -> {
                HomeActivity.btn_edit_activity?.visibility = View.VISIBLE
                HomeActivity.text_edit_activity?.visibility = View.GONE
                btn_save.visibility = View.GONE
                et_name.isEnabled = false
                et_dealer_domain.isEnabled = false
                et_dealerid.isEnabled = false
                et_email_user.isEnabled = false
            }
            tv_log_out -> {
                val alert = AskOption()
                alert.show()
            }
            tv_change_password -> {
                ActivityChangePassword.start(requireContext())
            }

        }
    }

    private fun AskOption(): AlertDialog {
        return AlertDialog.Builder(activity)
            .setTitle("Logout")
            .setMessage("Are you sure you want to logout?")
            .setPositiveButton("Logout") { dialog, whichButton ->

                dialog.dismiss()
                Preferences.prefs?.edit()?.clear()?.apply()
                DealerDetailsActivity.start(requireContext())
                activity?.finishAffinity()
               // logout()

            }

            .setNegativeButton(
                    "Cancel"
            ) { dialog, which -> dialog.dismiss() }
            .create()

    }

    private fun logout() {
        val auth0 = Auth0(
                this.getString(R.string.com_auth0_client_id),
                this.getString(R.string.com_auth0_domain)
        )
        auth0.isLoggingEnabled = true
        WebAuthProvider.logout(auth0)
            .withScheme("demo")
            .start(requireContext(), object : VoidCallback {
                override fun onSuccess(payload: Void?) {
                    Toast.makeText(activity, "Logout successfully", Toast.LENGTH_LONG).show()
                    Preferences.prefs?.edit()?.clear()?.apply()
                    val authentication = AuthenticationAPIClient(auth0)
                    val manager = CredentialsManager(authentication, SharedPreferencesStorage(requireContext()))
                    manager.clearCredentials()
                    DealerDetailsActivity.start(requireContext())
                    activity?.finishAffinity()
                }

                override fun onFailure(error: Auth0Exception) {
                    Log.e(TAG, " ------ onFailure, error = $error")
                    showSnackBar("failed: $error")
                    Preferences.prefs?.edit()?.clear()?.apply()
                    val authentication = AuthenticationAPIClient(auth0!!)
                    val manager = CredentialsManager(authentication, SharedPreferencesStorage(activity!!))
                    manager.clearCredentials()
                    DealerDetailsActivity.start(requireContext())
                    activity?.finishAffinity()
                }
            })
    }
}