package com.zopdealer.ui.inventory

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.zopdealer.R
import com.zopdealer.base_classes.BaseFragment
import com.zopdealer.ui.filter.FilterActivity
import com.zopdealer.ui.filter.FilterStatus
import com.zopdealer.ui.filter.InventorySort
import com.zopdealer.ui.inventory.vehicle_photos.VehiclePhotosActivity
import com.zopdealer.ui.vehicle_detail.VehicleDetailActivity
import com.zopdealer.utils.Constants
import com.zopdealer.utils.PaginationScrollListener
import com.zopdealer.utils.preferences.Preferences
import kotlinx.android.synthetic.main.fragment_inventory.*


class InventoryFragment : BaseFragment(), ItemClickListener, SwipeRefreshLayout.OnRefreshListener {

    lateinit var inventoryAdapter: AdapterInventory
    private lateinit var viewModel: InventoryViewModel
    lateinit var layoutManager: LinearLayoutManager
    private val PAGE_START = 1
    private var TOTAL_PAGES = 1
    private var TOTAL_COUNT= 0
    private var currentPage: Int = PAGE_START
    private var isLoadingPage = false
    private var isLastPageLoaded = false
    var filterList:ArrayList<String>?=null
    val filterMap = HashMap<String, String>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_inventory, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        swipe_container.setOnRefreshListener(this)

        viewModel = ViewModelProvider(requireActivity()).get(InventoryViewModel::class.java)
        viewModel.inventoryResponse.observe(::getLifecycle) {
            swipe_container.isRefreshing = false

            it?.let { listInventory ->
                TOTAL_PAGES = it.pager?.pageCount ?: 1
                listInventory.inventory?.let {

                    if (TOTAL_PAGES==1){
                        inventoryAdapter.addAll(it)
                        inventoryAdapter.removeLoadingFooter()
                       isLoadingPage=false
                    }else{
                        inventoryAdapter.addAll(it)

                        inventoryAdapter.removeLoadingFooter()
                        isLoadingPage = false

                        isLastPageLoaded = if (currentPage <= TOTAL_PAGES) {
                            inventoryAdapter.addLoadingFooter()
                            false
                        } else true
                    }
                }
            }
        }

        viewModel.error.observe(::getLifecycle) {
            swipe_container.isRefreshing = false
            it?.let {
                Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
            }
        }
        initViews()
    }

    private fun initViews() {
        inventoryAdapter = AdapterInventory(requireContext(), this)
        layoutManager = LinearLayoutManager(requireContext())
        rv_inventory.layoutManager = layoutManager
        rv_inventory.adapter = inventoryAdapter

        inventoryAdapter.onItemClick = {
            if (it.inventory_id!=null){
                VehicleDetailActivity.start(requireContext(), it.inventory_id!!, it.image_urls ?: "")
            }else{
                showSnackBar("Invalid inventory")
            }
        }

        // Used to handle the photo click for viewing and performing
        // the different operations for the respective vehicle
        inventoryAdapter.onPhotoClick = {
            if (it.inventory_id!=null){
                startActivity(
                        Intent(requireActivity(), VehiclePhotosActivity::class.java).putExtra(
                                Constants.INVENTORY_ID,
                                it.inventory_id
                        )
                )
            }else{
             showSnackBar("Invalid inventory")
            }

        }

        rv_inventory.addOnScrollListener(object : PaginationScrollListener(layoutManager) {
            override fun isLastPage(): Boolean {
                Log.d("Pagination ", "isLastPage $isLastPageLoaded")
                return isLastPageLoaded
            }

            override fun loadMoreItems(){
                Log.d("Pagination ", "loadMoreItems")
                isLoadingPage = true
                currentPage += 1
                if (inventorySort != null) {
                    viewModel.getInventoryList(
                            Preferences.prefs?.getString(Constants.BASE_URL, "") ?: "",
                            "",
                            currentPage,
                            column = inventorySort!!.column,
                            order = inventorySort!!.order,
                            filterMap = filterMap
                    )
                }else if (FilterActivity.filterList?.size!=0){
                    viewModel.getInventoryList(
                            Preferences.prefs?.getString(Constants.BASE_URL, "") ?: "",
                            "",
                            currentPage,
                            filterMap = filterMap
                    )
                } else{
                    getInventoryList(currentPage)
                }

            }

            override fun getTotalPageCount(): Int {
                Log.d("Pagination ", "getTotalPageCount $TOTAL_PAGES")
                return TOTAL_PAGES
            }

            override fun isLoading(): Boolean {
                Log.d("Pagination ", "isLoading $isLoadingPage")
                return isLoadingPage
            }

        })
    }

    private fun getInventoryList(page: Int = 1) {
        if (page <= 1)
            swipe_container.isRefreshing = true
           viewModel.getInventoryList(
            Preferences.prefs?.getString(Constants.BASE_URL, "") ?: "",
            page = page
        )
    }

    override fun onItemClick(
        pos: Int,
        url: String,
        drawable: Int,
        isOnline: Boolean
    ) {
        VehicleDetailActivity.start(requireContext(), url, drawable, isOnline)
    }
    override fun onResume() {
        super.onResume()
        try {
           onRefresh()
        }catch (e:Exception){

        }

    }

    override fun onRefresh() {
        currentPage = 1
        inventoryAdapter.getInventoryList().clear()
        inventoryAdapter.notifyDataSetChanged()

        if (inventorySort != null) {
            viewModel.getInventoryList(
                    Preferences.prefs?.getString(Constants.BASE_URL, "") ?: "",
                    column = inventorySort!!.column,
                    order = inventorySort!!.order,
                    filterMap = filterMap
            )
        }else if (FilterActivity.filterList?.size!=0){
             viewModel.getInventoryList(
                     Preferences.prefs?.getString(Constants.BASE_URL, "") ?: "",
                     filterMap = filterMap
             )
         }  else{
            getInventoryList(page = currentPage)
        }

    }
    var inventorySort: InventorySort?=null
    fun setFilters(filterStatusList: List<FilterStatus>?, inventorySort: InventorySort?) {
        swipe_container.isRefreshing = true
         this.inventorySort=inventorySort
        //filterList=ArrayList<String>()
        filterMap.clear()
        if (filterStatusList != null) {
            for (x in filterStatusList.indices) {
                if (filterStatusList[x].isSelected!!){
                    for (i in FilterActivity.filterList?.indices!!){
                        FilterActivity.filterList!![i]?.let { filterMap.put("zm_status[$i]", it) }
                    }
                }
            }

        }
        Log.e("filterList",""+FilterActivity.filterList?.toString())


      /*  if (inventorySort != null) {
            viewModel.getInventoryList(
                Preferences.prefs?.getString(Constants.BASE_URL, "") ?: "",
                column = inventorySort.column,
                order = inventorySort.order,
                filterMap = filterMap
            )
        } else {
            viewModel.getInventoryList(
                Preferences.prefs?.getString(Constants.BASE_URL, "") ?: "",
                filterMap = filterMap
            )
        }*/
    }
}