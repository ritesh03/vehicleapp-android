package com.zopdealer.ui.inventory

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.zopdealer.api.ApiService
import com.zopdealer.api.RetrofitClientInstance
import com.zopdealer.config.Config
import com.zopdealer.network.entities.ResponseError
import com.zopdealer.network.responses.Inventory
import com.zopdealer.network.responses.InventoryResponse
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback

class InventoryViewModel : ViewModel() {

    private val _error: MutableLiveData<String> = MutableLiveData()
    private val _inventoryResponse: MutableLiveData<InventoryResponse> = MutableLiveData()
    private val _inventoryDetail: MutableLiveData<Inventory> = MutableLiveData()

    val error: LiveData<String>
        get() = _error

    val inventoryResponse: LiveData<InventoryResponse>
        get() = _inventoryResponse

    val inventoryDetail: LiveData<Inventory>
        get() = _inventoryDetail

    fun getInventoryList(
        baseUrl: String,
        search: String? = null,
        page: Int? = 1,
        column: String? = null,
        order: String? = null,
        filterMap: HashMap<String, String>? = null
    ) {
        val apiService = RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call = apiService?.getInventoryList(search, page, column, order, filterMap ?: HashMap())
        call?.enqueue(object : Callback, retrofit2.Callback<InventoryResponse> {
            override fun onFailure(call: Call<InventoryResponse>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                call: Call<InventoryResponse>,
                response: Response<InventoryResponse>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        _inventoryResponse.postValue(it)
                    }
                } else {
                    try {
                        val responseError = Gson().fromJson(
                            response.errorBody()!!.string(),
                            ResponseError::class.java
                        )
                        _error.value = responseError.error
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })
    }

    fun getInventoryDetails(
        baseUrl: String,
        inventory_id: String?
    ) {
        //val apiService = RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val apiService = RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call = apiService?.getInventoryDetail(inventory_id)
        call?.enqueue(object : Callback, retrofit2.Callback<Inventory> {

            override fun onFailure(call: Call<Inventory>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                call: Call<Inventory>,
                response: Response<Inventory>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        _inventoryDetail.postValue(it)
                    }
                } else {
                    try {
                        val responseError = Gson().fromJson(
                                response.errorBody()!!.string(),
                                ResponseError::class.java
                        )
                        _error.value = responseError.error
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }

                }
            }
        })
    }
}

class InventoryViewModelFactory : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return InventoryViewModel() as T
    }
}

