package com.zopdealer.ui.inventory

interface ItemClickListener {
    fun onItemClick(
        pos: Int,
        url: String,
        drawable: Int,
        isOnline: Boolean
    )
}