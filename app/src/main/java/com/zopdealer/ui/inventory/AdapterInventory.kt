package com.zopdealer.ui.inventory

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zopdealer.R
import com.zopdealer.network.responses.Inventory
import com.zopdealer.ui.filter.FilterActivity
import com.zopdealer.utils.Constants
import com.zopdealer.utils.preferences.Preferences
import kotlinx.android.synthetic.main.adapter_inventroy.view.*

class AdapterInventory(
    private val context: Context,
    private val listener: ItemClickListener
) :
    RecyclerView.Adapter<AdapterInventory.MyViewHolder>() {

    private var isLoadingAdded = false

    private var listInventory = ArrayList<Inventory>()

    var onItemClick: ((Inventory) -> Unit)? = null

    var onPhotoClick: ((Inventory) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var viewHolder: MyViewHolder? = null
        val inflater = LayoutInflater.from(parent.context)
        when (viewType) {
            ITEM -> {
                val viewItem: View =
                    inflater.inflate(R.layout.adapter_inventroy, parent, false)
                viewHolder = MyViewHolder(viewItem)
            }
            LOADING -> {
                val viewLoading =
                    inflater.inflate(R.layout.item_progress, parent, false)
                viewHolder = MyViewHolder(viewLoading)
            }
        }
        return viewHolder!!
    }

    override fun getItemCount(): Int {
        return listInventory.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == listInventory.size - 1 && isLoadingAdded) LOADING else ITEM
    }

    @SuppressLint("ResourceType")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = listInventory[position]

        when (getItemViewType(position)) {
            ITEM -> {
                (holder.itemView.kmpl.background as GradientDrawable).setColor(Color.parseColor("#000000"))

                model.status?.let {
                    (holder.itemView.circle_online.background as GradientDrawable).setColor(
                        Color.parseColor(
                            getColorCode(it)
                        )
                    )
                }

                holder.itemView.tv_car_name.text = model.year + " " + model.make + " " + model.model

                if (!model.odometer.isNullOrEmpty())
                    holder.itemView.tv_car_km.text = model.odometer + " " + model.km_miles
                else
                    holder.itemView.tv_car_km.text = "0.0 KM"

                holder.itemView.tv_car_vin_no.text = "VIN: " + model.vin
                holder.itemView.tv_car_price.text = model.price
               // holder.itemView.tv_car_price_special.text = model.special_price

                if (model.special_price_on.toString().equals("1",true)) {
                    holder.itemView.view7.visibility = View.VISIBLE
                    holder.itemView.tv_car_price_special.visibility = View.VISIBLE
                    holder.itemView.tv_car_price_special.text = model.special_price
                } else {
                    holder.itemView.view7.visibility = View.GONE
                    holder.itemView.tv_car_price_special.visibility = View.GONE
                }

                holder.itemView.tv_car_status.text = model.status
                holder.itemView.tv_car_status.setTextColor(Color.parseColor(getColorCode(model.status)))
                holder.itemView.view_online.setBackgroundColor(Color.parseColor(getColorCode(model.status)))

                holder.itemView.tv_car_cc.text = model.engine
                holder.itemView.tv_transmission.text = model.transmission
                holder.itemView.tv_seats.text = model.passengers
                holder.itemView.tv_fuel_type.text = model.fuel_type
                holder.itemView.tv_car_type_text.text = model.body_type

                if (!model.exterior_color.isNullOrEmpty()) {
                    holder.itemView.tv_car_color_text.text = model.exterior_color
                    (holder.itemView.kmpl.background as GradientDrawable).setColor(
                        Color.parseColor(
                            getColorCode(model.exterior_color)
                        )
                    )
                } else {
                   // holder.itemView.tv_car_color_text.text = "White"
                    holder.itemView.tv_car_color_text.text = " "
                    (holder.itemView.kmpl.background as GradientDrawable).setColor(
                        Color.parseColor(
                            getColorCode("White")
                        )
                    )
                }

                (holder.itemView.kmpl.background as GradientDrawable).setColor(
                    Color.parseColor(
                        getColorCode(model.exterior_color ?: "White")
                    )
                )

                if(model.image_count.equals("0")){

                }else{
                    holder.itemView.tv_pictures_all.text =model.image_count
                }

                if (model.image_urls.isNullOrEmpty()) {
                    holder.itemView.iv_car_image.setImageResource(R.drawable.place_holder)
                } else {
                    model.image_urls?.let {
                        val imageArr = it.split(";")
                        //holder.itemView.tv_pictures_all.text = "${imageArr.size}"

                        val baseUrl = Preferences.prefs?.getString(Constants.BASE_URL, "")
                        baseUrl?.let { url ->
//                            val trimmedUrl = url.substring(0, url.length - 1)
                            Log.e("image url link==",Constants.IMAGE_BASE_URL + "" + imageArr[0])

                            Glide.with(context).load(Constants.IMAGE_BASE_URL + "" + imageArr[0]).centerInside()
                                .placeholder(R.drawable.place_holder)
                                .into(holder.itemView.iv_car_image)
                        }
                    }
                }

                // Used to handle the vehicle photos
                holder.itemView.tv_pictures_all.setOnClickListener {
                    onPhotoClick?.invoke(model)
                }

                holder.itemView.parent_view.setOnClickListener {
                    onItemClick?.invoke(model)
                }
            }
            LOADING -> {
            }
        }
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)

    fun submitList(list: List<Inventory>) {
        listInventory.clear()
        listInventory.addAll(list)
        notifyDataSetChanged()
    }

    fun getInventoryList(): ArrayList<Inventory> {
        return listInventory
    }

    private fun getColorCode(colorStr: String?): String {
        return when (colorStr) {
            "Black" -> "#000000"
            "Grey" -> "#939294"
            "Silver" -> "#C1C0C2"
            "White" -> "#FFFFFF"
            "Cream" -> "#EEE7D0"
            "Tan" -> "#D4B691"
            "Brown" -> "#974700"
            "Maroon" -> "#7F0E24"
            "Red" -> "#F92800"
            "Orange" -> "#F4A201"
            "Gold" -> "#F4B132"
            "Yellow" -> "#FDFB03"
            "Green" -> "#166500"
            "Teal" -> "#1F7D7E"
            "Blue" -> "#1E33FF"
            "Purple" -> "#7E91CB"
            "Beige" -> "#F5F5DC"
            "Instock" -> "#39DA8A"
            "Sold" -> "#FF5B5C"
            "Archive" -> "#FDAC41"
            "Unpublished" -> "#475F7B"
            "other" -> "#475F7B"
            else -> "#FFFFFF"
        }
    }

    fun add(inventory: Inventory) {
        listInventory.add(inventory)
        notifyItemInserted(listInventory.size-1)
    }

    fun addAll(moveResults: List<Inventory>) {

        for (result in moveResults) {
            add(result)
        }
    }

    fun addLoadingFooter() {
        isLoadingAdded = true
        //add(Inventory())
    }

    fun removeLoadingFooter() {
        isLoadingAdded = false
        /*val position: Int = listInventory.size - 1
        val result: Inventory? = getItem(position)
        if (result != null) {
            listInventory.removeAt(position)
            notifyItemRemoved(position)
        }*/
    }

    private fun getItem(position: Int): Inventory? {
        return listInventory[position]
    }

    companion object {
        private const val ITEM = 0
        private const val LOADING = 1
    }
}