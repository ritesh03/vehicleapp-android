package com.zopdealer.ui.inventory.vehicle_photos

import android.Manifest

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper

import com.zopdealer.R
import com.zopdealer.base_classes.BaseActivity
import com.zopdealer.network.responses.Gallery
import com.zopdealer.ui.add_vehicle.adapter.VehiclePhotosAdapter
import com.zopdealer.ui.add_vehicle.call_back.ItemInteriorPhotoMoveCallback
import com.zopdealer.ui.add_vehicle.interfaces.RecyclerViewClickListener
import com.zopdealer.ui.dealer.DealerDetailsActivity
import com.zopdealer.utils.Constants
import com.zopdealer.utils.DirectoryUtils
import com.zopdealer.utils.GridSpacingItemDecoration
import com.zopdealer.utils.preferences.Preferences
import kotlinx.android.synthetic.main.activity_vehicle_photos.*
import java.io.ByteArrayOutputStream
import java.io.FileNotFoundException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.set


/**
 * Vehicle Photos Activity used to handle the images for the inventories
 * Uplaod, fetch, sort, delete and active/inactive status all these
 * features handled in this view
 */
class VehiclePhotosActivity : BaseActivity(), RecyclerViewClickListener {

    // Instance of the VehiclePhotosAdapter
    lateinit var adapterPhotosAdapter: VehiclePhotosAdapter

    // Used to handle the vehicle images
    private var vehicleImagesList: MutableList<Gallery> = ArrayList()

    // Instance of the vehicle photos view model
    private lateinit var vehiclePhotosViewModel: VehiclePhotosViewModel

    // Used to handle the operations corresponding to the selectory id
    private var inventoryId: String? = null
    val singleimagelist = ArrayList<String>()

    override fun getID(): Int = R.layout.activity_vehicle_photos

    override fun iniView(savedInstanceState: Bundle?) {
        inventoryId = intent.getStringExtra(Constants.INVENTORY_ID)
        initViews()
        initClickListeners()
        initViewModel()
        initObservers()
    }

    /**
     * Used to initialize the views
     */
    private fun initViews() {
        adapterPhotosAdapter = VehiclePhotosAdapter(this, vehicleImagesList, this, 1)
        rv_interior_photos.apply {
            val touchHelper = ItemTouchHelper(ItemInteriorPhotoMoveCallback(adapterPhotosAdapter))
            touchHelper.attachToRecyclerView(this)
            // apply spacing
            val itemDecoration =
                GridSpacingItemDecoration(
                    resources.getDimensionPixelOffset(R.dimen.item_offset)
                )
            addItemDecoration(itemDecoration)
            adapter = adapterPhotosAdapter
        }
        swipe_container.setOnRefreshListener {
            fetchInventoryImages()
        }
    }

    /**
     * Used to handle the click listeners
     */
    private fun initClickListeners() {
        // Used to add the image from Gallery or Camera
        btn_add_image.setOnClickListener {
            if (checkAndRequestPermissions()) {
                showPictureDialog()
            }
        }
        btn_submit.setOnClickListener {
            val imgIdArray: ArrayList<String?> = ArrayList()
            vehicleImagesList.forEachIndexed { index, gallery ->
                imgIdArray.add(vehicleImagesList[index].id)
            }
            showLoading(true)
            // Used to sort the images
            vehiclePhotosViewModel.sortImagesRank(
                Preferences.prefs?.getString(
                    Constants.BASE_URL,
                    ""
                ) ?: "",
                inventoryId, imgIdArray
            )
        }
        btn_close.setOnClickListener { finish() }
    }

    /**
     * Used to initialize the view model
     */
    private fun initViewModel() {
        vehiclePhotosViewModel = ViewModelProvider(this).get(VehiclePhotosViewModel::class.java)
        swipe_container?.isRefreshing = true
        fetchInventoryImages()
    }

    /**
     * Used to observe the observers and update the view accordingly
     */
    private fun initObservers() {
        // Used to handel the fetch inventory images response and update view accordingly
        vehiclePhotosViewModel.inventoryImagesResponse.observe(::getLifecycle) {
            swipe_container?.isRefreshing = false
            showLoading(false)
            it?.let { gallery ->
                if (gallery.isEmpty()) {
                    tv_no_data.visibility = View.VISIBLE
                    rv_interior_photos.visibility = View.GONE
                } else {
                    tv_no_data.visibility = View.GONE
                    rv_interior_photos.visibility = View.VISIBLE
                }
                vehicleImagesList.clear()
                vehicleImagesList.addAll(gallery)
                setUpRecyclerView()
            }
        }

        // Used to handle the response ones the images are uploaded and update view accordingly
        vehiclePhotosViewModel.uploadInventoryImageResponse.observe(::getLifecycle) {
            showLoading(false)
            it?.let { uploadedImage ->
                tv_no_data.visibility = View.GONE
                rv_interior_photos.visibility = View.VISIBLE
                uploadedImage.apply {
                    vehicleImagesList.add(
                        Gallery(
                            id.toString(),
                            inventory_id,
                            image_url,
                            sort_rank,
                            Constants.INVENTORY_IMG_ACTIVE,
                            null,
                            null,
                            null,
                            null
                        )
                    )
                    setUpRecyclerView()

                   /* if (vehicleImagesList.size!=0){
                        tempImagesList.forEachIndexed { index, s ->

                            if(index==0){

                            }else{
                                singleimagelist.clear()
                                singleimagelist.add(s)
                                showLoading(true)
                                vehiclePhotosViewModel.uploadImages(
                                        Preferences.prefs?.getString(
                                                Constants.BASE_URL,
                                                ""
                                        ) ?: "",
                                        inventoryId, singleimagelist)
                            }

                        }
                    }*/


                }
            }
        }

        // Used to handle the response ones the images has been sorted and update view accordingly
        vehiclePhotosViewModel.sortImagesResponse.observe(::getLifecycle) {
            showLoading(false)
            it?.let { imagesSorted ->
                imagesSorted.success?.let { msg -> showAlertDialog(msg) }
            }
        }

        // Used to handle the errors in the api and update view accordingly
        vehiclePhotosViewModel.error.observe(::getLifecycle) {
            swipe_container?.isRefreshing = false
            it?.let {
                if (it.equals("Unauthorized Request")){
                    Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
                    Preferences.prefs?.edit()?.clear()?.apply()
                    DealerDetailsActivity.start(this)
                    finishAffinity()
                }else{
                    Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    /**
     * Used to handle the fetch inventory image api
     */
    private fun fetchInventoryImages() {
        vehiclePhotosViewModel.getInventoryImages(
            Preferences.prefs?.getString(
                Constants.BASE_URL,
                ""
            ) ?: "",
            inventoryId
        )
    }

    override fun onAdapterItemClick(view: View, pos: Int) {
        val imgIdArray: ArrayList<String?> = arrayListOf(vehicleImagesList[pos].id)
        if (view.id == R.id.btn_cross) {
            showLoading(true)
            // Used to remove the image
            vehiclePhotosViewModel.deleteInventoryImage(
                Preferences.prefs?.getString(
                    Constants.BASE_URL,
                    ""
                ) ?: "",
                inventoryId, imgIdArray

            )
            // Todo interiorImagesList.removeAt(pos)
            //Todo setUpRecyclerView()
        } else if (view.id == R.id.img_status) {
            val imgStatus = if (vehicleImagesList[pos].active == Constants.INVENTORY_IMG_ACTIVE) {
                Constants.INVENTORY_IMG_INACTIVE
            } else {
                Constants.INVENTORY_IMG_ACTIVE
            }
            showLoading(true)
            // Used to update the image status enable/disable
            vehiclePhotosViewModel.updateImageStatus(
                Preferences.prefs?.getString(
                    Constants.BASE_URL,
                    ""
                ) ?: "",
                inventoryId, imgIdArray, imgStatus

            )
        }
    }

    /**
     * Used to handle the image chooser popup
     */
    private fun showPictureDialog() {
        val pictureDialog = AlertDialog.Builder(this)
        pictureDialog.setTitle("Select Action")
        val pictureDialogItems = arrayOf("Gallery", "Camera")
        pictureDialog.setItems(
            pictureDialogItems
        ) { dialog, which ->
            when (which) {
                0 -> choosePhotoFromGallary()
                1 -> takePhotoFromCamera()
            }
        }
        pictureDialog.show()
    }

    /**
     * Used to open the gallery
     */
    private fun choosePhotoFromGallary() {
        this.startActivityForResult(Intent(Intent.ACTION_OPEN_DOCUMENT).apply { type = "image/*"
            putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
            intent.addCategory(Intent.CATEGORY_OPENABLE)
        }, 11
        )
    }

    /**
     * Used to open the camera
     */
    private fun takePhotoFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        this.startActivityForResult(intent, 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val tempImagesList = ArrayList<String>()
        /*  Vehicle photos from camera */
        if (resultCode == RESULT_OK && requestCode == 1) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            tempImagesList.add(getRealPathFromURI(getCaptureImageOutputUri(imageBitmap))!!)
        }
        /*  Vehicle photos from gallery  */
        else if (resultCode == RESULT_OK && requestCode == 11) {
            if (data != null) {
                   if (data.data != null) {
                    val clipData = data.clipData
                    if (clipData != null) {
                        tempImagesList.addAll(getMultipleImages(data))
                    } else {
                        //single image selected
                        val imageUri = data.data
                        try {
                            imageUri?.let {
                                tempImagesList.add(getRealPathFromURI(it)!!)
                            }
                        } catch (e: FileNotFoundException) {
                            e.printStackTrace()
                        }
                    }
                } else {
                    tempImagesList.addAll(getMultipleImages(data))
                }

            }
            // used to call the upload images api if there is any data in the tempImagesList
            if (tempImagesList.isNotEmpty()) {
                /* singleimagelist.add(tempImagesList.get(0))*/
                showLoading(true)
                vehiclePhotosViewModel.uploadImages(
                        Preferences.prefs?.getString(
                                Constants.BASE_URL,
                                ""
                        ) ?: "",
                        inventoryId, tempImagesList
                )

            }
        }
    }

    /**
     * Used to get the multiple images uri
     * @param data - instance of the data
     */
     fun getMultipleImages(data: Intent): ArrayList<String> {
        val tempImagesList = ArrayList<String>()
        //multiple images selected
        val clipData = data.clipData
        if (clipData != null) {
            for (i in 0 until clipData.itemCount) {
                val imageUri = clipData.getItemAt(i).uri
                try {
                    tempImagesList.add(getRealPathFromURI(imageUri)!!)
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                }
            }
        }
        return tempImagesList
    }

    /**
     * Used to get the real path from the Uri
     * @return path of the image
     */
     fun getRealPathFromURI(selectedImage: Uri?): String? =
        if ("content" == selectedImage?.scheme) {
            DirectoryUtils.getPathFromUri(this, selectedImage)
        } else {
            selectedImage?.path
        }

    /**
     * Get URI to image received from capture by camera.
     */
     fun getCaptureImageOutputUri(data: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        data.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path: String =
            MediaStore.Images.Media.insertImage(contentResolver, data, "Title", null)
        return Uri.parse(path)
    }

     val REQUEST_ID_MULTIPLE_PERMISSIONS = 3

    /**
     * Used to handle the permissions for read and write storage
     */
    private fun checkAndRequestPermissions(): Boolean {
        val cameraPermission =
            ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val writePermission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )

        val listPermissionsNeeded = java.util.ArrayList<String>()

        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
        }
        if (writePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        }

        if (listPermissionsNeeded.isNotEmpty()) {
            ActivityCompat.requestPermissions(
                this,
                listPermissionsNeeded.toTypedArray(),
                REQUEST_ID_MULTIPLE_PERMISSIONS
            )
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_ID_MULTIPLE_PERMISSIONS -> {

                val perms = HashMap<String, Int>()
                // Initialize the map with both permissions
                perms[Manifest.permission.CAMERA] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] =
                    PackageManager.PERMISSION_GRANTED
                // Fill with actual results from user
                if (grantResults.isNotEmpty()) {
                    for (i in permissions.indices) perms[permissions[i]] = grantResults[i]
                    // Check for both permissions
                    if (perms[Manifest.permission.CAMERA] == PackageManager.PERMISSION_GRANTED
                        && perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED
                    ) {
                        if (checkAndRequestPermissions()) {
                            showPictureDialog()
                        }
                    } else {
                        if (ActivityCompat.shouldShowRequestPermissionRationale(
                                this,
                                Manifest.permission.CAMERA
                            )
                            || ActivityCompat.shouldShowRequestPermissionRationale(
                                this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                            )
                        ) {
                            showDialogOK(getString(R.string.permissions_required),
                                DialogInterface.OnClickListener { dialog, which ->
                                    when (which) {
                                        DialogInterface.BUTTON_POSITIVE -> checkAndRequestPermissions()
                                        DialogInterface.BUTTON_NEGATIVE -> {
                                            // proceed with logic by disabling the related features or quit the app.
                                            //  finish()
                                        }
                                    }
                                })
                        } else {
                            showAlert(getString(R.string.permissions_alert_msg))
                            //proceed with logic by disabling the related features or quit the app.
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                    }
                } else {
                    if (checkAndRequestPermissions()) {
                        showPictureDialog()
                    }
                }
            }
        }
    }

    /**
     * Used to show the popup for the permissions
     */
    private fun showAlert(msg: String) {
        val dialog = AlertDialog.Builder(this)
        dialog.setMessage(msg)
            .setPositiveButton(getString(R.string.yes)) { paramDialogInterface, paramInt ->
                //  permissions class.requestPermission(type,code);
                startActivity(
                    Intent(
                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("com.vehicle")
                    )
                )
            }
            .setNegativeButton(getString(R.string.cancel)) { paramDialogInterface, paramInt ->
                Toast.makeText(this, getString(R.string.permissions_required), Toast.LENGTH_LONG)
                    .show()
            }
        dialog.show()
    }

    /**
     * Used to show the alert dialog with ok and cancel button
     */
    private fun showDialogOK(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this)
            .setMessage(message)
            .setPositiveButton(getString(R.string.ok_btn), okListener)
            .setNegativeButton(getString(R.string.cancel), okListener)
            .create()
            .show()
    }

    /**
     * Used to show the alert dialog
     */
    private fun showAlertDialog(message: String) {
        AlertDialog.Builder(this)
            .setMessage(message)
            .setPositiveButton(getString(R.string.ok_btn), null)
            .create()
            .show()
    }

    /**
     * Used to update the adapter
     */
    private fun setUpRecyclerView() {
        runOnUiThread { adapterPhotosAdapter.notifyDataSetChanged() }
    }
}