package com.zopdealer.ui.inventory.vehicle_photos

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.zopdealer.api.ApiService
import com.zopdealer.api.RetrofitClientInstance
import com.zopdealer.network.entities.ResponseError
import com.zopdealer.network.responses.Gallery
import com.zopdealer.network.responses.InventoryImages
import com.zopdealer.network.responses.SortImagesResponse
import com.zopdealer.network.responses.UploadInventoryImage
import com.zopdealer.utils.ApiConstants
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.io.File
import javax.security.auth.callback.Callback

/**
 * ViewModel used to handle the Vehicle Photos view
 */
class VehiclePhotosViewModel : ViewModel() {
    // Mutable live data used for handling the error
    private val _error: MutableLiveData<String> = MutableLiveData()
    // Mutable live data used for handling the inventory images response
    private val _inventoryImages: MutableLiveData<MutableList<Gallery>> = MutableLiveData()
    // Mutable live data used for handling the upload images response
    private val _uploadedImage: MutableLiveData<UploadInventoryImage> = MutableLiveData()
    // Mutable live data used for handling the upload images response
    private val _sortImages: MutableLiveData<SortImagesResponse> = MutableLiveData()

    /**
     * Used to get the error value and handle the view accordingly
     */
    val error: LiveData<String>
        get() = _error

    /**
     * Used to get the inventoryImages response and handle the view accordingly
     */
    val inventoryImagesResponse: LiveData<MutableList<Gallery>>
        get() = _inventoryImages

    /**
     * Used to get the response after uploading the image and handle the view accordingly
     */
    val uploadInventoryImageResponse: LiveData<UploadInventoryImage>
        get() = _uploadedImage

    /**
     * Used to get the response after sorting the images and handle the view accordingly
     */
    val sortImagesResponse: LiveData<SortImagesResponse>
        get() = _sortImages

    /**
     * Used to fetch the inventory images
     * @param baseUrl - Base Url value
     * @param inventoryId - Inventory id value corresponding to which images need to fetched
     */
    fun getInventoryImages(
        baseUrl: String,
        inventoryId: String?
    ) {
        val apiService =
            RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call = apiService?.getInventoryImages(inventoryId)
        call?.enqueue(object : Callback, retrofit2.Callback<InventoryImages> {
            override fun onFailure(call: Call<InventoryImages>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                call: Call<InventoryImages>,
                response: Response<InventoryImages>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        _inventoryImages.value = it.gallery
                    }
                } else {
                    try {
                        val responseError = Gson().fromJson(
                            response.errorBody()?.string(),
                            ResponseError::class.java
                        )
                        responseError?.error?.let {
                            _error.value = it
                        }
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })
    }

    /**
     * Used to upload the inventory images
     * @param baseUrl - Base Url value
     * @param inventoryId - Inventory id value corresponding to which images need to added
     * @param interiorImagesList - Images array
     */
    fun uploadImages(
        baseUrl: String,
        inventoryId: String?,
        interiorImagesList: ArrayList<String>
    ) {
        interiorImagesList.forEachIndexed { index, uri ->
            val file = File(uri)
            val reqFile: RequestBody = RequestBody.create(MediaType.parse("image/*"), file)
            val body = MultipartBody.Part.createFormData("file", file.name, reqFile)

            val name = RequestBody.create(MediaType.parse("text/plain"), "upload_test")
            val apiService = RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
            val call = apiService?.uploadImage(body, name, index, inventoryId)
            call?.enqueue(object : Callback, retrofit2.Callback<UploadInventoryImage> {
                override fun onFailure(call: Call<UploadInventoryImage>, t: Throwable) {
                    _error.postValue(t.localizedMessage)
                }

                override fun onResponse(
                    call: Call<UploadInventoryImage>,
                    response: Response<UploadInventoryImage>
                ) {
                    if (response.code() == 200) {
                        response.body()?.let {
                            _uploadedImage.value = it
                        }
                    } else {
                        try {
                            val responseError = Gson().fromJson(
                                response.errorBody()?.string(),
                                ResponseError::class.java
                            )
                            responseError?.error?.let {
                                _error.value = it
                            }
                        } catch (e: JsonSyntaxException) {
                            e.printStackTrace()
                            _error.value = e.toString()
                        }
                    }
                }
            })
        }

    }

    /**
     * Used to update the image status (active/inactive)
     * @param baseUrl - Base Url value
     * @param inventoryId - Inventory id value corresponding to which status needs to be updated
     * @param imgIdArray - Images id array
     * @param imgStatus - Images status (active = 1, inactive = 0)
     */
    fun updateImageStatus(
        baseUrl: String,
        inventoryId: String?,
        imgIdArray: ArrayList<String?>,
        imgStatus: String
    ) {
        val fields: HashMap<String,String> = HashMap()
        fields[ApiConstants.ACTIVATION] = imgStatus
        fields[ApiConstants.IMAGES_IDS] = Gson().toJson(imgIdArray)
        val apiService =
            RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call =
            apiService?.updateImageStatus(inventoryId, fields)
        call?.enqueue(object : Callback, retrofit2.Callback<InventoryImages> {
            override fun onFailure(call: Call<InventoryImages>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                call: Call<InventoryImages>,
                response: Response<InventoryImages>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        _inventoryImages.value = it.gallery
                    }
                } else {
                    try {
                        val responseError = Gson().fromJson(
                            response.errorBody()?.string(),
                            ResponseError::class.java
                        )
                        responseError?.error?.let {
                            _error.value = it
                        }
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })
    }

    /**
     * Used to delete the inventory image
     * @param baseUrl - Base Url value
     * @param inventoryId - Inventory id value corresponding to which image is deleted
     * @param imgIdArray - Images id array
     */
    fun deleteInventoryImage(
        baseUrl: String,
        inventoryId: String?,
        imgIdArray: ArrayList<String?>
    ) {
        val fields: HashMap<String,String> = HashMap()
        fields[ApiConstants.IMAGES_IDS] = Gson().toJson(imgIdArray)
        val apiService =
            RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call =
            apiService?.deleteInventoryImage(inventoryId, fields)
        call?.enqueue(object : Callback, retrofit2.Callback<InventoryImages> {
            override fun onFailure(call: Call<InventoryImages>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                call: Call<InventoryImages>,
                response: Response<InventoryImages>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        _inventoryImages.value = it.gallery
                    }
                } else {
                    try {
                        val responseError = Gson().fromJson(
                            response.errorBody()?.string(),
                            ResponseError::class.java
                        )
                        responseError?.error?.let {
                            _error.value = it
                        }
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })
    }

    /**
     * Used to sort the inventory image
     * @param baseUrl - Base Url value
     * @param inventoryId - Inventory id value corresponding to which image is deleted
     * @param imgIdArray - Images id array
     */
    fun sortImagesRank(
        baseUrl: String,
        inventoryId: String?,
        imgIdArray: java.util.ArrayList<String?>
    ) {
        /*val subField: HashMap<String,String> = HashMap()
        imgIdArray.forEachIndexed { index, imgId ->
            subField[ApiConstants.ID] = imgId!!
        }*/
        var jsonArray = JSONArray()
        imgIdArray.forEachIndexed { index, imgId ->
            var js = JSONObject()
            js.put(ApiConstants.ID,imgId!!)
            jsonArray.put(js)
        }
        val fields: HashMap<String,String> = HashMap()
        fields[ApiConstants.GALLERY] = jsonArray.toString()
        val apiService =
            RetrofitClientInstance(baseUrl).retrofitInstance?.create(ApiService::class.java)
        val call =
            apiService?.sortInventoryImagesRank(inventoryId, fields)
        call?.enqueue(object : Callback, retrofit2.Callback<SortImagesResponse> {
            override fun onFailure(call: Call<SortImagesResponse>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                call: Call<SortImagesResponse>,
                response: Response<SortImagesResponse>
            ) {
                if (response.code() == 200) {
                    response.body()?.let {
                        _sortImages.value = it
                    }
                } else {
                    try {
                        val responseError = Gson().fromJson(
                            response.errorBody()?.string(),
                            ResponseError::class.java
                        )
                        responseError?.error?.let {
                            _error.value = it
                        }
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })
    }
}