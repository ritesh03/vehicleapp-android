package com.zopdealer.ui.inventory.search_inventory

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.zopdealer.R
import com.zopdealer.base_classes.BaseActivity
import com.zopdealer.ui.inventory.AdapterInventory
import com.zopdealer.ui.inventory.InventoryViewModel
import com.zopdealer.ui.inventory.InventoryViewModelFactory
import com.zopdealer.ui.inventory.ItemClickListener
import com.zopdealer.ui.vehicle_detail.VehicleDetailActivity
import com.zopdealer.utils.Constants
import com.zopdealer.utils.preferences.Preferences
import kotlinx.android.synthetic.main.activity_search_inventory.*

class ActivitySearchInventory : BaseActivity(), ItemClickListener {

    lateinit var inventoryAdapter: AdapterInventory

    private lateinit var viewModel: InventoryViewModel

    companion object {
        fun start(context: Context) {
            val starter = Intent(context, ActivitySearchInventory::class.java)
            context.startActivity(starter)
        }
    }


    override fun getID(): Int {

        return R.layout.activity_search_inventory
    }

    override fun iniView(savedInstanceState: Bundle?) {
        viewModel = ViewModelProviders.of(
            this,
            InventoryViewModelFactory()
        )[InventoryViewModel::class.java]

        viewModel.inventoryResponse.observe(::getLifecycle) {
            it?.let { listInventory ->
                listInventory.inventory?.let {
                    inventoryAdapter.submitList(it)
                }
            }
        }

        viewModel.error.observe(::getLifecycle) {
            it?.let {
                Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
            }
        }


        initViews()

        search_text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s != null) {
                    if (s.toString().isNotEmpty()) {
                        getInventoryList(s.toString())
                        rv_search_inventory.visibility = View.VISIBLE
                    } else {
                        rv_search_inventory.visibility = View.GONE
                    }
                }
            }
        })
    }

    fun onClickSearchActivity(v: View) {
        when (v) {
            btn_back -> {
                finish()
            }
            btn_close -> {
                search_text.setText("")
                search_text.hint = "Search Vehicle"
            }
        }
    }

    private fun initViews() {
        inventoryAdapter = AdapterInventory(this, this)
        rv_search_inventory.layoutManager = StaggeredGridLayoutManager(1, 1)
        rv_search_inventory.adapter = inventoryAdapter

        inventoryAdapter.onItemClick = {
            VehicleDetailActivity.start(this, it.inventory_id!!, it.image_urls ?: "")
        }
    }

    override fun onItemClick(
        pos: Int,
        url: String,
        drawable: Int,
        isOnline: Boolean
    ) {

        VehicleDetailActivity.start(this, url, drawable, isOnline)
    }

    private fun getInventoryList(search: String?) {
        viewModel.getInventoryList(
            Preferences.prefs?.getString(Constants.BASE_URL, "") ?: "",
            search = search
        )
    }
}