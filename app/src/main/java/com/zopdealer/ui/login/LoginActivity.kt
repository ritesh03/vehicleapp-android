package com.zopdealer.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.zopdealer.R
import com.zopdealer.base_classes.BaseActivity
import com.zopdealer.ui.dealer.DealerDetailsActivity
import com.zopdealer.ui.home.HomeActivity
import com.zopdealer.utils.Constants
import com.zopdealer.utils.preferences.Preferences
import com.zopdealer.utils.preferences.saveValue
import kotlinx.android.synthetic.main.activity_login.*
import org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString
import java.security.MessageDigest
import java.security.SecureRandom

class LoginActivity : BaseActivity() {
    var loginViewModel:LoginViewModel?=null
    var randomString:String?=null
   // var verifier: String?=null
    companion object {
        fun start(context: Context) {
            val starter = Intent(context, LoginActivity::class.java)
            starter.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(starter)
        }
    }

    override fun getID(): Int {
        return R.layout.activity_login
    }

    override fun iniView(savedInstanceState: Bundle?) {
        loginViewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        if (!Preferences.prefs?.getString(Constants.EMAIL_ID, "0").isNullOrBlank()) {
            et_email.setText(Preferences.prefs?.getString(Constants.EMAIL_ID, ""))
        }
       /* if (!Preferences.prefs?.getString(Constants.DEALER_ID, "0").isNullOrBlank()) {
            et_dealer_id.setText(Preferences.prefs?.getString(Constants.DEALER_ID, ""))
        }*/
        val code_verifier = generateRandomString(128);
        Log.e("code_verifier", code_verifier);
        val code_challenge = generateCodeChallenge(code_verifier);
        Log.e("code_challenge", code_challenge);

        loginViewModel?.loginResponse?.observe(this, Observer {
            // Update the UI here directly
            showLoading(false)
           // Toast.makeText(this, "success", Toast.LENGTH_SHORT).show()
            if (it.state.equals(randomString, true)) {
                showLoading(true)
                loginViewModel?.getLoginToken(it.code.toString(),code_verifier)
            } else {
                Toast.makeText(this, "State not matched", Toast.LENGTH_SHORT).show()
            }

        })
        loginViewModel?.logintokenResponse?.observe(this, Observer {
            // Update the UI here directly
            showLoading(false)
            Preferences.prefs?.saveValue(Constants.USER_TOKEN, it.id_token)
            HomeActivity.start(this)
            finish()

        })


        loginViewModel?.error?.observe(this, Observer {
            // Update the UI here directly
            showLoading(false)
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()


        })


        btn_login.setOnClickListener {
            if (et_email.text.toString().isBlank()) {
                showSnackBar(getString(R.string.empty_email), window.decorView)
            } else if (et_password.text.toString().isBlank()) {
                showSnackBar(getString(R.string.empty_password), window.decorView)
            } else {
                if (loginViewModel != null) {
                    showLoading(true)
                    //random string
                    val length = 6
                    randomString = getRandomString(length)
                    Log.e("randomString===", randomString!!)
                    // Dependency: Apache Commons Codec
                    var connection:String=DealerDetailsActivity.appstate
                   /* if (DealerDetailsActivity.appstate.equals("live")){
                       connection="livetest"
                    }else{
                        connection="localtest"
                    }*/

                    loginViewModel?.userLogin(et_email.text.toString(), et_password.text.toString(), randomString!!,code_challenge,connection)
                }
            }
        }
    }

    fun getRandomString(length: Int) : String {
        val charset = "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz0123456789"
        return (1..length)
                .map { charset.random() }
                .joinToString("")
    }

    fun generateRandomString(length : Int) : String{
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~";
        for (i in 1..length) {
            var double_value = Math.floor(Math.random() * possible.length);
            var position = double_value.toInt(); text += possible.get(position);
        }
        return text;
    }

    fun generateCodeChallenge(code_verifier : String) : String{
        return hashString(code_verifier, "SHA-256");
    }
    private fun hashString(input: String, algorithm: String): String {
        val bytes: ByteArray = input.toByteArray();
        val md = MessageDigest.getInstance("SHA-256")
        md.update(bytes, 0, bytes.size)
        val digest = md.digest()
        val challenge: String = Base64.encodeToString(digest, Base64.URL_SAFE or Base64.NO_PADDING);
        return challenge;
    }
   /* suspend fun onClickLoginPage(view: View) {
        when (view) {
            btn_login -> {
                if (et_email.text.toString().isBlank()) {
                    showSnackBar(getString(R.string.empty_email), window.decorView)
                } else if (et_password.text.toString().isBlank()) {
                    showSnackBar(getString(R.string.empty_password), window.decorView)
                } else {
                    if (loginViewModel != null) {
                        loginViewModel.userLogin(et_email.text.toString(),et_password.text.toString())
                    }
                }
              *//*  Preferences.prefs?.saveValue(Constants.EMAIL_ID, et_email.text.toString())
                HomeActivity.start(this)*//*

            }//btn_login

            btn_forgot_password -> {
                ActivityForgotPassword.start(this)
            }

            tv_edit_dealer_id -> {
                DealerDetailsActivity.start(this)
            }

        }//when

    }//onClickLoginPage*/
}//LoginActivity