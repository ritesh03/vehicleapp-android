package com.zopdealer.ui.login

import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonSyntaxException
import com.zopdealer.api.ApiService
import com.zopdealer.api.RetrofitClientInstance
import com.zopdealer.network.responses.LoginResponse
import com.zopdealer.network.responses.LogintokenResponse
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback


class LoginViewModel : ViewModel() {
    private val _error: MutableLiveData<String> = MutableLiveData()
    private val _loginReponseResponse: MutableLiveData<LoginResponse> = MutableLiveData()
    private val _tokenResponse: MutableLiveData<LogintokenResponse> = MutableLiveData()

        /*suspend fun userLogin(
            email: String,
            password: String
        ) = withContext(Dispatchers.IO) { repository.userLogin(email, password) }*/
        val error: LiveData<String>
            get() = _error

      val loginResponse: LiveData<LoginResponse>
        get() = _loginReponseResponse

     val logintokenResponse: LiveData<LogintokenResponse>
        get() = _tokenResponse

    fun userLogin(
            email: String,
            password: String,
            state: String,
            code_challenge: String,
            connection: String
    ){
         // test login base url=https://cardoro.ca/
        // test login redirect url=https://cardoro.ca/callback
        //live login base url=https://zopauth.com/
        // live login redirect url= https://www.zopauth.com/callback
        val apiService = RetrofitClientInstance("https://zopauth.com/").retrofitInstance?.create(ApiService::class.java)
        val call=apiService?.userLogin("code", "zopsoftware-app", "https://www.zopauth.com/callback",
                "openid email profile app user", state, connection, code_challenge.toString().trim(), "S256", "android", email, password)
        call?.enqueue(object : Callback, retrofit2.Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                _error.postValue(t.localizedMessage)

            }

            override fun onResponse(
                    call: Call<LoginResponse>,
                    response: Response<LoginResponse>
            ) {
                if (response.isSuccessful) {
                    _loginReponseResponse.postValue(response.body())

                } else {
                    try {
                        if (response.errorBody()?.toString() != null) {
                            val jObjError = JSONObject(response.errorBody()!!.charStream().readText())
                            _error.value = jObjError.getString("messages")
                            //Toast.makeText(requireActivity,response.errorBody()?.string(), Toast.LENGTH_LONG).show()
                        } else {
                            _error.value = "Server error"
                        }
                        /*    val responseError = Gson().fromJson(
                                response.errorBody()!!.string(),
                                ResponseError::class.java
                        )
                        _error.value = responseError.error*/
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })

    }

    fun getLoginToken(code: String, code_verifier: String){
        val apiService = RetrofitClientInstance("https://zopauth.com/").retrofitInstance?.create(ApiService::class.java)
        val call=apiService?.logintoken("authorization_code", "zopsoftware-app", "https://www.zopauth.com/callback", code, code_verifier)
        call?.enqueue(object : Callback, retrofit2.Callback<LogintokenResponse> {
            override fun onFailure(call: Call<LogintokenResponse>, t: Throwable) {
                _error.postValue(t.localizedMessage)
            }

            override fun onResponse(
                    call: Call<LogintokenResponse>,
                    response: Response<LogintokenResponse>
            ) {
                if (response.isSuccessful) {
                    _tokenResponse.postValue(response.body())

                } else {
                    try {
                        if (response.errorBody()?.toString() != null) {
                            val jObjError = JSONObject(response.errorBody()!!.charStream().readText())
                            _error.value = jObjError.getString("messages")
                            //Toast.makeText(requireActivity,response.errorBody()?.string(), Toast.LENGTH_LONG).show()
                        } else {
                            _error.value = "Server error"
                        }


                        /*  val responseError = Gson().fromJson(
                                response.errorBody()!!.string(),
                                ResponseError::class.java
                        )
                        _error.value = responseError.error*/
                    } catch (e: JsonSyntaxException) {
                        e.printStackTrace()
                        _error.value = e.toString()
                    }
                }
            }
        })
    }
}