package com.zopdealer.ui.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.zopdealer.network.repositories.LoginRepository

@Suppress("UNCHECKED_CAST")
class LoginViewModelFactory (
    private val repository: LoginRepository
) : ViewModelProvider.NewInstanceFactory() {

   /* override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LoginViewModel(repository) as
    }*/
}