package com.zopdealer.utils;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.DimenRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

    private int mItemOffset;

    public GridSpacingItemDecoration(int itemOffset) {
        mItemOffset = itemOffset;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        int position = parent.getChildViewHolder(view).getAdapterPosition();
        int itemCount = state.getItemCount();
        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        setSpacingForDirection(outRect, layoutManager, position, itemCount);
    }

    private void setSpacingForDirection(Rect outRect,
                                        RecyclerView.LayoutManager layoutManager,
                                        int position,
                                        int itemCount) {
        GridLayoutManager gridLayoutManager = (GridLayoutManager) layoutManager;
        int cols = gridLayoutManager.getSpanCount();
        int rows = itemCount / cols;

        outRect.left = mItemOffset;
        outRect.right = position % cols == cols - 1 ? mItemOffset : 0;
        outRect.top = mItemOffset;
        outRect.bottom = mItemOffset;
    }
}