package com.zopdealer.utils

object Constants {

    val INTENT_EXTRA_LIMIT: String="limit"
    const val BASE_URL: String = ""
    //test server
    //const val IMAGE_BASE_URL: String = "https://zd-local.b-cdn.net"
    //live server url
    const val IMAGE_BASE_URL: String = "https://zopsoftware-asset.b-cdn.net"

    const val SNACK_BAR_DURATION = 2000
    const val CAMERA_REQUEST = 0
    const val GALLERY_PICTURE = 1

    //Shared Preference KEY
    const val DEALER_ID="dealer_id"
    const val DEALER_DOMAIN="dealer_domain"
    const val DEALER_NAME="dealer_name"
    const val EMAIL_ID="email_id"
    const val USER_ID = "user_id"
    const val USER_TOKEN="user_token"
    const val INVENTORY_ID = "inventory_id"

    // Used to handle the active image status
    const val INVENTORY_IMG_ACTIVE = "1"
    // Used to handle the inactive image status
    const val INVENTORY_IMG_INACTIVE = "0"
}

/**
 * Used to handle the api constants
 */
object ApiConstants {
    // Used to for the ids array
    const val IMAGES_IDS = "ids"
    // Used to for the activation key in status api
    const val ACTIVATION = "activation"
    // Used to for the gallery array
    const val GALLERY = "gallery"
    // Used to for the ids in sorting the rank
    const val ID = "id"
}