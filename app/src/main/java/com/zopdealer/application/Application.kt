package com.zopdealer.application

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.zopdealer.network.MyApi
import com.zopdealer.network.NetworkConnectionInterceptor
import com.zopdealer.ui.home.HomeViewModelFactory
import com.zopdealer.ui.login.LoginViewModelFactory
import com.zopdealer.utils.preferences.Preferences
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class Application:Application(), KodeinAware {
    companion object AppContext {
        var mContext: Context? = null
        fun getContext(): Context {
            return mContext!!
        }
    }
    init {
        mContext = this
    }
    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        Preferences.initPreferences(this)
        mContext = applicationContext
    }

    override val kodein = Kodein.lazy {
        import(androidXModule(this@Application))

        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { MyApi(instance()) }
        bind() from provider { LoginViewModelFactory(instance()) }

    }

}