package com.zopdealer.base_classes

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.icu.util.Calendar
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar
import com.zopdealer.R
import com.zopdealer.utils.Constants
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


open class BaseFragment : Fragment() {
    protected var mContent: View? = null// For showing snackbar
    private var mActivity: FragmentActivity? = null

    private var mProgressDialog: Dialog? = null

    private lateinit var mCalendar: java.util.Calendar
    private var mStartTime: Calendar? = null
    private var mEndTime: Calendar? = null

    var cal = java.util.Calendar.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mActivity = activity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mContent = view
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mStartTime = Calendar.getInstance()
            mEndTime = Calendar.getInstance()
            mCalendar = java.util.Calendar.getInstance()
        }

    }

    fun showLoading(show: Boolean?) {
        if (show!!) showProgress() else hideProgress()
    }

    private fun showProgress() {
        if (mProgressDialog == null) {

            mProgressDialog =
                this!!.mActivity?.let { Dialog(it, android.R.style.Theme_Translucent) }
            mProgressDialog?.window!!.requestFeature(Window.FEATURE_NO_TITLE)
            mProgressDialog?.setContentView(R.layout.loader_half__layout)
            mProgressDialog?.setCancelable(false)

        }

        mProgressDialog?.show()
    }

    fun hideProgress() {
        if (mProgressDialog != null && mProgressDialog!!.isShowing) {
            mProgressDialog?.dismiss()
        }
    }

    @SuppressLint("WrongConstant")
    fun showSnackBar(message: String) {
        mContent?.let {

            val snackbar = Snackbar.make(it, message, Snackbar.LENGTH_LONG)
            val snackbarView = snackbar.view
            val tv = snackbarView.findViewById<TextView>(R.id.snackbar_text)
            tv.maxLines = 3
            snackbar.duration = Constants.SNACK_BAR_DURATION
            snackbar.show()

        }
    }

    fun showSnackBar(message: String, content: View) {
        this.let {
            Snackbar.make(content, message, Snackbar.LENGTH_LONG).show()
        }
    }

    /**
     * Add fragment with or without addToBackStack
     *
     * @param fragment       which needs to be attached
     * @param addToBackStack is fragment needed to backstack
     */
    fun addFragment(fragment: Fragment, addToBackStack: Boolean, id: Int) {
        val tag = fragment.javaClass.simpleName
        val fragmentManager = activity?.supportFragmentManager
        val fragmentOldObject = fragmentManager?.findFragmentByTag(tag)
        val transaction = fragmentManager?.beginTransaction()
        transaction?.setCustomAnimations(
            R.anim.anim_in,
            R.anim.anim_out,
            R.anim.anim_in_reverse,
            R.anim.anim_out_reverse
        )
        if (fragmentOldObject != null) {
            fragmentManager.popBackStackImmediate(tag, 0)
        } else {
            if (addToBackStack) {
                transaction?.addToBackStack(tag)
            }
            transaction?.add(id, fragment, tag)
                ?.commitAllowingStateLoss()
        }
    }

    fun replaceFragment(
        fragment: Fragment, addToBackStack: Boolean, id: Int
    ) {
        val tag: String = fragment::class.java.simpleName
        val fragmentManager = activity?.supportFragmentManager
        val fragmentObj: Fragment? = fragmentManager?.findFragmentByTag(tag)
        val transaction = fragmentManager?.beginTransaction()
        if (fragmentObj == null) {
            transaction?.replace(id, fragment, tag)
                ?.addToBackStack(tag)
                ?.commitAllowingStateLoss()
        } else {
            transaction?.replace(id, fragmentObj, tag)
                ?.addToBackStack(tag)
                ?.commitAllowingStateLoss()
        }
    }

    fun spinnerArrayAdapter(spinner: Spinner, array: Int, context: Context) {
        ArrayAdapter.createFromResource(
            context,
            array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter

        }
    }


    fun spinnerArrayAdapter(spinner: Spinner, array: ArrayList<String>, context: Context) {

        val adapter: ArrayAdapter<String> =
            ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, array)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spinner.adapter = adapter
    }


    fun updateDateInView(cal: java.util.Calendar, format: String): String {
        val myFormat = format // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        return sdf.format(cal.getTime()).toString().toUpperCase()
    }

    fun getAbbreviatedFromDateTime(dateTime: String, dateFormat: String, field: String): String? {
        val input = SimpleDateFormat(dateFormat, Locale.getDefault())
        val output = SimpleDateFormat(field, Locale.getDefault())
        try {
            val getAbbreviate = input.parse(dateTime)    // parse input
            return output.format(getAbbreviate)    // format output
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return null
    }

    open fun hideKeyboard() {
        val view = activity?.currentFocus
        if (view != null) {
            val inputManager: InputMethodManager =
                activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(
                view.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
        }
    }

}