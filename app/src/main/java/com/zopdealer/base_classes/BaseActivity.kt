package com.zopdealer.base_classes

import android.Manifest
import com.zopdealer.R
import android.app.Activity
import android.app.Dialog
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.*
import com.google.android.material.snackbar.Snackbar
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

open abstract class BaseActivity: AppCompatActivity() {
    protected var mDoubleBackToExitPressedOnce = false

    private val PERMISSION_REQUEST = 121

    protected val TAG: String = javaClass.simpleName

    private var mProgressDialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //  requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        setContentView(getID())
        iniView(savedInstanceState)
        //  startService(Intent(this, LocationService_c::class.java))

    }

    abstract fun getID(): Int
    abstract fun iniView(savedInstanceState: Bundle?)

    fun showSnackBar(message: String, content: View) {
        this.let {
            Snackbar.make(content, message, Snackbar.LENGTH_LONG).show()
        }
    }

    private fun showProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = Dialog(this)
            mProgressDialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
            mProgressDialog?.setContentView(R.layout.loader_half__layout)
            mProgressDialog?.setCancelable(false)

        }
        mProgressDialog!!.show()
    }

    private fun hideProgress() {
        if (mProgressDialog != null && mProgressDialog!!.isShowing) {
            mProgressDialog?.dismiss()
        }
    }

    fun showLoading(show: Boolean?) {
        if (show!!) showProgress() else hideProgress()
    }


    fun hideSoftKeyboard() {
        if (this.currentFocus != null) {
            val inputMethodManager = this.getSystemService(
                Activity.INPUT_METHOD_SERVICE
            ) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(
                this.currentFocus!!.windowToken, 0
            )
        }
    }
    fun showSoftKeyBoard() {
        if (this.currentFocus != null) {
            val inputMethodManager = this.getSystemService(
                Activity.INPUT_METHOD_SERVICE
            ) as InputMethodManager
            inputMethodManager.showSoftInputFromInputMethod(
                this.currentFocus!!.windowToken, 0
            )
        }
    }
    fun roundOffTo2DecPlaces(value: Double): String {
        return String.format("%.2f", value)
    }

    fun callToPhoneNO(tel:String){
        if (checkSelfPermission(
                this,
                Manifest.permission.CALL_PHONE
            ) != PackageManager.PERMISSION_GRANTED
        ) run {
            ActivityCompat.requestPermissions(
                this as Activity, arrayOf(
                    Manifest.permission.CALL_PHONE
                ), 993
            )
        }
    }

    fun getAbbreviatedFromDateTime(dateTime: String, dateFormat: String, field: String): String? {
        val input = SimpleDateFormat(dateFormat, Locale.getDefault())
        val output = SimpleDateFormat(field, Locale.getDefault())
        try {
            val getAbbreviate = input.parse(dateTime)    // parse input
            return output.format(getAbbreviate)    // format output
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return null
    }
}