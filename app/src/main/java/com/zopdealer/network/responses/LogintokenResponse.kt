package com.zopdealer.network.responses

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LogintokenResponse(
        val id_token: String?,
        val token_type: String?,
        val expires_in: String?,
        val access_token: String?,
        val refresh_token: String?
): Parcelable
