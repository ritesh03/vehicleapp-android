package com.zopdealer.network.responses

import java.io.Serializable

data class InventoryResponse(
    val inventory: List<Inventory>?,
    val pager: Pager?
)

data class Pager(val hasMore: Boolean, val total: Int, val perPage: Int, val pageCount: Int)

data class Inventory(
    var inventory_id: String? = null,
    var vin: String? = null,
    var vindisplay: String? = "1",
    var stock_no: String? = null,
    var stock_date: String? = null,
    var status: String? = null,
    val sub_status: String? = null,
    var feature_listing: String? = "0",
    var vehicle_type: String? = null,
    var visibility: String? = "1",
    var vehicle_tags: String? = null,
    var title: String? = null,
    var year: String? = null,
    var make: String? = null,
    var model: String? = null,
    var trim: String? = null,
    var body_type: String? = null,
    var door: String? = null,
    var engine: String? = null,
    var litres: String? = null,
    var transmission: String? = null,
    var drivetrain: String? = null,
    var passengers: String? = null,
    var odometer: String? = null,
    var km_miles: String? = null,
    var fuel_type: String? = null,
    var fuel_city: String? = null,
    var fuel_hwy: String? = null,
    var exterior_color: String? = null,
    var interior_color: String? = null,
    var price: String? = null,
    var hide_price: String? = "0",
    var special_price_on: String? =null,
    var special_price: String? = null,
    var special_price_type: String? = null,
    var special_price_date: String? = null,
    var show_stripe: String? = null,
    var financing_on: String? = null,
    var financing_special: String? = "0",
    var downpayment: String? = null,
    var interest_rate: String? = null,
    var term: String? = null,
    var payment_frequency: String? = null,
    var location: String? = null,
    var vehicle_description: String? = null,
    var custom_features: String? = null,
    var syndication: String? = null,
    var created_at: String? = null,
    var updated_at: String? = null,
    var deleted_at: String? = null,
    var vin_company: String? = null,
    var image_urls: String? = null,
    var image_count: String? = null,
    var standard: String? = null,
    var optional: String? = null,
    var technical_specification: String? = null,
    var generic_equipment: String? = null
) : Serializable

/**
 * Used to handle the inventory images response
 */
data class InventoryImages(var last_rankval: String?, var gallery: MutableList<Gallery>)

/**
 * Used to handle the Gallery images array that will return in InventoryImages response
 */
data class Gallery(
    var id: String?,
    var inventory_id: String?,
    var image_url: String?,
    var sort_rank: String?,
    var active: String?,
    var created_at: String?,
    var updated_at: String?,
    var deleted_at: String?,
    var image_with_overlay_url: String?
)

/**
 * Used to handle the upload inventory image response
 */
data class UploadInventoryImage(
    var image_url: String?,
    var inventory_id: String?,
    var sort_rank: String?,
    var id: Int,
    var success: String?
)

/**
 * Used to handle the sort inventory images response
 */
data class SortImagesResponse(var success: String?)