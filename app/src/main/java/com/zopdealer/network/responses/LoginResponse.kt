package com.zopdealer.network.responses

import android.os.Parcelable
import com.zopdealer.network.entities.User
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LoginResponse(
    val email: String?,
    val state: String?,
    val code: String?
   /* val data: User?*/
) : Parcelable
