package com.zopdealer.network.responses

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

class DropDownResponses {

    @Parcelize
    data class YearResponse(
        val status: Boolean?,
        val message: String?
    ) : Parcelable


    @Parcelize
    data class MakeResponse(
        val status: Boolean?,
        val message: String?
    ) : Parcelable


    @Parcelize
    data class BodyTypeResponse(
        val status: Boolean?,
        val message: String?
    ) : Parcelable


}//DropDownResponses