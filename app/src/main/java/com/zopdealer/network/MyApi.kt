package com.zopdealer.network

import com.readystatesoftware.chuck.ChuckInterceptor
import com.zopdealer.application.Application
import com.zopdealer.config.Config
import com.zopdealer.network.responses.DropDownResponses
import com.zopdealer.network.responses.LoginResponse
import com.zopdealer.network.responses.SignUpResponse
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface MyApi {

    @FormUrlEncoded
    @POST("/json/year.json")
    suspend fun fetchYear(): Response<DropDownResponses.YearResponse>

    @FormUrlEncoded
    @POST("/json/make.json")
    suspend fun fetchMake(): Response<DropDownResponses.MakeResponse>

    @FormUrlEncoded
    @POST("/json/body_type.json")
    suspend fun fetchBodyType(): Response<DropDownResponses.BodyTypeResponse>

    @FormUrlEncoded
    @POST("/login")
    suspend fun userLogin(
        @Field("email") email: String,
        @Field("password") password: String
    ): Response<LoginResponse>

    @FormUrlEncoded
    @POST("/registration")
    suspend fun userSignup(
        @Field("name") name: String,
        @Field("password") password: String,
        @Field("phone") phone: String,
        @Field("city") city: String,
        @Field("otp") otp: String,
        @Field("device_id") device_id: String,
        @Field("state") state: String
    ): Response<SignUpResponse>

    companion object {
        operator fun invoke(
            networkConnectionInterceptor: NetworkConnectionInterceptor
        ): MyApi {

            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            val okkHttpclient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(ChuckInterceptor(Application.mContext))
                .addInterceptor(BasicAuthInterceptor("", ""))
                .addInterceptor { chain ->
                    val request: Request =
                        chain.request().newBuilder()
                            .addHeader("Authorization", "")
                            .build()
                    chain.proceed(request)
                }
                .build()

            return Retrofit.Builder()
                .client(okkHttpclient)
                .baseUrl(Config.BASE_URL_PAL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(MyApi::class.java)

        }
    }
}

