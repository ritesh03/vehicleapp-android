package com.zopdealer.network.repositories

import com.zopdealer.network.MyApi
import com.zopdealer.network.SafeApiRequest
import com.zopdealer.network.responses.LoginResponse

class LoginRepository (
    private val api: MyApi
    ) : SafeApiRequest() {

        suspend fun userLogin(email: String, password: String): LoginResponse {
            return apiRequest { api.userLogin(email, password) }
        }

}