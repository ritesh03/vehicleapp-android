package com.zopdealer.network.repositories

import com.zopdealer.network.MyApi
import com.zopdealer.network.SafeApiRequest
import com.zopdealer.network.responses.DropDownResponses

class HomeRepository(
    private val api: MyApi
) : SafeApiRequest() {

    suspend fun getYear(): DropDownResponses.YearResponse {
        return apiRequest { api.fetchYear() }
    }

    suspend fun getMake(): DropDownResponses.MakeResponse {
        return apiRequest { api.fetchMake() }
    }

    suspend fun getBodyType(): DropDownResponses.BodyTypeResponse {
        return apiRequest { api.fetchBodyType() }
    }


}//HomeRepository