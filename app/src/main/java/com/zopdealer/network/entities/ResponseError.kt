package com.zopdealer.network.entities

data class ResponseError(val error: String?)