package com.zopdealer.api

import com.zopdealer.utils.Constants
import com.zopdealer.utils.preferences.Preferences
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.SecureRandom
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager
import javax.security.cert.X509Certificate

class RetrofitClientInstance(private val baseUrl: String) {
    private var retrofit: Retrofit? = null
    val retrofitInstance: Retrofit?
        get() {
            if (retrofit == null) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY  // Create a trust manager that does not validate certificate chains
                val trustAllCerts:  Array<TrustManager> = arrayOf(object : X509TrustManager {
                    override fun checkClientTrusted(p0: Array<out java.security.cert.X509Certificate>?, p1: String?) {}
                    override fun checkServerTrusted(p0: Array<out java.security.cert.X509Certificate>?, p1: String?) {}
                    override fun getAcceptedIssuers(): Array<out java.security.cert.X509Certificate>? = arrayOf()
                })

                // Install the all-trusting trust manager
                val  sslContext = SSLContext.getInstance("SSL")
                sslContext.init(null, trustAllCerts, SecureRandom())

                // Create an ssl socket factory with our all-trusting manager
                val sslSocketFactory = sslContext.socketFactory

                val client = OkHttpClient.Builder()
                    .connectTimeout(8,TimeUnit.MINUTES)
                    .readTimeout(1000,TimeUnit.SECONDS)
                    .writeTimeout(1000,TimeUnit.SECONDS)
                    .sslSocketFactory(sslSocketFactory, trustAllCerts.first() as X509TrustManager)
                    .apply {
                    addInterceptor(
                        Interceptor { chain ->
                            val builder = chain.request().newBuilder()
                            builder.header(
                                "Authorization",
                                "Bearer " + Preferences.prefs?.getString(Constants.USER_TOKEN, "")
                            )
                            return@Interceptor chain.proceed(builder.build())
                        }
                    )
                    addInterceptor(interceptor)

                }
                retrofit = Retrofit.Builder()
                    .client(client.build())
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return retrofit
        }
}