package com.zopdealer.api

import com.zopdealer.network.responses.*
import com.zopdealer.ui.add_vehicle.model.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface ApiService {
    @POST("api/admin/get/connection/")
    fun getConnectionId(): Call<ConnectionResponse>
    @FormUrlEncoded
    @POST("rest/authorize")
    fun userLogin(
            @Query("response_type") response_type: String?,
            @Query("client_id") client_id: String?,
            @Query("redirect_uri") redirect_uri: String?,
            @Query("scope") scope: String?,
            @Query("state") state: String?,
            @Query("connection") connection: String?,
            @Query("code_challenge") code_challenge: String?,
            @Query("code_challenge_method") code_challenge_method: String?,
            @Query("device") device: String?,
            @Field("email") email: String,
            @Field("password") password: String
    ): Call<LoginResponse>

 @FormUrlEncoded
 @POST("rest/token")
 fun logintoken(
         @Field("grant_type") grant_type : String?,
         @Field("client_id") client_id: String?,
         @Field("redirect_uri") redirect_uri: String?,
         @Field("code") code : String?,
         @Field("code_verifier") code_verifier : String?
 ): Call<LogintokenResponse>



    @GET("api/website/inventory")
    fun getInventoryList(
        @Query("search") search: String?,
        @Query("page") page: Int?,
        @Query("column") column: String?,
        @Query("order") order: String?,
        @QueryMap filter: Map<String, String>
    ): Call<InventoryResponse>

    @GET("/api/business/inventory/show/{inventory_id}")
    fun getInventoryDetail(@Path("inventory_id") inventory_id: String?): Call<Inventory>

    @GET("api/business/vehicles/{vin}")
    fun decodeVin(@Path("vin") vin: String?): Call<VinModel>

    @GET("api/business/location/active")
    fun getVehicleLocation(): Call<VehicleLocationModel>

    @GET("api/business/configurations/active/inventory")
    fun getActiveConfiguration(): Call<List<ActiveConfigurationModel>>

    @GET("json/year.json")
    fun getYear(): Call<List<String>>

    @GET("json/make.json")
    fun getMake(): Call<List<String>>

    @GET("json/body_type.json")
    fun getBodyType(): Call<List<String>>

    @GET("json/door.json")
    fun getDoor(): Call<List<String>>

    @GET("json/cylinder.json")
    fun getCylinder(): Call<List<String>>

    @GET("json/transmission.json")
    fun getTransmission(): Call<List<String>>

    @GET("json/drivetrain.json")
    fun getDriveTrain(): Call<List<String>>

    @GET("json/passenger.json")
    fun getPassenger(): Call<List<String>>

    @GET("json/fuel_type.json")
    fun getFuelType(): Call<List<String>>

    @GET("/api/business/inventory/image/{inventory_id}")
    fun getInventoryImages(@Path("inventory_id") inventory_id: String?): Call<InventoryImages>

    @Multipart
    @POST("/api/business/inventory/image/upload/{inventory_id}")
    fun uploadImage(
        @Part image: MultipartBody.Part?,
        @Part("file") name: RequestBody?,
        @Part("sort_rank") rank: Int?,
        @Path("inventory_id") inventory_id: String?
    ): Call<UploadInventoryImage>

    @FormUrlEncoded
    @POST("api/business/inventory/image/active/{inventory_id}")
    fun updateImageStatus(
        @Path("inventory_id") inventory_id: String?,
        @FieldMap fields: Map<String, String>
    ): Call<InventoryImages>

    @FormUrlEncoded
    @POST("api/business/inventory/image/delete/{inventory_id}")
    fun deleteInventoryImage(
        @Path("inventory_id") inventory_id: String?,
        @FieldMap fields: Map<String, String>
    ): Call<InventoryImages>

    @FormUrlEncoded
    @POST("api/business/inventory/image/sort/{inventory_id}")
    fun sortInventoryImagesRank(
        @Path("inventory_id") inventory_id: String?,
        @FieldMap fields: Map<String, String>
    ): Call<SortImagesResponse>

    @GET("api/syndication/companies/all?paginate=false")
    fun getSyndicationCompanies(): Call<SyndicationCompaniesModel>

    @GET("api/business/configurations/active/syndication")
    fun getActiveSyndication(): Call<List<ActiveSyndicationModel>>

    @FormUrlEncoded
    @POST("api/business/inventory/create")
    fun createInventory(@FieldMap fields: Map<String, String>): Call<CreateInventorySuccessModel>

    @FormUrlEncoded
    @POST("/api/business/inventory/update/{inventory_id}")
    fun updateInventory(
        @Path("inventory_id") inventoryId: String?,
        @FieldMap fields: Map<String, String>
    ): Call<CreateInventorySuccessModel>
}